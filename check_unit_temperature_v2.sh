#!/bin/bash

PROGNAME="check_unit_temperature_v2"
AUTHOR="Sami Ereq 2015/07 Netnordic AB"
VERSION="Version 2.0"

print_version() {
        echo "$VERSION $AUTHOR"
}

print_help() {
print_version $AUTHOR $VERSION
echo ""
echo "$PROGNAME is a OP5 plugin to check temp on network supporing OID's"
echo ""
echo "Usage: $PROGNAME [SNMP version] [SNMP community] [Host] [OID]"
}

#Will be $ARGX$ input from OP5 X=ARG number
SNMPv="2c"
SNMPc=$1
WARNTEMP=$2
CRITTEMP=$3
HOST=$4
CURRTEMPOID=$5

#Exit status for service
ST_OK=0
ST_WR=1
ST_CR=2
ST_UK=3

#Standard response
resp=" - Unit temperature: "

#Get current temperature value and strip away uneccesary info.
OUTPUT="snmpget -O vq -v $SNMPv -c $SNMPc $HOST $CURRTEMPOID";
CURRTEMP=$($OUTPUT);
#echo ${CURRTEMP}
CURRTEMP1=$(echo "${CURRTEMP} C | Temperature=$CURRTEMP;$WARNTEMP;$CRITTEMP;;")

if [ "$CURRTEMP" -ge "$WARNTEMP" -a "$CURRTEMP" -lt "$CRITTEMP" ]
        then
                echo "WARNING${resp}${CURRTEMP1}"
                exit $ST_WR
        elif [ "$CURRTEMP" -ge "$CRITTEMP" ]
                then
                echo "CRITICAL${resp}${CURRTEMP1}"
                exit $ST_CR
		elif [ "$CURRTEMP" -lt "$WARNTEMP" -a "$CURRTEMP" -lt "$CRITTEMP" ]
		then
				echo "OK${resp}${CURRTEMP1}"
                exit $ST_OK
		else
				echo "UNKNOWN${resp}${CURRTEMP1}"
				exit $ST_UK
fi
