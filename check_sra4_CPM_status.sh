#!/bin/bash

PROGNAME="check_sra4_CPM_status"
print_help() {
echo "$PROGNAME is a OP5 plugin to CPM Card status on Alcatel SRa4 (ALCATEL SR 7750)"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST]"
}

declare -a OID=('150994977' '150994993')
declare -a TEMP
declare -a SENSORNAME

BASECHECKOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.16.1."
BASENAMEOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.8.1."
EXITCODE=0

SNMPv="2c"      #SNMP Version
SNMPc=$1        #SNMP Community
HOST=$2         #HOST

for i in 0 1; do

CHECK[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASECHECKOID${OID[$i]}`
SLOTNAME[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASENAMEOID${OID[$i]}`

if [ "${CHECK[$i]}" != "-1" ]
then
        if [ "${CHECK[$i]}" -gt "2" ]
        then
                CHECKSTATUS="CRITICAL - "
                EXITCODE=2

        elif [ "${CHECK[$i]}" -lt "2" ]  && [ $EXITCODE -ne 2 ]
        then
                CHECKSTATUS="WARNING - "
                EXITCODE=1
        elif [ "${CHECK[$i]}" == "2" ] && [ $EXITCODE -eq 0 ]
        then
                CHECKSTATUS="OK - "
                EXITCODE=0
        fi
        CHECKACTIVE="$CHECKACTIVE ${SLOTNAME[$i]}: ${CHECK[$i]} "
fi
done
echo "$CHECKSTATUS$CHECKACTIVE"
exit $EXITCODE