#!/bin/bash

AUTHOR="Author: Farhan Islam, credit to Niklas Sigg"
VERSION="VERSION 1.0"
PROGNAME="$0"

print_version() {
  echo ""
  echo "$PROGNAME - $VERSION"
  echo "$AUTHOR"
  echo ""
}

print_help() {
  echo ""
  echo "$PROGNAME"
  echo "Usage: $PROGNAME -H [Host] -v [SNMP version] -C [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv or skip the flag if you're using snmpv1/snmpv2c) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -o0 [oid for temperature sensor0] -o1 [oid for temperature sensor1] -o2 [oid for temperature sensor2] -o3 [oid for temperature sensor3] -o4 [oid for temperature sensor4] -w [warning thresshold] -c [critical thresshold] "
  echo ""
}

#Flags
while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -C|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -o0|--oidsensor0)
    shift
    OIDSENSOR0=$1;;
    -o1|--oidsensor1)
    shift
    OIDSENSOR1=$1;;
    -o2|--oidsensor2)
    shift
    OIDSENSOR2=$1;;
    -o3|--oidsensor3)
    shift
    OIDSENSOR3=$1;;
    -o4|--oidsensor4)
    shift
    OIDSENSOR4=$1;;
    -w|--warning)
    shift
    WARN=$1;;
    -c|--critical)
    shift
    CRIT=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

SENSOR0=$OIDSENSOR0
SENSOR1=$OIDSENSOR1
SENSOR2=$OIDSENSOR2
SENSOR3=$OIDSENSOR3
SENSOR4=$OIDSENSOR4

#sensor0
if [[ "$SNMPl" == "authPriv" ]]; then
  SENSOR0OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SENSOR0 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  SENSOR0OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SENSOR0 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  SENSOR0OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SENSOR0 2>&1)
else
  SENSOR0OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SENSOR0 2>&1)
fi

#sensor1
if [[ "$SNMPl" == "authPriv" ]]; then
  SENSOR1OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SENSOR1 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  SENSOR1OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SENSOR1 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  SENSOR1OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SENSOR1 2>&1)
else
  SENSOR1OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SENSOR1 2>&1)
fi

#sensor2
if [[ "$SNMPl" == "authPriv" ]]; then
  SENSOR2OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SENSOR2 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  SENSOR2OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SENSOR2 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  SENSOR2OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SENSOR2 2>&1)
else
  SENSOR2OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SENSOR2 2>&1)
fi

#sensor3
if [[ "$SNMPl" == "authPriv" ]]; then
  SENSOR3OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SENSOR3 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  SENSOR3OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SENSOR3 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  SENSOR3OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SENSOR3 2>&1)
else
  SENSOR3OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SENSOR3 2>&1)
fi

#sensor4
if [[ "$SNMPl" == "authPriv" ]]; then
  SENSOR4OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SENSOR4 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  SENSOR4OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SENSOR4 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  SENSOR4OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SENSOR4 2>&1)
else
  SENSOR4OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SENSOR4 2>&1)
fi

if [ $? -ne 0 ]
then
  echo "Unknown Unit status. Rechecking your flags might help."
  exit 3
fi

(( OUTPUT0=$SENSOR0OUTPUT / 100 ))
#CURRENTTEMPERATURE0
if [[ "$OUTPUT0" -ge "$CRIT" ]]; then
  CURRENTTEMPERATURE0="CRITICAL - SFP $OUTPUT0 C"
  EXITCODE=2
elif [[ "$OUTPUT0" -ge "$WARN" && "$OUTPUT0" -lt "$CRIT" ]]; then
  CURRENTTEMPERATURE0="WARNING - SFP $OUTPUT0 C"
  EXITCODE=1
elif [[ "$OUTPUT0" -lt "$WARN" ]]; then
  CURRENTTEMPERATURE0="OK - SFP $OUTPUT0 C"
  EXITCODE=0
else
  CURRENTTEMPERATURE3="Temperature Unknown"
  EXITCODE=3
fi

(( OUTPUT1=$SENSOR1OUTPUT / 100 ))
#CURRENTTEMPERATURE1
if [[ "$OUTPUT1" -ge "$CRIT" ]]; then
  CURRENTTEMPERATURE1="CRITICAL - DDR $OUTPUT1 C"
  EXITCODE=2
elif [[ "$OUTPUT1" -ge "$WARN" && "$OUTPUT1" -lt "$CRIT" ]]; then
  CURRENTTEMPERATURE1="WARNING - DDR $OUTPUT1 C"
  EXITCODE=1
elif [[ "$OUTPUT1" -lt "$WARN" ]]; then
  CURRENTTEMPERATURE1="OK - DDR $OUTPUT1 C"
  EXITCODE=0
else
  CURRENTTEMPERATURE3="Temperature Unknown"
  EXITCODE=3
fi

(( OUTPUT2=$SENSOR2OUTPUT / 100 ))
#CURRENTTEMPERATURE2
if [[ "$OUTPUT2" -ge "$CRIT" ]]; then
  CURRENTTEMPERATURE2="CRITICAL - MCP $OUTPUT2 C"
  EXITCODE=2
elif [[ "$OUTPUT2" -ge "$WARN" && "$OUTPUT2" -lt "$CRIT" ]]; then
  CURRENTTEMPERATURE2="WARNING - MCP $OUTPUT2 C"
  EXITCODE=1
elif [[ "$OUTPUT2" -lt "$WARN" ]]; then
  CURRENTTEMPERATURE2="OK - MCP $OUTPUT2 C"
  EXITCODE=0
else
  CURRENTTEMPERATURE4="Temperature Unknown"
  EXITCODE=3
fi

(( OUTPUT3=$SENSOR3OUTPUT / 100 ))
#CURRENTTEMPERATURE3
if [[ "$OUTPUT3" -ge "$CRIT" ]]; then
  CURRENTTEMPERATURE3="CRITICAL - CPU $OUTPUT3 C"
  EXITCODE=2
elif [[ "$OUTPUT3" -ge "$WARN" && "$OUTPUT3" -lt "$CRIT" ]]; then
  CURRENTTEMPERATURE3="WARNING - CPU $OUTPUT3 C"
  EXITCODE=1
elif [[ "$OUTPUT3" -lt "$WARN" ]]; then
  CURRENTTEMPERATURE3="OK - CPU $OUTPUT3 C"
  EXITCODE=0
else
  CURRENTTEMPERATURE3="Temperature Unknown"
  EXITCODE=3
fi

(( OUTPUT4=$SENSOR4OUTPUT / 100 ))
#CURRENTTEMPERATURE4
if [[ "$OUTPUT4" -ge "$CRIT" ]]; then
  CURRENTTEMPERATURE4="CRITICAL - SW $OUTPUT4 C"
  EXITCODE=2
elif [[ "$OUTPUT4" -ge "$WARN" && "$OUTPUT4" -lt "$CRIT" ]]; then
  CURRENTTEMPERATURE4="WARNING - SW $OUTPUT4 C"
  EXITCODE=1
elif [[ "$OUTPUT4" -lt "$WARN" ]]; then
  CURRENTTEMPERATURE4="OK - SW $OUTPUT4 C"
  EXITCODE=0
else
  CURRENTTEMPERATURE4="Temperature Unknown"
  EXITCODE=3
fi

echo "'$CURRENTTEMPERATURE0' '$CURRENTTEMPERATURE1' '$CURRENTTEMPERATURE2' '$CURRENTTEMPERATURE3' '$CURRENTTEMPERATURE4' | 'Temperature SFP'=$OUTPUT0;$WARN;$CRIT 'Temperature DDR'=$OUTPUT1;$WARN;$CRIT 'Temperature SW2'=$OUTPUT2;$WARN;$CRIT 'Temperature CPU'=$OUTPUT3;$WARN;$CRIT 'Temperature SW1'=$OUTPUT4;$WARN;$CRIT"
exit $EXITCODE