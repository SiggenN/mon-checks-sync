#!/bin/bash

# author    Niklas Sigg | NetNordic Sweden AB
# date      2020-09-03

print_help() {
    echo ""
    echo "This is a script for Icinga2, used for monitoring Power Supply status and voltage on iDRAC9. More specifically Poweredge R640 which utilizes 2 CPU's."
    echo "$PROGNAME Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass]"
    echo ""
}

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

IFS=$'\n'

virtualdiskname=".1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.2"
virtualdiskstate=".1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.4"
virtualdisksize=".1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.6"
displayname=".1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.36"
raidlevel=".1.3.6.1.4.1.674.10892.5.5.1.20.140.1.1.13"

if [[ "$SNMPl" == "authPriv" ]]; then
  virtualdiskname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $virtualdiskname 2>&1))
  virtualdiskstate=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $virtualdiskstate 2>&1))
  virtualdisksize=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $virtualdisksize 2>&1))
  displayname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $displayname 2>&1))
  raidlevel=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $raidlevel 2>&1))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  virtualdiskname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $virtualdiskname 2>&1))
  virtualdiskstate=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $virtualdiskstate 2>&1))
  virtualdisksize=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $virtualdisksize 2>&1))
  displayname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $displayname 2>&1))
  raidlevel=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $raidlevel 2>&1))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  virtualdiskname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $virtualdiskname 2>&1))
  virtualdiskstate=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $virtualdiskstate 2>&1))
  virtualdisksize=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $virtualdisksize 2>&1))
  displayname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $displayname 2>&1))
  raidlevel=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $raidlevel 2>&1))
else
  virtualdiskname=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $virtualdiskname))
  virtualdiskstate=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $virtualdiskstate))
  virtualdisksize=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $virtualdisksize))
  displayname=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $displayname))
  raidlevel=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $raidlevel))
fi

raidlevel_function () {
if [[ "${raidlevel[i]}" = "1" ]]; then
    echo "Raid type is none of the following; R0, R1, R5, R6, R10, R50, R60, ConcR1"
elif [[ "${raidlevel[i]}" = "2" ]]; then
    echo "Raid type = RAID-0"
elif [[ "${raidlevel[i]}" = "3" ]]; then
    echo "Raid type = RAID-1"
elif [[ "${raidlevel[i]}" = "4" ]]; then
    echo "Raid type = RAID-5"
elif [[ "${raidlevel[i]}" = "6" ]]; then
    echo "Raid type = RAID-10"
elif [[ "${raidlevel[i]}" = "7" ]]; then
    echo "Raid type = RAID-50"
elif [[ "${raidlevel[i]}" = "8" ]]; then
    echo "Raid type = RAID-60"
elif [[ "${raidlevel[i]}" = "9" ]]; then
    echo "Raid type = Concatenated RAID-1"
else
    echo "Raid type unknown"
fi
}

for (( i = 0; i < ${#virtualdiskname[@]}; i++ )); do
    printf "%s" "Display Name/FQDD = ${displayname[i]}:" $'\n'
    if [[ "${virtualdiskstate[i]}" == "2" ]]; then #Integer 2 equals to "Online"
        echo "OK - The virtual disk is operating normally or optimally."
        echo "Disk Name = ${virtualdiskname[i]}"
        raidlevel_function
        echo "Total Disk Size = $((${virtualdisksize[i]}/1000)) GBytes or ${virtualdisksize[i]} Mbytes."
        echo "________________________________________________"
        exitcode=0
    elif [[ "${virtualdiskstate[i]}" == "1" ]]; then #Integer 1 eguals to "Unknown"
        echo "Unknown - The current state could not be determined."
        echo "Disk Name = ${virtualdiskname[i]}"
        raidlevel_function
        echo "Total Disk Size = $((${virtualdisksize[i]}/1000)) GBytes or ${virtualdisksize[i]} Mbytes."
        echo "________________________________________________"
        exitcode=3
    elif [[ "${virtualdiskstate[i]}" == "3" ]]; then #Integer 3 equals to "Failed"
        echo "Critical - The virtual disk has encountered a failure. The data on disk is lost or is about to be lost."
        echo "Disk Name = ${virtualdiskname[i]}"
        raidlevel_function
        echo "Total Disk Size = $((${virtualdisksize[i]}/1000)) GBytes or ${virtualdisksize[i]} Mbytes."
        echo "________________________________________________"
        exitcode=2
    elif [[ "${virtualdiskstate[i]}" == "4" ]]; then #Integer 4 equals to "Degraded"
        echo "Critical - The virtual disk encounterd a failure with one or all of the constituent redundant physical disks. The data on the virtual disk might no longer be fault tolerant."
        echo "Disk Name = ${virtualdiskname[i]}"
        raidlevel_function
        echo "Total Disk Size = $((${virtualdisksize[i]}/1000)) GBytes or ${virtualdisksize[i]} Mbytes."
        echo "________________________________________________"
        exitcode=2
    fi
done

exit $exitcode