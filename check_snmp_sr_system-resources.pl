#!/usr/bin/perl -w

use SNMP;
use Socket;

$VERSION = "1.0";
$AUTHOR = "2015 Jonas Jacobsson / NetNordic AB";

sub print_version {
    print "check_snmp_sr_system-resources.pl version $VERSION, $AUTHOR\n\n";
}

sub print_help {
    print "check_snmp_sr_system-resources.pl is a Nagios plugin to check the card system resources\n",
    "counters in Alcatel-Lucent SR platform. SROS v12 and later may be needed.\n\n";
}

sub print_usage {
    print "Usage: check_snmp_sr_system-resources.pl <host> <community> <card> <resource> <warn> <crit>\n",
    "       check_snmp_sr_system-resources.pl -h|--help\n",
    "       check_snmp_sr_system-resources.pl -v|--version\n\n",
    "       <host>        The target host\n",
    "       <community>   The SNMP community to use\n",
    "       <card>        Card slot in SR chassis\n",
    "       <resource>    Resource to check for. Can be any of...\n",
    "         dynamic-queues\n",
    "         subscriber-hosts\n",
    "       <warn>        Warning threshold\n",
    "       <crit>        Critical threshold\n\n";
}

if (($#ARGV + 1) == 1 && ($ARGV[0] eq "-h" || $ARGV[0] eq "--help")) {
    print_version();
    print_help();
    print_usage();
    exit 0;
} elsif (($#ARGV + 1) == 1 && ($ARGV[0] eq "-v" || $ARGV[0] eq "--version")) {
    print_version();
    exit 0;
} elsif (($#ARGV + 1) != 6) {
    print_usage();
    exit 0;
}

my $HOST = $ARGV[0];
my $COMMUNITY = $ARGV[1];
my $CARD = $ARGV[2];
my $RESOURCE = $ARGV[3];
my $WARN = $ARGV[4];
my $CRIT = $ARGV[5];

my $session;
my $var;
my $vb;
my $allocated;
my $total;

&SNMP::initMib();

my %snmpparms;
$snmpparms{Community} = $COMMUNITY;
$snmpparms{DestHost} = inet_ntoa(inet_aton($HOST));
$snmpparms{Version} = '2';
$snmpparms{UseSprintValue} = '1';
$session = new SNMP::Session(%snmpparms);

if ($RESOURCE eq "dynamic-queues") {
    # Total number of dynamic queues
    $OID = ".1.3.6.1.4.1.6527.3.1.2.2.3.46.1.61.1.$CARD";
    $vb = new SNMP::Varbind([$OID,'1']);
    $total = $session->get($vb);
    if ($session->{ErrorNum}) {
	die "Got $session->{ErrorStr} querying $HOST for $OID.\n";
    }

    # Number of allocated queues
    $OID = ".1.3.6.1.4.1.6527.3.1.2.2.3.46.1.62.1.$CARD";
    $vb = new SNMP::Varbind([$OID,'1']); # '0' is the instance.
    $allocated = $session->get($vb); # Get exactly what we asked for.
    if ($session->{ErrorNum}) {
	die "Got $session->{ErrorStr} querying $HOST for $OID.\n";
    }

} elsif ($RESOURCE eq "subscriber-hosts") {
    # Total number of subscriber hosts supported
    $OID = ".1.3.6.1.4.1.6527.3.1.2.2.3.46.1.63.1.$CARD";
    $vb = new SNMP::Varbind([$OID,'1']); # '0' is the instance.
    $total = $session->get($vb); # Get exactly what we asked for.
    if ($session->{ErrorNum}) {
	die "Got $session->{ErrorStr} querying $HOST for $OID.\n";
    }

    # Allocated number of subscriber hosts
    $OID = ".1.3.6.1.4.1.6527.3.1.2.2.3.46.1.64.1.$CARD";
    $vb = new SNMP::Varbind([$OID,'1']); # '0' is the instance.
    $allocated = $session->get($vb); # Get exactly what we asked for.
    if ($session->{ErrorNum}) {
	die "Got $session->{ErrorStr} querying $HOST for $OID.\n";
    }
 
} else {
    die("No valid resource specified!\n");
}

my $percent = int(($allocated/$total)*100);

if ($allocated >= ($total - $CRIT)) {
    print "CRITICAL - $allocated used out of $total : $percent%|$RESOURCE=$allocated;".($total-$WARN).";".($total-$CRIT).";;";
    $exitval = 2;
} elsif ($allocated >= ($total - $WARN)) {
    print "WARNING - $allocated used out of $total : $percent%|$RESOURCE=$allocated;".($total-$WARN).";".($total-$CRIT).";;";
    $exitval = 1;
} else {
    print "OK - $allocated used out of $total : $percent%|$RESOURCE=$allocated;".($total-$WARN).";".($total-$CRIT).";;";
    $exitval = 0;
}

exit $exitval;

# END OF PROGRAM
