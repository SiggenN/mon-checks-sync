#!/usr/bin/perl -w
#
# check_icpe.pl
# Written by JonJac NetNordic AB

use DBI;
use DBD::mysql;
use Getopt::Long;

use warnings;

my $database = "icpelog";
my $host = "localhost";
my $userid = "icpe";
my $passwd = "icpe";

my $checktype = "";
my $mac = "";
my $node = "";
my $warning = "90";
my $critical = "80";
my $debug = 0;

GetOptions ("type|t=s"   => \$checktype,
	    "mac|m=s"  => \$mac,
	    "node|n=i" => \$node,
	    "host|h=s" => \$host,
	    "database|d=s" => \$database,
	    "user|u=s" => \$userid,
	    "password|p=s" => \$passwd,
	    "warning|w=i" => \$warning,
	    "critical|c=i" => \$critical,
	    "debug" => \$debug)
    or die("Error in command line arguments\n");

$mac = lc $mac;

unless ($mac =~ /[a-z0-9]{12}/) {
    die("Not a valid MAC address\n");
}

# invoke the ConnectToMySQL sub-routine to make the database connection
$connection = ConnectToMySql($database);


if ($checktype =~ /^battery$/) {
    # node=7 instance=0 vid=0x010f ptype=0x0c02 pid=0x1002 class=0x0080 descr=battery level=100 value=0x64
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = $node AND mac = \'${mac}\' AND cmdclass = 'battery' AND action = 'status' ORDER BY id DESC LIMIT 1";
    
    $statement = $connection->prepare($query);
    $statement->execute();
    
    
    if (@data = $statement->fetchrow_array()) {

        if ($debug) {
	    print join("#", @data);
	    print "\n";
	}

	if ($data[7] > $warning) {
	    print "OK! Battery level is: $data[7] | Level=$data[7];$warning;$critical;;\n";
	    exit 0;
	} elsif ($data[7] >= $critical) {
	    print "WARNING! Battery level is: $data[7] | Level=$data[7];$warning;$critical;;\n";
	    exit 1;
	} else {
	    print "CRITICAL! Battery level is; $data[7] | Level=$data[7];$warning;$critical;;\n";
	    exit 2;
	}
    } else {
	print "Unknown, no values fetched";
	exit 3;
    }

} elsif ($checktype =~ /temp/) {
    # node=7 instance=0 vid=0x010f ptype=0x0c02 pid=0x1002 class=0x0031 descr=msensor type=0x01 precision=1 unit=0 size=2 data=0x00f7 data32=0x00f70000
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue,message FROM log WHERE node = $node AND mac = \'${mac}\' AND cmdclass = 'msensor' AND action = 'status' ORDER by id DESC LIMIT 1"; 
    $statement = $connection->prepare($query);
    $statement->execute();
    
    
    if(@data = $statement->fetchrow_array()) {	
	if ($debug) {
	    print join("#", @data);
	    print "\n";
	}
	if ($data[7] eq '') {
	    print "Unknown, no values fetched";
	    exit 3;
	}

	my $prec = 1;
	if($data[8] =~ /precision=(\d)/) {
	    $prec = $1
	};
	my $temp = $data[7] / (10**$prec);

	if ($temp < $warning) {
	    print "OK! Temperature is: $temp | Temperature=$temp;$warning;$critical;;\n";
	    exit 0;
	} elsif ($temp < $critical) {
	    print "WARNING! Temperature is: $temp | Temperature=$temp;$warning;$critical;;\n";
	    exit 1;
	} else {
	    print "CRITICAL! Temperature is: $temp | Temperature=$temp;$warning;$critical;;\n";
	    exit 2;
	}	
    } else {
	print "Unknown, no values fetched";
	exit 3;
    }   

} elsif ($checktype =~ /powerlevel/) {
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = $node AND mac = \'${mac}\' AND cmdclass = 'msensor' AND action = 'status' ORDER by id DESC LIMIT 1";
    $statement = $connection->prepare($query);
    $statement->execute();

    if (@data = $statement->fetchrow_array()) {
	if ($debug) {
	    print join("#", @data);
	    print "\n";
	}
	if ($data[7] eq '') {
	    print "Unknown, no values fetched";
	    exit 3;
	}

	my $power=$data[7]/10;
	if ($power < $warning) {
	    print "OK! Power level is: $power | Powerlevel=$power;$warning;$critical;;\n";
	    exit 0;
	} elsif ($power < $critical) {
	    print "WARNING! Power level is: $power | Powerlevel=$power;$warning;$critical;;\n";
	    exit 1;
	} else {
	    print "CRITICAL! Power level is: $power | Powerlevel=$power;$warning;$critical;;\n";
	    exit 2;
	}
    } else {
	print "Unknown, no values fetched";
	exit 3;
    }

} elsif ($checktype =~ /powercontrol/) {
    # icpe/0x0006191B70FF/rpt/node/10/class/basic/act/status node=10 instance=0 vid=0x010f ptype=0x0600 pid=0x1000 class=0x0020 descr=basic value=0xff
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = $node AND mac = \'${mac}\' AND ack=0 AND cmdclass = 'basic' AND action = 'status' ORDER BY id DESC LIMIT 1";

    $statement = $connection->prepare($query);
    $statement->execute();

    if (@data = $statement->fetchrow_array()) {

        if ($debug) {
            print join("#", @data);
            print "\n";
        }

        # Power plug is turned off
        if ($data[7] eq "off") {
            my $timeoff = time - int($data[2]);

            if ($timeoff < $warning) {
                print "OK! : Power has been turned off in: $timeoff\n";
                exit 0;
            } elsif ($timeoff < $critical) {
                print "WARNING! Power has been turned off in: $timeoff\n";
                exit 1;
            } else {
                print "CRITICAL! Power has been turned off in: $timeoff\n";
                exit 2;
            }

        # Power plug is turned on
        } elsif ($data[7] eq "on") {
            print "OK! Power is turned on.\n";
            exit 0;
        } else {
            print "CRITICAL! Could not find power status.\n";
            exit 3;
        }
    } else {
        print "Unknown, no values fetched";
        exit 3;
    }   

} elsif ($checktype =~ /door/) {
    # node=6 instance=0 vid=0x010f ptype=0x0701 pid=0x1001 class=0x0071 descr=alarm v1alm=00 level=0 exValid=1 srcNode=0 stat=ff zalm=06 evt=17 evtType=00 almParam=
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = $node AND mac = \'${mac}\' AND ack=0 AND cmdclass = 'alarm' AND action = 'status' AND (mvalue = '16' or mvalue = '17') ORDER BY id DESC LIMIT 1";

    $statement = $connection->prepare($query);
    $statement->execute();

    if (@data = $statement->fetchrow_array()) {

	if ($debug) {
	    print join("#", @data);
	    print "\n";
	}

	# Door is open
	if ($data[7] eq "16") {
	    my $timeopen = time - int($data[2]);
	    
	    if ($timeopen < $warning) {
		print "OK! Door has been open in: $timeopen\n";
		exit 0;
	    } elsif ($timeopen < $critical) {
		print "WARNING! Door has been open in: $timeopen\n";
		exit 1;
	    } else {
		print "CRITICAL! Door has been open in: $timeopen\n";
		exit 2;
	    }
	    
	# Door is closed
	} elsif ($data[7] eq "17") {
	    print "OK! Door is closed.\n";
	    exit 0;
	} else {
	    print "CRITICAL! Could not find door status.\n";
	    exit 3;
	}
    } else {
	print "Unknown, no values fetched from db.\n";
	exit 3;
    }

} elsif ($checktype =~ /flood/) {
# node=11 instance=0 vid=0x010f ptype=0x0b01 pid=0x1002 class=0x0071 descr=alarm v1alm=05 level=2 exValid=1 srcNode=0 stat=ff zalm=05 evt=02 evtType=00 almParam=
        $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = $node AND mac = \'${mac}\' AND ack=0 AND cmdclass = 'alarm' AND action = 'status' \ AND mvalue IS NOT NULL AND mvalue != '' ORDER \
BY id DESC LIMIT 1";

	$statement = $connection->prepare($query);
	$statement->execute();

	if (@data = $statement->fetchrow_array()) {

        if ($debug) {
            print join("#", @data);
            print "\n";
        }

        # There is water on the floor!
        if ($data[7] eq "02") {
            my $timewater = time - int($data[2]);

            if ($timewater < $warning) {
                print "OK! Water present since: $timewater\n";
                exit 0;
            } elsif ($timewater < $critical) {
                print "WARNING! Water present since: $timewater\n";
                exit 1;
            } else {
                print "CRITICAL! Water present since: $timewater\n";
                exit 2;
            }

        # No water present
        } elsif ($data[7] eq "00") {
            print "OK! No water present.\n";
            exit 0;
        } else {
            print "CRITICAL! Could not find flood sensor status.\n";
            exit 3;
        }
    } else {
        print "Unknown, no values fetched from db.\n";
        exit 0;
    }

} elsif ($checktype =~ /smoke/) {
# node=7 instance=0 vid=0x010f ptype=0x0c02 pid=0x1002 class=0x0071 descr=alarm v1alm=00 level=0 exValid=1 srcNode=0 stat=ff zalm=01 evt=02 evtType=00 almParam=
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = $node AND mac = \'${mac}\' AND ack=0 AND cmdclass = 'alarm' \
AND action = 'status' AND ptype = '0c02' AND zalm = '01' ORDER BY id DESC LIMIT 1";

    $statement = $connection->prepare($query);
    $statement->execute();

    if (@data = $statement->fetchrow_array()) {

        if ($debug) {
            print join("#", @data);
            print "\n";
        }

        # Smoke detected!
        if ($data[7] eq "02") {
            my $timesmoke = time - int($data[2]);
            print "CRITICAL! Smoke present since: $timesmoke | Time=$timesmoke;;;;\n";
            exit 2;

        # No water present
        } elsif ($data[7] eq "00") {
            print "OK! No smoke detected.\n";
            exit 0;
        } elsif ($data[7] eq "03") {
            print "OK! Selftest.\n";
            exit 0;
        } else {
            print "CRITICAL! Could not find flood sensor status.\n";
            exit 3;
        }
    } else {
        print "Unknown, no values fetched from db.\n";
        exit 0;
    }

} elsif ($checktype =~ /^batterybackup$/) {
    # icpe/0x0006191B70FF/rpt/node/sys/class/alarm/act/stat 0x84 - battery
    # icpe/0x0006191B70FF/rpt/node/sys/class/alarm/act/stat 0x01 - charging
    # icpe/0x0006191B70FF/rpt/node/sys/class/alarm/act/stat 0x02 - charged
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = 'sys' AND mac = \'${mac}\' AND ack=0 AND cmdclass = 'alarm' AND action = 'stat' ORDER BY id DESC LIMIT 1";

    $statement = $connection->prepare($query);
    $statement->execute();

    if (@data = $statement->fetchrow_array()) {

	if ($debug) {
	    print join("#", @data);
	    print "\n";
	}

	my @mvalue = split(/\|/, $data[7]);

	# Set default exitlevel to "Warning"
	my $exitlevel = 1;
	my $message = "";
	my $batlevel = 0;

	# System battery is charging (main power exists)
	if ($mvalue[0] eq "ac") {
	    $message = "Main power online.";
	} else {
	    $message = "Main power offline.";
	    $exitlevel = 2;
	}

	# Do not warn if battery is > 75 %

	if ($mvalue[1] eq "batfull") {
	    $message .= " Battery full.";
	    $exitlevel = 0;
	    $batlevel = 100;
	} else {
	    if ($mvalue[1] eq "batcap100") {
		$message .= " Battery > 75% capacity.";
		$exitlevel = 0 if $exitlevel == 1;
		$batlevel = 80;
	    } elsif ($mvalue[1] eq "batcap75") {
		$message .= " Battery < 75% capacity.";
		$batlevel = 60;
	    } elsif ($mvalue[1] eq "batcap50") {
                $message .= " Battery < 50% capacity.";
		$batlevel = 30;
	    } elsif ($mvalue[1] eq "batcaplow") {
                $message .= " Battery capacity very low.";
	    } else {
		$message .= " Battery capacity unknown.";
	    }
	}

        if ($mvalue[2] eq "discharging") {
	    $message .= " Discharging.";
	} elsif ($mvalue[2] eq "charged") {
	    $message .= " Charged.";
	} elsif ($mvalue[2] eq "charging") {
	    $message .= " Charging.";
	}

	
	if ($exitlevel == 0) {
	    print "OK! " . $message . " | Level=$batlevel;$warning;$critical;;\n";
	    exit 0;
	} elsif ($exitlevel == 1) {
	    print "WARNING! " . $message . " | Level=$batlevel;$warning;$critical;;\n";
	    exit 1;
	} elsif ($exitlevel == 2) {
	    print "CRITICAL! " . $message . " | Level=$batlevel;$warning;$critical;;\n";
	    exit 2;
	} else {
	    print "Unknown status!\n";
	    exit 3;
	}

    } else {
        print "Unknown, no values fetched from db.\n";
	exit 3;
    }
} elsif ($checktype =~ /batterybackupstatus/) {
    # Simple status check if ac is online or not
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = 'sys' AND mac = \'${mac}\' AND ack=0 AND cmdclass = 'alarm' AND action = 'stat' ORDER BY id DESC LIMIT 1";

    $statement = $connection->prepare($query);
    $statement->execute();
    
    if (@data = $statement->fetchrow_array()) {
	
        if ($debug) {
            print join("#", @data);
            print "\n";
        }
	
        my @mvalue = split(/\|/, $data[7]);

        if ($mvalue[0] eq "ac") {
            print "OK! Main power is online.\n";
	    exit 0;
        } elsif ($mvalue[0] eq "noac") {
            print "CRITICAL! Main power is offline.\n";
            return 2;
        } else {
            print "UNKNOWN! Main power has unknown status.\n";
            return 3;
	}

    } else {
        print "Unknown, no values fetched from db.\n";
	exit 3;
    }

} elsif ($checktype =~ /batterybackuplevel/) {
    # icpe/0x0006191B70FF/rpt/node/sys/class/alarm/act/stat 0x84 - battery
    # icpe/0x0006191B70FF/rpt/node/sys/class/alarm/act/stat 0x01 - charging
    # icpe/0x0006191B70FF/rpt/node/sys/class/alarm/act/stat 0x02 - charged
    $query = "SELECT id,timestamp,UNIX_TIMESTAMP(timestamp),mac,node,cmdclass,ack,mvalue FROM log WHERE node = 'sys' AND mac = \'${mac}\' AND ack=0 AND cmdclass = 'alarm' AND action = 'stat' ORDER BY id DESC LIMIT 1";

    $statement = $connection->prepare($query);
    $statement->execute();

    if (@data = $statement->fetchrow_array()) {

        if ($debug) {
            print join("#", @data);
            print "\n";
        }

        my @mvalue = split(/\|/, $data[7]);

        my $exitlevel = 1;
        my $message = "";
        my $batlevel = 0;

        if ($mvalue[1] eq "batfull") {
            $message .= "Battery full.";
            $batlevel = 100;
        } else {
            if ($mvalue[1] eq "batcap100") {
                $message .= "Battery > 75% capacity.";
                $batlevel = 80;
            } elsif ($mvalue[1] eq "batcap75") {
                $message .= "Battery < 75% capacity.";
                $batlevel = 60;
            } elsif ($mvalue[1] eq "batcap50") {
                $message .= "Battery < 50% capacity.";
                $batlevel = 30;
            } elsif ($mvalue[1] eq "batcaplow") {
                $message .= "Battery capacity very low.";
		$batlevel = 5;
            } else {
                $message .= "Battery capacity unknown.";
            }
        }
        
        if ($mvalue[2] eq "discharging") {
            $message .= " Discharging.";
        } elsif ($mvalue[2] eq "charged") {
            $message .= " Charged.";
        } elsif ($mvalue[2] eq "charging") {
            $message .= " Charging.";
        }


        if ($batlevel > $warning) {
            print "OK! " . $message . " | Level=$batlevel;$warning;$critical;;\n";
            exit 0;
        } elsif ($batlevel > $critical) {
            print "WARNING! " . $message . " | Level=$batlevel;$warning;$critical;;\n";
            exit 1;
        } elsif ($batlevel > 0) {
            print "CRITICAL! " . $message . " | Level=$batlevel;$warning;$critical;;\n";
            exit 2;
        } else {
            print "UNKNOWN! Unknown backup battery status!\n";
            exit 3;
        }
    } else {
	print "Unknown, no values fetched from db.\n";
	exit 3;
    }
}

# Exit the script
print "No matching check type!\n";
exit 3;

sub ConnectToMySql {

    my ($db) = @_;

    # assign the values to your connection variable
    my $connectionInfo="dbi:mysql:$db;$host";

    # make connection to database
    my $l_connection = DBI->connect($connectionInfo,$userid,$passwd);

    # the value of this connection is returned by the sub-routine
    return $l_connection;

}
