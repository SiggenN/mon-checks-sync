#!/bin/bash

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done



output=($(snmpwalk -Oqv -v 2c -c $SNMPc $HOST .1.3.6.1.2.1.25.3.3.1.2 2>&1))
exitcode=0
if [[ "output" -ge "90" ]]; then
        echo "Critical! CPU load is over 90%, CPU Idle should be 10% or below. Verify this by logging into the switch and type 'show system load'. Load currently at $output%"
        exitcode=2
else
        echo "OK! CPU Usage is fine, cpu idle % should be above 10. Load currently at $output%"
fi
exit $exitcode