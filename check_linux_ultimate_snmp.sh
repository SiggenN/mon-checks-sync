#!/bin/bash

################
# SNMP-based host mopnitoring plugin for Nagios, by Miklos Kokenyesi
# version history
# 1.0 (2019-12-22) - SNMP error detection; check types: cpu, disk, load, mem, net, proc, procs, swap, time, uptime, users
# 1.1 (2020-02-22) - Bugfix for SNMP read error handling
# 1.2 (2020-03-10) - Adjustments to variable handling in awk commands
# 1.3 (2020-09-25) - Bugfix in CPU usage, SNMPv3 support
# 1.4 (2021-01-13) - Added perfdata for uptime //Niklas Sigg
################
about="\nSNMP-based host monitoring plugin for Nagios, version 1.3 by Miklos Kokenyesi"
usage="\nUsage: ./check_snmp_host.sh -H host -v SNMPversion (1|2c|3) -t type [-m mountpoint] [-n nic] [-p process] [-w warning] [-c critical]\n
In case SNMPversion=1 or 2c:\n
\t-C community\n
In case SNMPversion=3\n
\t-s security level (noAuthNoPriv|authNoPriv|authPriv)\n
\t-u username for authentication\n
\t-a authentication protocol (MD5|SHA)\n
\t-A authentication protocol passphrase\n
\t-x privacy protocol (DES|AES)\n
\t-X privacy protocol passphrase\n
Types:\n
\tcpu:\tCPU load percentage. Use the provided template to get a stacked graph in pnp4nagios.\n
\t\tThe stacked graph will show user, system, kernel, nice and wait times with different colors.\n
\t\tOn multi-processor systems this will display the average load of all processors.\n
\tdisk:\tDisk usage in percentage.\n
\t\tUse the -m flag to specify the mountpoint. The root partition is, for example: -m /\n
\tload:\tSystem load averages for 1 minute, 5 minutes and 15 minutes.\n
\t\tThe provided pnp4nagios template will combine the three data into a single graph.\n
\t\tWarning and critical thresholds should contain values for each of them, separated by commas.\n
\t\tFor example: -w 5,4,3 -c 9,8,7\n
\tmem:\tMemory usage (percentage). Use the provided template to get a stacked graph in pnp4nagios.\n
\t\tThe stacked graph will show used, buffer and cache memory with different colors.\n
\t\tAlerting will happen when the real usage (used memory) will reach a threshold.\n
\tnet:\tNetwork traffic (in packets). Graphs are in megabytes (MB).\n
\t\tUse the -n flag to specify the network interface you want to monitor.\n
\t\tUse the provided template to get a combined (in/out) traffic graph in pnp4nagios.\n
\t\tDon't set -w or -c, this check is just for traffic display and visualization.\n
\tproc:\tDetailed information of a process. Use the -p flag to specify the process name, for example: -p httpd\n
\t\tYou will get CPU and memory usage graphs, and can set warning/critical levels for the memory usage (in MB).\n
\t\tYou will get a critical alert if the number of running instances is 0 (zero).\n
\tprocs:\tThe total number of currently running processes.\n
\tswap:\tSwap space usage (percentage).\n
\ttime:\tTimedrift compared to the Nagios server system time.\n
\t\tWarning and critical levels are in seconds. Use only absolute numbers (for example, 5 instead of -5).\n
\tuptime:\tSystem uptime. This is just for information display, do not set -w or -c.\n
\tusers:\tThe number of active user sessions.\n"

# Nagios exit codes
statusOk=0
statusWarning=1
statusCritical=2
statusUnknown=3

displayHelp() {
	echo -e $about
	echo -e $usage
	exit $statusUnknown
}

displayError() {
	errorMsg=$1
	echo $errorMsg
	exit $statusUnknown
}

readOid() {
	oid=$1
	if [ $version = "3" ];then
		oidRead=$(snmpwalk -v $version -l $security -u $user -a $authProt -A $authPass -x $privProt -X $privPass -O vq $host $oid || displayError "SNMP read failed.")
	else
		oidRead=$(snmpwalk -v $version -c $community -O vq $host $oid || displayError "SNMP read failed.")
	fi
	case $oidRead in
		"No Such Object available on this agent at this OID")
			return 1
			;;
		"No Such Instance currently exists at this OID")
			return 1
			;;
		"SNMP read failed.")
			return 1
			;;
		*)
			echo $oidRead
			;;
	esac
}

checkThresholds() {
	warningLevel=$1
	criticalLevel=$2
	([ $warningLevel != `echo $warningLevel | grep -o '[0-9]*'` ]) && displayError "The warning threshold ($warningLevel) is not a number."
	([ $criticalLevel != `echo $criticalLevel | grep -o '[0-9]*'` ]) && displayError "The critical threshold ($criticalLevel) is not a number."
	if [[ $warningLevel -ne 0 ]] && [[ $criticalLevel -ne 0 ]];then
		[[ $warningLevel -ge $criticalLevel ]] && displayError "The warning level ($warningLevel) should be lower than the critical level ($criticalLevel)!"
	fi
	[[ $warning = 0 ]] && warning=''
	[[ $critical = 0 ]] && critical=''
}


if ([[ $1 = "--help" || $# = 0 ]]);
	then
	echo -e $about
	echo -e $usage
	exit 1;
fi

while getopts ":H:v:C:s:u:a:A:x:X:t:m:n:p:w:c:" input;
do
	case $input in
		H)	host=${OPTARG};;
		v)	version=${OPTARG};;
		C)	community=${OPTARG};;
		s)	security=${OPTARG};;
		u)	user=${OPTARG};;
		a)	authProt=${OPTARG};;
		A)	authPass=${OPTARG};;
		x)	privProt=${OPTARG};;
		X)	privPass=${OPTARG};;
		t)	type=${OPTARG};;
		m)	mountPoint=${OPTARG};;
		n)	nicName=${OPTARG};;
		p)	processName=${OPTARG};;
		w)	warning=${OPTARG};;
		c)	critical=${OPTARG};;
		*)	displayHelp;;
	esac
done

# Some error handling related to settings
[[ -z $version ]] && displayError "Please specify the SNMP version (1|2c|3) we should use."
if [[ $version = "1" || $version = "2" || $version = "2c" ]];then
	[[ -z $community ]] && displayError "Please specify the SNMP read community string."
fi
if [ $version = "3" ];then
	case $security in
		noAuthNoPriv)	security='noAuthNoPriv';;
		authNoPriv)	security='authNoPriv';;
		authPriv)	security='authPriv';;
		*)		displayError "Please specify the security level (-s) (noAuthNoPriv|authNoPriv|authPriv).";;
	esac
	[[ -z $user ]] && displayError "Please specify the username for authentication (-u)."
	case $authProt in
		MD5)	authProt='MD5';;
		SHA)	authProt='SHA';;
		*)	displayError "Please specify the authentication protocol (-a) (MD5|SHA).";;
	esac
	[[ -z $authPass ]] && displayError "Please provide an authentication passphrase (-A)."
	case $privProt in
		DES)    privProt='DES';;
		AES)    privProt='AES';;
		*)      displayError "Please specify the privacy protocol (-p) (DES|AES).";;
	esac
	[[ -z $privPass ]] && displayError "Please provide a privacy passphrase (-P)."
fi

# Sanitize warning and critical levels
if [[ $warning = '' ]] || [[ `echo $warning | grep -o '-'` = '-' ]];then warning=0;fi
if [[ $critical = '' ]] || [[ `echo $critical | grep -o '-'` = '-' ]];then critical=0;fi

case $type in
	cpu)
		cpuUser=$(readOid .1.3.6.1.4.1.2021.11.50.0) || displayError "SNMP error: OID not found."
		cpuNice=$(readOid .1.3.6.1.4.1.2021.11.51.0) || displayError "SNMP error: OID not found."
		#cpuSystem=$(readOid .1.3.6.1.4.1.2021.11.52.0) || displayError "SNMP error: OID not found." #this is a calculated value
		cpuIdle=$(readOid .1.3.6.1.4.1.2021.11.53.0) || displayError "SNMP error: OID not found."
		cpuWait=$(readOid .1.3.6.1.4.1.2021.11.54.0) || displayError "SNMP error: OID not found."
		cpuSystem=$(readOid .1.3.6.1.4.1.2021.11.55.0) || displayError "SNMP error: OID not found." #this is caled as kernel in the MIB
		cpuInterrupt=$(readOid .1.3.6.1.4.1.2021.11.56.0) || displayError "SNMP error: OID not found."
		cpuTotal=$((${cpuUser}+${cpuNice}+${cpuSystem}+${cpuIdle}+${cpuWait}+${cpuInterrupt}))
		cpuUsage=$((${cpuTotal}-${cpuIdle}))
		cpuUsagePct=$(awk -v cpuUsage="$cpuUsage" -v cpuTotal="$cpuTotal" 'BEGIN {printf "%.1f", cpuUsage*100/cpuTotal}')
		cpuUserPct=$(awk -v cpuUser="$cpuUser" -v cpuTotal="$cpuTotal" 'BEGIN {printf "%.1f", cpuUser*100/cpuTotal}')
		cpuNicePct=$(awk -v cpuNice="$cpuNice" -v cpuTotal="$cpuTotal" 'BEGIN {printf "%.1f", cpuNice*100/cpuTotal}')
		cpuSystemPct=$(awk -v cpuSystem="$cpuSystem" -v cpuTotal="$cpuTotal" 'BEGIN {printf "%.1f", cpuSystem*100/cpuTotal}')
		cpuIdlePct=$(awk -v cpuIdle="$cpuIdle" -v cpuTotal="$cpuTotal" 'BEGIN {printf "%.1f", cpuIdle*100/cpuTotal}')
		cpuWaitPct=$(awk -v cpuWait="$cpuWait" -v cpuTotal="$cpuTotal" 'BEGIN {printf "%.1f", cpuWait*100/cpuTotal}')
		checkThresholds $warning $critical
		cpuUsagePctInt=$(echo $cpuUsagePct | sed 's/\..*//')
		echo "CPU usage: ${cpuUsagePct}%|User=$cpuUserPct Nice=$cpuNicePct System=$cpuSystemPct Idle=$cpuIdlePct Wait=$cpuWaitPct Thresholds=0;$warning;$critical;0;100"
		([[ $critical -gt 0 && $cpuUsagePctInt -ge $critical ]]) && exit $statusCritical
		([[ $warning -gt 0 && $cpuUsagePctInt -ge $warning ]]) && exit $statusWarning
		exit $statusOk
		;;
	disk)
		[[ -z $mountPoint ]] && displayError "Please specify the mountpoint using the -m flag, for example: -m /"
		[[ `echo $mountPoint | grep -o '^.'` != '/' ]] && displayError "\"$mountPoint\" does not look like a mountpoint."
		targetId=$(snmpwalk -v 2c -c $community -O n $host .1.3.6.1.2.1.25.2.3.1.3 | grep 'STRING: '$mountPoint'$' | sed 's/ = STRING: .*//' | grep -o '\.[0-9]'*$ | sed 's/\.//')
		if [ -z $targetId ];then
			echo "Could not find \"$mountPoint\" in SNMP. This is what I found:"
			if [ $version = "3" ];then
				snmpwalk -v $version -l $security -u $user -a $authProt -A $authPass -x $privProt -X $privPass -O vq $host .1.3.6.1.2.1.25.2.3.1.3 | grep '^/'
			else
				snmpwalk -v $version -c $community -O vq $host .1.3.6.1.2.1.25.2.3.1.3 | grep '^/'
			fi
			displayError "Pick one of these."
		fi
		diskTotal=$(readOid .1.3.6.1.2.1.25.2.3.1.5.$targetId) || displayError "SNMP error: OID not found."
		diskUsage=$(readOid .1.3.6.1.2.1.25.2.3.1.6.$targetId) || displayError "SNMP error: OID not found."
		diskUsagePct=$(awk -v diskUsage="$diskUsage" -v diskTotal="$diskTotal" 'BEGIN {printf "%.1f", diskUsage*100/diskTotal}')
		checkThresholds $warning $critical
		diskUsagePctInt=$(echo $diskUsagePct | sed 's/\..*//')
		echo "Disk usage of $mountPoint: ${diskUsagePct}%|'Disk usage of $mountPoint'=${diskUsagePct}%;$warning;$critical;0;100"
		([[ $critical -gt 0 && $diskUsagePctInt -ge $critical ]]) && exit $statusCritical
		([[ $warning -gt 0 && $diskUsagePctInt -ge $warning ]]) && exit $statusWarning
		exit $statusOk
		;;
	load)
		load1=$(readOid .1.3.6.1.4.1.2021.10.1.3.1) || displayError "SNMP error: OID not found."
		load5=$(readOid .1.3.6.1.4.1.2021.10.1.3.2) || displayError "SNMP error: OID not found."
		load15=$(readOid .1.3.6.1.4.1.2021.10.1.3.3) || displayError "SNMP error: OID not found."
		load1Max=$(readOid .1.3.6.1.4.1.2021.10.1.4.1) || displayError "SNMP error: OID not found."
		load5Max=$(readOid .1.3.6.1.4.1.2021.10.1.4.2) || displayError "SNMP error: OID not found."
		load15Max=$(readOid .1.3.6.1.4.1.2021.10.1.4.3) || displayError "SNMP error: OID not found."
		if [ -n $warning ];then
			warningLevels=$(echo $warning | tr ',' ' ')
			read -a warningArray <<< $warningLevels
			load1Warning=${warningArray[0]}
			load5Warning=${warningArray[1]}
			load15Warning=${warningArray[2]}
			([[ $load1Warning == "" || $load5Warning == "" || $load15Warning == "" ]]) && displayError "The warning threshold must be given for each load level, separated by commas (for example: -w 5,4,3)"
		fi
		if [ -n $critical ];then
			criticalLevels=$(echo $critical | tr ',' ' ')
			read -a criticalArray <<< $criticalLevels
			load1Critical=${criticalArray[0]}
			load5Critical=${criticalArray[1]}
			load15Critical=${criticalArray[2]}
			([[ $load1Critical == "" || $load5Critical == "" || $load15Critical == "" ]]) && displayError "The critical threshold must be given for each load level, separated by commas (for example: -w 9,8,7)"
		fi
		checkThresholds $load1Warning $load1Critical
		checkThresholds $load5Warning $load5Critical
		checkThresholds $load15Warning $load15Critical
		load1Int=$(echo $load1 | grep -o '^[0-9]*')
		load5Int=$(echo $load5 | grep -o '^[0-9]*')
		load15Int=$(echo $load15 | grep -o '^[0-9]*')
		echo "Average load levels (1/5/15 minutes): $load1, $load5, $load15|'Average load - 1 minute'=$load1;$load1Warning;$load1Critical;;$load1Max 'Average load - 5 minutes'=$load5;$load5Warning;$load5Critical;;$load5Max 'Average load - 15 minutes'=$load15;$load15Warning;$load15Critical;;$load15Max"
		([ $load1Int -ge $load1Critical -o $load5Int -ge $load5Critical -o $load15Int -ge $load15Critical ]) && exit $statusCritical
		([ $load1Int -ge $load1Warning -o $load5Int -ge $load5Warning -o $load15Int -ge $load15Warning ]) && exit $statusWarning
		exit $statusOk
		;;
	mem)
		memTotal=$(readOid .1.3.6.1.2.1.25.2.3.1.5.1) || displayError "SNMP error: OID not found."
		memUsed=$(readOid .1.3.6.1.2.1.25.2.3.1.6.1) || displayError "SNMP error: OID not found."
		memBuffer=$(readOid .1.3.6.1.2.1.25.2.3.1.6.6) || displayError "SNMP error: OID not found."
		memCache=$(readOid .1.3.6.1.2.1.25.2.3.1.6.7) || displayError "SNMP error: OID not found."
		memReal=$((${memUsed}-${memBuffer}-${memCache}))
		memUsedPct=$((${memUsed}*100/${memTotal}))
		memBufferPct=$(awk -v memBuffer="$memBuffer" -v memTotal="$memTotal" 'BEGIN {printf "%.1f", memBuffer*100/memTotal}')
		memCachePct=$(awk -v memCache="$memCache" -v memTotal="$memTotal" 'BEGIN {printf "%.1f", memCache*100/memTotal}')
		memRealPct=$(awk -v memReal="$memReal" -v memTotal="$memTotal" 'BEGIN {printf "%.1f", memReal*100/memTotal}')
		checkThresholds $warning $critical
		memRealPctInt=$(echo $memRealPct | sed 's/\..*//')
		echo "Used memory: ${memRealPct}%|Used=$memRealPct Buffer=$memBufferPct Cache=$memCachePct Thresholds=0;$warning;$critical;0;100"
		([[ $critical -gt 0 && $memRealPctInt -ge $critical ]]) && exit $statusCritical
		([[ $warning -gt 0 && $memRealPctInt -ge $warning ]]) && exit $statusWarning
		exit $statusOk
		;;
	net)
		[[ -z $nicName ]] && displayError "Please specify a network interface name using the -n flag, for example: -n eth0"
		if [ $version = "3" ];then
			targetId=$(snmpwalk -v $version -l $security -u $user -a $authProt -A $authPass -x $privProt -X $privPass -O n $host .1.3.6.1.2.1.31.1.1.1.1 | grep 'STRING: '$nicName'$' | sed 's/ = STRING: .*//' | grep -o '\.[0-9]'*$ | sed 's/\.//')
		else
			targetId=$(snmpwalk -v $version -c $community -O n $host .1.3.6.1.2.1.31.1.1.1.1 | grep 'STRING: '$nicName'$' | sed 's/ = STRING: .*//' | grep -o '\.[0-9]'*$ | sed 's/\.//')
		fi
		if [ -z $targetId ];then
			echo "Could not find \"$nicName\" in SNMP. This is what I found:"
			if [ $version = "3" ];then
				snmpwalk -v $version -l $security -u $user -a $authProt -A $authPass -x $privProt -X $privPass -O vq $host .1.3.6.1.2.1.31.1.1.1.1
			else
				snmpwalk -v $version -c $community -O vq $host .1.3.6.1.2.1.31.1.1.1.1
			fi
			displayError "Pick one of these."
		fi
		bytesIn=$(readOid .1.3.6.1.2.1.31.1.1.1.6.$targetId)
		bytesOut=$(readOid .1.3.6.1.2.1.31.1.1.1.10.$targetId)
		([ $bytesIn -ge 1024 ]) && counterTotalIn="$(awk 'BEGIN {printf "%.1f", '$bytesIn'/1024}') KB"
		([ $bytesIn -ge $((1024*1024)) ]) && counterTotalIn="$(awk 'BEGIN {printf "%.2f", '$bytesIn'/1024/1024}') MB"
		([ $bytesIn -ge $((1024*1024*1024)) ]) && counterTotalIn="$(awk 'BEGIN {printf "%.2f", '$bytesIn'/1024/1024/1024}') GB"
		([ $bytesIn -ge $((1024*1024*1024*1024)) ]) && counterTotalIn="$(awk 'BEGIN {printf "%.3f", '$bytesIn'/1024/1024/1024/1024}') TB"
		([ $bytesIn -ge $((1024*1024*1024*1024*1024)) ]) && counterTotalIn="$(awk 'BEGIN {printf "%.3f", '$bytesIn'/1024/1024/1024/1024/1024}') PB"
		([ $bytesOut -ge 1024 ]) && counterTotalOut="$(awk 'BEGIN {printf "%.1f", '$bytesOut'/1024}') KB"
		([ $bytesOut -ge $((1024*1024)) ]) && counterTotalOut="$(awk 'BEGIN {printf "%.2f", '$bytesOut'/1024/1024}') MB"
		([ $bytesOut -ge $((1024*1024*1024)) ]) && counterTotalOut="$(awk 'BEGIN {printf "%.2f", '$bytesOut'/1024/1024/1024}') GB"
		([ $bytesOut -ge $((1024*1024*1024*1024)) ]) && counterTotalOut="$(awk 'BEGIN {printf "%.3f", '$bytesOut'/1024/1024/1024/1024}') TB"
		([ $bytesOut -ge $((1024*1024*1024*1024*1024)) ]) && counterTotalOut="$(awk 'BEGIN {printf "%.3f", '$bytesOut'/1024/1024/1024/1024/1024}') PB"
		#nicSpeed=$(readOid .1.3.6.1.2.1.31.1.1.1.15.$targetId)
		megabytesIn=$(($bytesIn/1024))
		megabytesOut=$(($bytesOut/1024))
		echo "Network traffic counters on $nicName: $counterTotalIn in, $counterTotalOut out|In=${megabytesIn}c Out=${megabytesOut}c"
		exit $statusOk
		;;
	proc)
		[[ -z $processName ]] && displayError "Please specify the process name using the -p flag, for example: -p httpd"
		if [ $version = "3" ];then
			pids=$(snmpwalk -v $version -l $security -u $user -a $authProt -A $authPass -x $privProt -X $privPass -O n $host .1.3.6.1.2.1.25.4.2.1.2 | grep \""$processName"\" | sed 's/ = STRING:.*//' | grep -o '[0-9]*$')
		else
			pids=$(snmpwalk -v $version -c $community -O n $host .1.3.6.1.2.1.25.4.2.1.2 | grep \""$processName"\" | sed 's/ = STRING:.*//' | grep -o '[0-9]*$')
		fi
		if [ -z "$pids" ];then
			echo "I could not find a process called \"$processName\"."
			if [ $version = "3" ];then
				similar=$(snmpwalk -v $version -l $security -u $user -a $authProt -A $authPass -x $privProt -X $privPass -O vq $host .1.3.6.1.2.1.25.4.2.1.2 | grep "$processName" | sed 's/\"//g')
			else
				similar=$(snmpwalk -v $version -c $community -O vq $host .1.3.6.1.2.1.25.4.2.1.2 | grep "$processName" | sed 's/\"//g')
			fi
			if [ -n "$similar" ];then
				echo "Perhaps you meant:"
				echo $similar | tr ' ' '\n' | sort | uniq
			else
				echo "Here is a list of currently running processes:"
				snmpwalk -v 2c -c $community -O vq $host .1.3.6.1.2.1.25.4.2.1.2 | sort | uniq
			fi
			displayError "Please enter the exact name of the process."
		fi
		instances=$(echo $pids | wc -w)
		if [ $version = "3" ];then
			cpuTimeList=$(snmpwalk -v $version -l $security -u $user -a $authProt -A $authPass -x $privProt -X $privPass -O n $host .1.3.6.1.2.1.25.5.1.1.1 | sed 's/^\.1\.3\.6\.1\.2\.1\.25\.5\.1\.1\.1\.//g' | sed 's/ = INTEGER: /,/g')
			memUsageList=$(snmpwalk -v $version -l $security -u $user -a $authProt -A $authPass -x $privProt -X $privPass -O n $host .1.3.6.1.2.1.25.5.1.1.2 | sed 's/^\.1\.3\.6\.1\.2\.1\.25\.5\.1\.1\.2\.//g' | sed 's/ = INTEGER: /,/g' | sed 's/ KBytes//g')
		else
			cpuTimeList=$(snmpwalk -v $version -c $community -O n $host .1.3.6.1.2.1.25.5.1.1.1 | sed 's/^\.1\.3\.6\.1\.2\.1\.25\.5\.1\.1\.1\.//g' | sed 's/ = INTEGER: /,/g')
			memUsageList=$(snmpwalk -v $version -c $community -O n $host .1.3.6.1.2.1.25.5.1.1.2 | sed 's/^\.1\.3\.6\.1\.2\.1\.25\.5\.1\.1\.2\.//g' | sed 's/ = INTEGER: /,/g' | sed 's/ KBytes//g')
		fi
		cpuTime=0
		memUsage=0
		for pid in $pids;do
			cpuTimePid=$(echo $cpuTimeList | tr ' ' '\n' | grep "^$pid," | awk -F "," '{print $2}')
			cpuTime=$(($cpuTime+$cpuTimePid))
			memUsagePid=$(echo $memUsageList | tr ' ' '\n' | grep "^$pid," | awk -F "," '{print $2}')
			memUsagePidMB=$(awk -v memUsagePid="$memUsagePid" 'BEGIN {printf "%.1f", memUsagePid/1024}')
			memUsage=$(awk -v memUsage="$memUsage" -v memUsagePidMB="$memUsagePidMB" 'BEGIN {printf "%.1f", memUsage+memUsagePidMB}')
		done
		memUsageInt=$(awk -v memUsage="$memUsage" 'BEGIN {printf "%.0f", memUsage}')
		memUsageHR="$memUsage MB"
		([ $memUsageInt -ge 1024 ]) && memUsageHR="$(awk -v memUsage="$memUsage" 'BEGIN {printf "%.2f", memUsage/1024}') GB"
		([ $memUsageInt -ge $((1024*1024)) ]) && memUsageHR="$(awk -v memUsage="$memUsage" 'BEGIN {printf "%.3f", memUsage/1024/1024}') TB"
		checkThresholds $warning $critical
		echo "Process $processName runs in $instances instance"`([[ $instances -ge 2 ]]) && echo s`", using $memUsageHR memory|'CPU times'=${cpuTime}c 'Memory usage'=${memUsage}MB;$warning;$critical"
		([[ $instances -lt 0 ]]) && exit $statusCritical
		([[ $critical -gt 0 && $memUsageInt -ge $critical ]]) && exit $statusCritical
		([[ $warning -gt 0 && $memUsageInt -ge $warning ]]) && exit $statusWarning
		exit $statusOk
		;;
	procs)
		processes=$(readOid .1.3.6.1.2.1.25.1.6.0) || displayError "SNMP error: OID not found."
		checkThresholds $warning $critical
		echo "Running processes: $processes|'Running processes'=$processes;$warning;$critical"
		([[ $critical -gt 0 && $processes -ge $critical ]]) && exit $statusCritical
		([[ $warning -gt 0 && $processes -ge $warning ]]) && exit $statusWarning
		exit $statusOk
		;;
	swap)
		swapTotal=$(readOid .1.3.6.1.2.1.25.2.3.1.5.10) || displayError "SNMP error: OID not found."
		swapUsed=$(readOid .1.3.6.1.2.1.25.2.3.1.6.10) || displayError "SNMP error: OID not found."
		swapUsedPct=$(awk -v swapUsed="$swapUsed" -v swapTotal="$swapTotal" 'BEGIN {printf "%.1f", swapUsed*100/swapTotal}')
		checkThresholds $warning $critical
		swapUsedPctInt=$(echo $swapUsedPct | sed 's/\..*//')
		echo "Used swap space: ${swapUsedPct}%|'Used swap space'=${swapUsedPct}%;$warning;$critical;0;100"
		([[ $critical -gt 0 && $swapUsedPctInt -ge $critical ]]) && exit $statusCritical
		([[ $warning -gt 0 && $swapUsedPctInt -ge $warning ]]) && exit $statusWarning
		exit $statusOk
		;;
	time)
		timeNagios=$(date +%s)
		timeHost=$(readOid .1.3.6.1.2.1.25.1.2) || displayError "SNMP error: OID not found."
		timeHostSeconds=$(date -d "`echo $timeHost | sed 's/,/ /g'`" +%s)
		timeDrift=$((${timeNagios}-${timeHostSeconds}))
		timeDriftAbs=$(echo $timeDrift | grep -o '[0-9]*')
		([ $timeDriftAbs -eq 1 ]) && sec="second" || sec="seconds"
		checkThresholds $warning $critical
		echo "Time difference compared to Nagios: $timeDrift $sec|Timedrift=${timeDriftAbs}s;$warning;$critical"
		([[ $critical -gt 0 && $timeDriftAbs -ge $critical ]]) && exit $statusCritical
		([[ $warning -gt 0 && $timeDriftAbs -ge $warning ]]) && exit $statusWarning
		exit $statusOk
		;;
	uptime)
		IFS=$':'
		uptime=$(readOid .1.3.6.1.2.1.25.1.1) || displayError "SNMP error: OID not found."
		unset IFS
		read -r -a int <<< $uptime
		echo "System uptime: ${int[0]} days, ${int[1]} hours, ${int[2]} minutes and ${int[3]} seconds. | Days=${int[0]} Hours=${int[1]} Minutes=${int[2]} Seconds=${int[3]}"
		exit $statusOk
		;;
	users)
		users=$(readOid .1.3.6.1.2.1.25.1.5) || displayError "SNMP error: OID not found."
		checkThresholds $warning $critical
		echo "Users logged in: $users|'Users logged in'=$users;$warning;$critical"
		([[ $critical -gt 0 && $users -ge $critical ]]) && exit $statusCritical
		([[ $warning -gt 0 && $users -ge $warning ]]) && exit $statusWarning
		exit $statusOk
		;;
	*)
		displayHelp
		;;
esac