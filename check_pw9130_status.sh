#!/bin/bash

PROGNAME="check_pw9130_status"
print_help() {
echo "$PROGNAME is a Icinga plugin to check ABM status on Eaton Powerware 9130"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST]"
}

declare -i OUTP

OID=".1.3.6.1.4.1.534.1.2.5.0"	#status

SNMPv="1"	#SNMP Version
SNMPc=$1	#SNMP Community
HOST=$2		#HOSTIP

OUTP=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $OID`

case $OUTP in
  5)
    EXITCODE=3
    STATUS="Battery Status Unknown"
    ;;
  4)
    EXITCODE=0
    STATUS="Battery Resting"
    ;;
  3)
    EXITCODE=0
    STATUS="Battery Floating"
    ;;
  2)
    EXITCODE=1
    STATUS="Battery Discharging"
    ;;
  1)
    EXITCODE=0
    STATUS="Battery Charging"
    ;;
  *)
    EXITCODE=3
    STATUS="Error!"
    ;;
esac

echo "$STATUS"
exit $EXITCODE