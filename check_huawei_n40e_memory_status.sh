#!/bin/bash

PROGNAME="check_huawei_n40e_memory_status"
print_help() {
        echo "$PROGNAME is a Nagios plugin to check memory status on Huawei N40E-X3 may work on other models"
        echo ""
        echo "Usage: $PROGNAME [SNMP Community] [HOST] [WARNING MEMORY] [CRITICAL MEMORY]"
}

declare -a OID=('16842753' '16842754' '17039361' '17039362' '17104897' '17104898' '17432577')
declare -a MEMORY
declare -a SENSORNAME

OIDINDEXMAX=$(expr ${#OID[@]} - 1)

BASEMEMORYOID=".1.3.6.1.4.1.2011.5.25.31.1.1.1.1.36."
BASENAMEOID=".1.3.6.1.2.1.47.1.1.1.1.7."
EXITCODE=0

SNMPv="2c"      #SNMP Version
SNMPc=$1        #SNMP Community
HOST=$2         #HOST
WARN=$3         #Warning memory
CRIT=$4         #Critical memory

for (( i=0; i<$OIDINDEXMAX; i++ )); do
        MEMORY[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASEMEMORYOID${OID[$i]}`
        SENSORNAME[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASENAMEOID${OID[$i]}`

        if [[ "${MEMORY[$i]}" =~ ^[0-9]+$ ]]
        then

                        if [[ "${MEMORY[$i]}" -gt "$CRIT" ]]
                        then
                                MEMORYSTATUS="CRITICAL - "
                EXITCODE=2

                        elif [[ "${MEMORY[$i]}" -gt "$WARN" ]]  && [[ $EXITCODE -ne 2 ]]
                        then
                MEMORYSTATUS="WARNING - "
                EXITCODE=1

                        elif [[ "${MEMORY[$i]}" -le "$WARN" ]] && [[ $EXITCODE -eq 0 ]]
                        then
                MEMORYSTATUS="OK - "
                EXITCODE=0
                        fi

                        MEMORYACTIVE="$MEMORYACTIVE ${SENSORNAME[$i]}: ${MEMORY[$i]} % "
                        GRAPHACTIVE="$GRAPHACTIVE ${SENSORNAME[$i]}(Memory%)=${MEMORY[$i]};$WARN;$CRIT;; "
                fi
done

echo "$MEMORYSTATUS$MEMORYACTIVE|$GRAPHACTIVE"
exit $EXITCODE