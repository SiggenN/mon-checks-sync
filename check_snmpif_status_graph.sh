#!/bin/sh


PROGNAME="check_snmpif_status_graph"

print_help() {
echo "$PROGNAME is a icinga plugin to check interface status and graph output"
echo ""
}

PLUGIN_DIR="/usr/lib64/nagios/plugins/"
for i in $*; do

        cmd+=" $i"
done

CHECKCMD=$($PLUGIN_DIR/check_snmpif $cmd)
CHECKRES=$?

if [[ $CHECKRES -eq 0 ]]
then
        GRAPVAL=0
else
        GRAPVAL=1
fi

echo "$CHECKCMD|Status=$GRAPVAL;;1;;"
exit $CHECKRES

