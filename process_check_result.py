#!/usr/bin/env python3
'''
this script is intended for use with snmptt to update an icinga service.
i.e. snmptt processes a snmp trap and executes this script, passing variables
from the trap to it
'''
import requests
import urllib3
import json
import argparse
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def get_args():
    '''
    help function get arguments from command line
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument("-s","--service",
                        help="icinga service to update",
                       )
    parser.add_argument("-i","--ip",
                        help="IP of icinga host",
                       )
    parser.add_argument("-o","--output",
                        help="output to write to icinga",
                        default=''
                       )
    parser.add_argument("-u","--user",
                        help="icinga api user",
                        default="process_traps",
                       )
    parser.add_argument("-p","--password",
                        help="icinga api user password",
                       )
    parser.add_argument("-d","--data",
                        help="trap variable",
                       )
    parser.add_argument("-t","--translation",
                        help="json with a map from trap variable to icinga state",
                        default='{"Battery is in discharging status": "2", "Battery charger is in charging status": "0"}'
                       )
    return parser.parse_args()

def process_result(body,user,password,host='localhost'):
    '''
    make rest api call to icinga updating service
    '''
    url = 'https://{0}:5665/v1/actions/process-check-result'.format(host)
    headers = {
        'Accept': 'application/json',
        }
    req = requests.post(url=url,
                        auth=(user,password),
                        verify=False,
                        data=json.dumps(body),
                        headers=headers,
                       )
    print(req.text) #print result message from icinga, not really necessary (just for testing)

def main():
    #get arguments passed to script
    args = get_args()

    #load the translation and get the icinga state from the dict using trap variable as key
    #note that we can only handle a single variable atm, if traps are more complicated a new script is needed
    translation_map = json.loads(args.translation)
    icinga_state = translation_map.get(str(args.data))
    if not icinga_state:
        return

    #prepare filter to find the correct host and service in icinga
    filter = 'host.address == "{}" && service.name == "{}"'.format(args.ip, args.service)

    #select output to service. if an output was not passed to script, take the trap data instead
    if args.output:
        icinga_output = args.output
    else:
        icinga_output = args.data

    #prep json body and make request to icinga to update service
    update_body = {"type": "Service",
                   "filter": filter,
                   "exit_status": icinga_state,
                   "plugin_output": icinga_output,
                  }
    process_result(update_body,
                   args.user,
                   args.password,
                  )

if __name__ == '__main__':
    main()