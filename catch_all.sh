#!/bin/bash

query=$(find /opt/packetfront/becs3/roots/cjm/ -type f -name "*.lic" ! -name ".*" -exec ls -Ahtr {} \; | tail -1)
validto=$(grep "valid_to" $query | cut -d '"' -f '4-' | cut -d '"' -f '1')
today=$(date -d now+30days +%s)                   #Adds 30 days to todays and formats output in seconds
validto1=$(date -d $validto +%s)                  #Formats license expiration date in seconds
validto=$(date -d $validto +"%b %d %Y")           #Formats license expiration date in human readable
#today=$(date -d now +"%b %m %Y")

if [ "$validto1" -le "$today" ]; then
            echo "CRITICAL - License expires in less than 30 days at $validto"
            exit 2
    elif [ "$validto1" -gt "$today" ]; then
            echo "OK - More than 30 days until license expire at $validto"
            exit 0
    else
            echo "Unknown, somethings wrong"
            exit 3
fi

[root@ok-bc2s custom]# ^C
[root@ok-bc2s custom]# cat catch_all.sh 
#!/bin/sh



function checkfile {
cores=
returnvalue=0
#echo $(find /opt/packetfront/becs3/roots/sm/ibos-*/core -type d)
for arg in `find /opt/packetfront/becs3/roots/sm/ibos-*/core -type d`
do
        #match folders NOT named ibos-asr nor ibos-any
        if echo $arg | grep -v -q 'ibos-asr[456]' && echo $arg | grep -v -q 'ibos-any' 
        then
                if find $arg -maxdepth 0 -empty | read
                then
                        echo "empty" > /dev/null 2>&1
                else
                        returnvalue=1
                        coredirs="${coredirs} $arg"
                        for file in `ls $arg`
                        do 
                                filepath=$arg/$file
                                cores="${cores} $filepath"
                        done
                fi
        fi

done


if [ $returnvalue -eq 1 ]
then
        echo "STATUS WARNING! cores with malformed names found in: $coredirs"
else
        echo "STATUS OK! No cores found!"
fi

return $returnvalue


}

checkfile