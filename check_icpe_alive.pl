#!/usr/bin/perl -w
#
# check_icpe_alive.pl
# This check will look for icpe status in db.
# Written by JonJac NetNordic AB 2017

use DBI;
use DBD::mysql;
use Getopt::Long;

use warnings;

my $database = "icpelog";
my $host = "localhost";
my $userid = "icpe";
my $passwd = "icpe";

my $mac = "";
my $warning = "90";
my $critical = "80";
my $debug = 0;

GetOptions ("mac|m=s"  => \$mac,
	    "host|h=s" => \$host,
	    "database|d=s" => \$database,
	    "user|u=s" => \$userid,
	    "password|p=s" => \$passwd,
	    "warning|w=i" => \$warning,
	    "critical|c=i" => \$critical,
	    "debug" => \$debug)
    or die("Error in command line arguments\n");

$mac = uc $mac;

unless ($mac =~ /[a-fA-F0-9]{12}/) {
    die("Not a valid MAC address\n");
}

# invoke the ConnectToMySQL sub-routine to make the database connection
$connection = ConnectToMySql($database);


$query = "SELECT id,mac,ipaddress,network,status,lastupdate FROM log WHERE mac = '$mac' ORDER BY id DESC LIMIT 1";
    
$statement = $connection->prepare($query);
$statement->execute();
    
    
if (@data = $statement->fetchrow_array()) {

    if ($debug) {
	print join("#", @data);
	print "\n";
    }
    
    if ($data[4] eq "up") {
	print "OK! Device is alive and have ip address $data[2]. Last checked $data[5].\n";
	exit 0;
    } else {
	print "CRITICAL! Device seem to be down. Latest ip address was $data[2]. Last checked $data[5].\n";
	exit 2;
    }
    
}

# Exit the script
print "Could not find device in db!\n";
exit 3;

sub ConnectToMySql {

    my ($db) = @_;

    # assign the values to your connection variable
    my $connectionInfo="dbi:mysql:$db;$host";

    # make connection to database
    my $l_connection = DBI->connect($connectionInfo,$userid,$passwd);

    # the value of this connection is returned by the sub-routine
    return $l_connection;

}
