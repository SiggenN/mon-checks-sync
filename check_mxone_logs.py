#!/usr/bin/env python3

from elasticsearch import Elasticsearch
import argparse
import sys

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--servers",
                  nargs='+',
                  help="<Required> elasticsearch servers to query",
                  required=True)
    parser.add_argument("-i", "--index",
                  help="<Required> elasticsearch index to query",
                  required=True)
    parser.add_argument("-f", "--faultcode",
                  help="<Required> fault code of the mxone alarm",
                  required=True)
    parser.add_argument("-d", "--domain",
                  help="<Required> domain of the mxone alarm",
                  required=True)
    parser.add_argument("-n", "--name",
                  help="hostname of the mxone server",
                  default='kk-te-mx01')
    args=parser.parse_args()
    fault_body = {
                  "query": {
                      "bool": {
                          "must": [
                              {
                                  "term": {
                                      "fault_code":args.faultcode
                                  }
                              },
                              {
                                  "term": {
                                      "alarm_domain":args.domain
                                  }
                              },
                              {
                                  "match": {
                                      "syslog_hostname":args.name
                                  }
                              },
                          ],
                          "must_not": {
                              "term": {
                                  "tags": "handled"
                                  }
                              }
                          }
                      }
                  }
    es = Elasticsearch(args.servers)
    output = ""
    alarm_cnt = 0
    result_fault = es.search(index=args.index, body=fault_body)
    #print("Got %d Hits:" % result_fault['hits']['total'])
    if result_fault['hits']['total'] == 0:
        print("OK: No unhandled alarms found \n")
    else:
        for hit in result_fault['hits']['hits']:
            #print("%(syslog_message)s \n" % hit["_source"])
            handle = hit["_source"]["handle"]
            #print(hit["_source"]["handle"] + "\n")
            handle_body = {
                  "query": {
                      "bool": {
                          "must": {
                              "match": {
                                  "handle":handle
                                  }
                              },
                          "filter": {
                                "range": {
                                    "@timestamp": {
                                        "gt": hit["_source"]["@timestamp"]
                                        }
                                    }
                                }
                          }
                      }
                  }
            result_clear = es.search(index=args.index, body=handle_body)
            if result_clear['hits']['total'] >= 1:
                #print("found matching clear for handle " + hit["_source"]["handle"] + "timestamp " + hit["_source"]["@timestamp"] + "\n")
                update_body = {
                        "doc": {
                            "tags": "handled"
                        }
                    }
                es.update(index=hit["_index"], id=hit["_id"], body=update_body, refresh=True, doc_type="doc")
            else:
                alarm_cnt += 1
                output = output + "%(syslog_timestamp)s %(syslog_message)s \n" % hit["_source"]
                #print("ouch, no match for handle:"+ hit["_source"]["handle"] + "\n")
    if alarm_cnt > 0:
        print("CRITICAL: found " + str(alarm_cnt) + " alarms \n" + output)
        sys.exit(2)

if __name__ == "__main__":
    main()
