#!/bin/bash

PROGNAME="check_multicast_packets"
AUTHOR="Vanessa Siskina 2018/04 NetNordic Soluitions AB"
VERSION="Version 2.1"

print_version() {
        echo "$VERSION $AUTHOR"
}

print_help() {
print_version $AUTHOR $VERSION
echo ""
echo "$PROGNAME is a Icinga Plugin to check Incomming Multicast Packets on 7750 Models"
echo ""
echo "Usage: $PROGNAME -v 2c [HOST] [SNMPID]"
}

#Will be $ARGX$ input from Icinga X=ARG number
SNMPv="2c"                                      #SNMP Version
SNMPc=X6vmEiG62c                                #SNMP Community
HOST=$4                                                                                 #HOST
WARN=$2                                                                                 #WARNING
CRIT=$3                                                                                 #CRITICAL

INTERFACEOID=".1.3.6.1.2.1.31.1.1.1.2."          #OID to get number of routes that are currently active on this node
INTERFACEID=$5
#Exit status for service

ST_OK=0                                         #ROUTES OK
ST_WR=1                                         #ROUTES Warning
ST_CR=2                                         #ROUTES Critical
#ST_UK=3                                        #Unknown stat


declare -i MULTICASTS
MULTICASTS=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $INTERFACEOID$INTERFACEID`;

MULTICASTS1=$(echo "${MULTICASTS} | Packets=$MULTICASTS;$WARN;$CRIT;;")

#CMDs after the pipe is for OP5 graphs.

if [ "$MULTICASTS" -gt "$WARN" -a "$MULTICASTS" -lt "$CRIT" ]
        then
                echo "WARNING - Packets: ${MULTICASTS1}"
                exit $ST_WR

        elif [ "$MULTICASTS" -ge "$CRIT" ]
                then
                echo "CRITICAL - Packets: ${MULTICASTS1}"
                exit $ST_CR
        else
                echo "Multicast Packets: ${MULTICASTS1}"
                exit
fi

fi