#!/bin/bash
#Fixed for Huawei by @Ness

PROGNAME="check_huawei_n40e_temp_status"
print_help() {
        echo "$PROGNAME is a Nagios plugin to check temperature status on Huawei N40E-X3 may work on other models"
        echo ""
        echo "Usage: $PROGNAME [SNMP Community] [HOST] [WARNING TEMP] [CRITICAL TEMP]"
}

declare -a OID=('16842753' '16842754' '17039361' '17039362' '17104897' '17104898' '17432577')
declare -a TEMP
declare -a SENSORNAME

OIDINDEXMAX=$(expr ${#OID[@]} - 1)

BASETEMPOID=".1.3.6.1.4.1.2011.5.25.31.1.1.1.1.11."
BASENAMEOID=".1.3.6.1.2.1.47.1.1.1.1.7."
EXITCODE=0

SNMPv="2c"      #SNMP Version
SNMPc=$1        #SNMP Community
HOST=$2         #HOST
WARN=$3         #Warning temp
CRIT=$4         #Critical temp

for (( i=0; i<$OIDINDEXMAX; i++ )); do
        TEMP[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASETEMPOID${OID[$i]}`
        SENSORNAME[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASENAMEOID${OID[$i]}`

        if [[ "${TEMP[$i]}" =~ ^[0-9]+$ ]] 
        then
        
			if [[ "${TEMP[$i]}" -gt "$CRIT" ]]
			then
				TEMPSTATUS="CRITICAL - "
                EXITCODE=2

			elif [[ "${TEMP[$i]}" -gt "$WARN" ]]  && [[ $EXITCODE -ne 2 ]]
			then
                TEMPSTATUS="WARNING - "
                EXITCODE=1
                
			elif [[ "${TEMP[$i]}" -le "$WARN" ]] && [[ $EXITCODE -eq 0 ]]
			then
                TEMPSTATUS="OK - "
                EXITCODE=0
			fi
        
			TEMPACTIVE="$TEMPACTIVE ${SENSORNAME[$i]}: ${TEMP[$i]} C "
			GRAPHACTIVE="$GRAPHACTIVE ${SENSORNAME[$i]}(Temp)=${TEMP[$i]};$WARN;$CRIT;; "
		fi
done

echo "$TEMPSTATUS$TEMPACTIVE|$GRAPHACTIVE"
exit $EXITCODE