#!/bin/bash

# author    Niklas Sigg | NetNordic Sweden AB
# date      2020-09-01

print_help() {
    echo ""
    echo "This is a script for Icinga2, used for monitoring Power Supply status and voltage on iDRAC9. More specifically Poweredge R640 which utilizes 2 CPU's."
    echo "$PROGNAME Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass]"
    echo ""
}

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

ramname=".1.3.6.1.4.1.674.10892.5.4.1100.50.1.8"
ramsize=".1.3.6.1.4.1.674.10892.5.4.1100.50.1.14"

IFS=$'\n'

if [[ "$SNMPl" == "authPriv" ]]; then
  ramname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $ramname 2>&1))
  ramsize=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $ramsize 2>&1))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  ramname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $ramname 2>&1))
  ramsize=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $ramsize 2>&1))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  ramname=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $ramname 2>&1))
  ramsize=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $ramsize 2>&1))
else
  ramname=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $ramname))
  ramsize=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $ramsize))
fi

ramsize_function () {
    for (( i = 0; i < ${#ramname[@]}; i++ )); do
        if [[ "${ramsize[i]}" < "20000000" ]]; then
            exitcode=2
        fi
    done
}

if [[ "${ramsize[i]}" == "0" ]]; then
    echo "No Memory Installed."
    exitcode=2
elif [[ "${ramsize[i]}" == "2147483647" ]]; then
    echo "Unknown Memory Size."
    exitcode=3
else
    echo "OK - Memory is within set thresholds."
    ramsize_function
fi

for (( i = 0; i < ${#ramname[@]}; i++ )); do
    printf "%s" "${ramname[i]} Memory at roughly $((${ramsize[i]}/1000000)) MB / (${ramsize[i]} in KBytes)" $'\n'
done

unset IFS
exit $exitcode