#!/bin/bash

PROGNAME="check_adva_accessportloopbackstatus.sh"
AUTHOR="Sami Ereq 2016-09 Netnordic AB"
VERSION="Version 2.0"

print_version() {
        echo "$VERSION $AUTHOR"
}

print_help() {
print_version $AUTHOR $VERSION
echo ""
echo "$PROGNAME is a OP5 plugin to check if loopback is active on accessport-1-1-3-1 on ADVA."
echo ""
echo "Usage: $PROGNAME [ COMMUNITY ] [ HOST ]"
}

#Exit status for service
ST_OK=0
ST_WR=1
ST_CR=2
ST_UK=3


COMMUNITY=$1
HOST=$2
GETADVAMODEL=$(snmpget -v2c -c $COMMUNITY $HOST sysDescr.0 -Ovq)

if [ "$GETADVAMODEL" == "FSP150CC-XG210" ]; then

                ACCESSPORT3=".1.3.6.1.4.1.2544.1.12.4.1.1.1.30.1.1.3.1"
                OUTERVLANID1=".1.3.6.1.4.1.2544.1.12.4.1.1.1.34.1.1.3.1"

        elif [ "$GETADVAMODEL" == "FSP150CC-GE112" ]; then

                OUTERVLANID1=".1.3.6.1.4.1.2544.1.12.4.1.1.1.34.1.1.1.3"
                ACCESSPORT3=".1.3.6.1.4.1.2544.1.12.4.1.1.1.30.1.1.1.3"

fi

GETLOOPBACKSTATE="snmpget -v2c -c $COMMUNITY $HOST $ACCESSPORT3 -Ovq"
GETVLANID=$(snmpget -v2c -c $COMMUNITY $HOST $OUTERVLANID1 -Ovq | cut -d '"' -f '2-' | cut -d '-' -f '1')

for STATE in `${GETLOOPBACKSTATE}`; do
        if              [ "$STATE" -gt 1 ]; then

                echo "Loopback Configuration Is Active! Outer VLAN ID1 = $GETVLANID"
                exit $ST_CR

        elif    [ "$STATE" -eq 1 ]; then

                echo "Loopback is not active"
                exit $ST_OK
        else
                echo "Something is wrong = $STATE"
                $ST_UK
        fi;
done

