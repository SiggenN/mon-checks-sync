#!/usr/bin/env python3

from elasticsearch import Elasticsearch
import argparse
import sys

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--servers",
                  nargs='+',
                  help="<Required> elasticsearch servers to query",
                  required=True)
    parser.add_argument("-i", "--index",
                  help="<Required> elasticsearch index to query",
                  required=True)
    parser.add_argument("-t", "--time",
                  help="<Required> minimum time since last log entry in minutes",
                  required=True)
    args=parser.parse_args()
    fault_body = {
                  "query": {
                      "range": {
                          "received_at": {
                              "gt": "now-{}m/m".format(args.time) 
                              }
                          }
                      }
                  } 
    es = Elasticsearch(args.servers)
    result_fault = es.search(index=args.index, body=fault_body)
    #print("Got %d Hits:" % result_fault['hits']['total'])
    if result_fault['hits']['total'] > 0:
        print("OK: Got {} log entries last {} minutes:".format(result_fault['hits']['total'], args.time))
    else:        
        print("CRITICAL: No logs received during last {} minutes".format(args.time))
        sys.exit(2)

if __name__ == "__main__":
    main()