#!/bin/bash

#
# Script for monitoring NexentaStor
#
# Written by Sander Petersson @ NetNordic AB
#
# Usage:
#

### Variables

snmpget () {

	snmpwalk -O vq -v 2c -c $1 $2 $3

}

pooldatausage () {

	poollist=$($sng '.1.3.6.1.4.1.40045.1.1.1.1.1.2.1.1.1' | sed -e 's/"//g')
	usagelist=$($sng '.1.3.6.1.4.1.40045.1.1.1.1.1.2.1.1.5')
	poolARRAY=($poollist)
	usageARRAY=($usagelist)

	if [ ${#poolARRAY[@]} -eq ${#usageARRAY[@]} ]
	then
		typeset -i i
		for ((i=0;i<${#poolARRAY[@]};i++));
		do
			echo "${poolARRAY[$i]}:${usageARRAY[$i]}"
		done
	else
		echo "poolARRAY and usageARRAY don't match"
		exit 3
	fi

}

poolhealth () {

	poollist=$($sng '.1.3.6.1.4.1.40045.1.1.1.1.1.2.1.1.1' | sed -e 's/"//g')
	healthlist=$($sng '.1.3.6.1.4.1.40045.1.1.1.1.1.2.1.1.3')
	poolARRAY=($poollist)
	healthARRAY=($healthlist)

        if [ ${#poolARRAY[@]} -eq ${#healthARRAY[@]} ]
        then
                typeset -i i
                for ((i=0;i<${#poolARRAY[@]};i++));
                do
                        echo "${poolARRAY[$i]}:${healthARRAY[$i]}"
                done
        else
                echo "poolARRAY and healthARRAY don't match"
                exit 3
        fi


}

poolusagereport () {

	datausage=$(pooldatausage | grep $1 | cut -d: -f2)

	if [ ! -z $datausage ]
	then
		if [ $datausage -lt 70 ]
		then
			echo "OK! Usage in pool: $1 is: $datausage% | $1=$datausage%;;"
			return 0
		elif [ $datausage -lt 80 ]
		then
			echo "WARNING! Usage in pool: $1 is: $datausage% | $1=$datausage%;;"
			return 1
		else
			echo "CRITICAL! Usage in pool: $1 is: $datausage% | $1=$datausage%;;"
			return 2
		fi
	else
		echo "OK! Pool not found in on this nexenta host"
		exit 0
	fi

}

poolhealthreport () {

	poolhealth=$(poolhealth | grep $1 | cut -d: -f2)

        if [ ! -z $poolhealth ]
        then
		if [ $poolhealth -eq 1 ]
		then
			echo "CRITICAL! STATE for Pool: $1 is DEGRADED!"
			exit 2
		elif [ $poolhealth -eq 2 ]
		then
			echo "CRITICAL! STATE for Pool: $1 is FAULTED!"
			exit 2
		elif [ $poolhealth -eq 3 ]
                then
                        echo "CRITICAL! STATE for Pool: $1 is OFFLINE!"
                        exit 2
		elif [ $poolhealth -eq 4 ]
                then
                        echo "OK! STATE for Pool: $1 is Online"
                        exit 0
		elif [ $poolhealth -eq 5 ]
                then
                        echo "CRITICAL! STATE for Pool: $1 is REMOVED!"
                        exit 2
		elif [ $poolhealth -eq 6 ]
                then
                        echo "CRITICAL! STATE for Pool: $1 is UNAVAILABLE!"
                        exit 2
                elif [ $poolhealth -eq 7 ]
                then
                        echo "OK! STATE for Pool: $1 is Online"
                        exit 0
		fi
        else
                echo "OK! Pool not found in on this nexenta host"
                exit 0
        fi

}

while [[ $# > 1 ]]
do
        key="$1"
        sng="snmpget $COMMUNITY $ADDRESS"

        case $key in
                -h|--host)
                ADDRESS="$2"
                shift
                ;;
                -c|--community)
                COMMUNITY="$2"
                shift
                ;;
                --pooldatausage)
                poolusagereport $2
                shift
                ;;
                --poolstatus)
                poolhealthreport $2
                shift
                ;;
                --pool)
                POOL="$2"
                shift
                ;;
                *)
                ;;
        esac
        shift

done
