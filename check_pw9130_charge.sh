#!/bin/bash

PROGNAME="check_pw9130_charge"
print_help() {
echo "$PROGNAME is a Icinga plugin to check charge status on Eaton Powerware 9130"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST]"
}

declare -a OUTP
declare -i TIME

OID=(
	".1.3.6.1.4.1.534.1.2.1.0"	#TimeS
	".1.3.6.1.4.1.534.1.2.3.0"	#Current#
	".1.3.6.1.4.1.534.1.2.4.0"	#Capacity%
	)

ARRL=${#OID[@]}
	
EXITCODE=3

SNMPv="1"	#SNMP Version
SNMPc=$1	#SNMP Community
HOST=$2		#HOSTIP
CRIT=93     #below % to alert

for (( i=0; i<ARRL; i++ )); do
OUTP[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST ${OID[$i]}`
done

TIME=$((OUTP[0] / 60))

if [ "${OUTP[1]}" -lt "0" ]
then
	EXITCODE=1
	CHRG="Battery is Discharging! $TIME Minutes Remaining!"
elif [ "${OUTP[1]}" -gt "0" ]
then
	EXITCODE=0
	CHRG="Battery is Charging!"
elif [ "${OUTP[1]}" -eq "0" ]
then
	EXITCODE=0
	CHRG="Battery is Charged."
else
	CHRG="Unknown."
fi

if [ "${OUTP[2]}" -lt $CRIT ] && [ $EXITCODE -eq 1 ]
then
	EXITCODE=2
fi

echo "$CHRG ${OUTP[2]}% Charge. | Charge=${OUTP[2]}%;98;$CRIT 'Minutes Remaining'=$TIME;60;40;0"
exit $EXITCODE