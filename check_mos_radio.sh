#!/bin/bash

author="Author: Niklas Sigg"
prog="$0"

print_help() {
    echo ""
    echo "$author"
    echo "This is a script for Icinga2, used for monitoring status and quality of specific radios to a specific customer. This script will most likely not work with other radios or channels without slight modification."
    echo "Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass]"
    echo "       -x [Privacy mode] -X [Privacy pass] -T [Usage type] (rfquality|ratioerr|clients|trafficutil)"
    echo ""
}

#Flags
while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -C|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -T|--type)
    shift
    type=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

radiohostname="1.3.6.1.4.1.388.50.1.4.2.25.10.1.1.3"
rfqualityindex="1.3.6.1.4.1.388.50.1.4.2.25.10.1.1.21"
ratioerr="1.3.6.1.4.1.388.50.1.4.2.25.10.1.1.28" #Ratio of medium errors and rx-retries to rx-packets in percentage
clients=".1.3.6.1.4.1.388.50.1.4.2.25.10.1.1.22" #Number of clients per radio
trafficutil=".1.3.6.1.4.1.388.50.1.4.2.25.10.1.1.12" #Traffic utilization

if [[ "$SNMPl" == "authPriv" ]]; then
  radiohostname=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $radiohostname 2>&1 | sed -n '0~2!p' | sed 's/["]//g'))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  radiohostname=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $radiohostname 2>&1 | sed -n '0~2!p' | sed 's/["]//g'))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  radiohostname=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu $HOST $radiohostname 2>&1 | sed -n '0~2!p' | sed 's/["]//g'))
else
  radiohostname=($(timeout 3m snmpwalk -Oqv -v $SNMPv -t 300 -c $SNMPc $HOST $radiohostname 2>&1 | sed -n '0~2!p' | sed 's/["]//g'))
fi

rfquality_function () {

if [[ "$SNMPl" == "authPriv" ]]; then
  rfqualityindex=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $rfqualityindex 2>&1 | sed -n '0~2!p'))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  rfqualityindex=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $rfqualityindex 2>&1 | sed -n '0~2!p'))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  rfqualityindex=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu $HOST $rfqualityindex 2>&1 | sed -n '0~2!p'))
else
  rfqualityindex=($(timeout 3m snmpwalk -Oqv -v $SNMPv -t 300 -c $SNMPc $HOST $rfqualityindex 2>&1 | sed -n '0~2!p'))
fi

exitcode="0"
for (( i = 0; i < ${#rfqualityindex[@]}; i++ )); do
    if [[ "${rfqualityindex[i]}" -ge "0" && "${rfqualityindex[i]}" -lt "20" ]]; then
        exitcode="2"
        break
    elif [[ "${rfqualityindex[i]}" -ge "20" && "${rfqualityindex[i]}" -lt "35" ]]; then
        exitcode="1"
        break
    fi
done

if [[ "$exitcode" == "2" ]]; then
    echo "CRITICAL! Very poor quality of one or more radios (below 20%)! See all values below:"
elif [[ "$exitcode" == "1" ]]; then
    echo "WARNING! Poor quality of one or more radios (below 35%)! See all values below:"
elif [[ "$exitcode" == "0" ]]; then
    echo "OK! Good or average quality of one or more radios (above 35%)! See all values below:"
fi


echo ""
echo "Measures the quality of the medium in percentage. It is a function of average retries and error rate. Its range is from 0-100. Typically:"
echo "-1: no data available ( e.g. radio is not in wlan operation mode )"
echo "0-20: very poor quality"
echo "20-35: poor quality"
echo "40-60: average quality"
echo "60-100: good quality"
echo ""

for (( i = 0; i < ${#radiohostname[@]}; i++ )); do
    printf "%s" "${radiohostname[i]}    "
    if [[ "${rfqualityindex[i]}" -eq "-1" ]]; then
        echo "No data available (e.g. radio is not in wlan operation)"
    elif [[ "${rfqualityindex[i]}" -ge "0" && "${rfqualityindex[i]}" -lt "20" ]]; then
        echo "Very poor quality at ${rfqualityindex[i]}%"
    elif [[ "${rfqualityindex[i]}" -ge "20" && "${rfqualityindex[i]}" -lt "35" ]]; then
        echo "Poor quality at ${rfqualityindex[i]}%"
    elif [[ "${rfqualityindex[i]}" -ge "35" && "${rfqualityindex[i]}" -lt "60" ]]; then
        echo "Average quality at ${rfqualityindex[i]}%"
    elif [[ "${rfqualityindex[i]}" -ge "60" ]]; then
        echo "Good quality at ${rfqualityindex[i]}%"
    fi
done 

exit $exitcode

}

ratioerr_function () {

if [[ "$SNMPl" == "authPriv" ]]; then
  ratioerr=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $ratioerr 2>&1 | sed -n '0~2!p'))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  ratioerr=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $ratioerr 2>&1 | sed -n '0~2!p'))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  ratioerr=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu $HOST $ratioerr 2>&1 | sed -n '0~2!p'))
else
  ratioerr=($(timeout 3m snmpwalk -Oqv -v $SNMPv -t 300 -c $SNMPc $HOST $ratioerr 2>&1 | sed -n '0~2!p'))
fi

exitcode="0"
for (( i = 0; i < ${#ratioerr[@]}; i++ )); do
    if [[ "${ratioerr[i]}" -ge "40" ]]; then
        exitcode="2"
        break
    elif [[ "${ratioerr[i]}" -ge "20" && "${ratioerr[i]}" -lt "40" ]]; then
        exitcode="1"
        break
    fi
done

if [[ "$exitcode" == "2" ]]; then
    echo "CRITICAL! High ratio of medium errors and rx-retries to rx-packets on one or more radios (above 40%)! See all values below:"
elif [[ "$exitcode" == "1" ]]; then
    echo "WARNING! Medium-high ratio of medium errors and rx-retries to rx-packets on one or more radios (above 20%)! See all values below:"
elif [[ "$exitcode" == "0" ]]; then
    echo "OK! Low ratio of medium errors and rx-retries to rx-packets on one or more radios (below 20%)! See all values below:"
fi

echo ""
echo "Ratio of medium errors and rx-retries to rx-packets in percentage. Its range is from 0-100. Typically:"
echo "0-20: Low ratio (good)"
echo "20-40: Medium-high ratio (bad)"
echo "40-100: High ratio (very bad)"
echo ""

for (( i = 0; i < ${#radiohostname[@]}; i++ )); do
    printf "%s" "${radiohostname[i]}    "
    if [[ "${ratioerr[i]}" -ge "40" ]]; then
        echo "High ratio at ${ratioerr[i]}%"
    elif [[ "${ratioerr[i]}" -ge "20" && "${ratioerr[i]}" -lt "40" ]]; then
        echo "Medium-high ratio at ${ratioerr[i]}%"
    elif [[ "${ratioerr[i]}" -lt "20" ]]; then
        echo "Low ratio at ${ratioerr[i]}%"
    fi
done 

exit $exitcode

}

clients_function () {

if [[ "$SNMPl" == "authPriv" ]]; then
  clients=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $clients 2>&1 | sed -n '0~2!p'))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  clients=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $clients 2>&1 | sed -n '0~2!p'))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  clients=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu $HOST $clients 2>&1 | sed -n '0~2!p'))
else
  clients=($(timeout 3m snmpwalk -Oqv -v $SNMPv -t 300 -c $SNMPc $HOST $clients 2>&1 | sed -n '0~2!p'))
fi

echo ""
echo "Shows number of clients per radio. This currently has no thresholds set. Icinga will always show this as 'OK'"
echo ""

for (( i = 0; i < ${#radiohostname[@]}; i++ )); do
    printf "%s" "${radiohostname[i]}    "
    if [[ "${clients[i]}" -ge "0" ]]; then
        echo "Number of clients: ${clients[i]}"
    fi
done 

exit 0

}

trafficutil_function () {

if [[ "$SNMPl" == "authPriv" ]]; then
  trafficutil=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $trafficutil 2>&1 | sed -n '0~2!p'))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  trafficutil=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $trafficutil 2>&1 | sed -n '0~2!p'))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  trafficutil=($(timeout 3m snmpwalk -Ovq -v $SNMPv -t 300 -l $SNMPl -u $SNMPu $HOST $trafficutil 2>&1 | sed -n '0~2!p'))
else
  trafficutil=($(timeout 3m snmpwalk -Oqv -v $SNMPv -t 300 -c $SNMPc $HOST $trafficutil 2>&1 | sed -n '0~2!p'))
fi

echo ""
echo "Shows traffic utilization per radio. This currently has no thresholds set. Icinga will always show this as 'OK'"
echo ""

for (( i = 0; i < ${#radiohostname[@]}; i++ )); do
    printf "%s" "${radiohostname[i]}    "
    if [[ "${trafficutil[i]}" -ge "0" ]]; then
        echo "Traffic utilization: ${trafficutil[i]}"
    elif [[ "${trafficutil[i]}" -eq "-1" ]]; then
        echo "Radio turned off: ${trafficutil[i]}"
    fi
done 

exit 0

}

if [[ "$type" == "rfquality" ]]; then
    rfquality_function
elif [[ "$type" == "ratioerr" ]]; then
    ratioerr_function
elif [[ "$type" == "clients" ]]; then
    clients_function
elif [[ "$type" == "trafficutil" ]]; then
    trafficutil_function
else
    echo "Please fill in the 'type' flag. For example:"
    echo "-T rfquality"
    echo "or"
    echo "-T ratioerr"
    echo "or"
    echo "-T clients"
    echo "or"
    echo "-T trafficutil"
    exit 3
fi