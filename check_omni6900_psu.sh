#!/bin/bash

PROGNAME="check_6900_psu"
print_help()    {
        echo "$PROGNAME is a Icinga/OP5 plugin to check the status of PSUs on Alcatel Omniswitch 6900."
        echo ""
        echo "Usage: $PROGNAME [HOST] [SNMP Community] [BOOL: Stacked?]"
}


EXITCODE=0

declare -a CHECK
declare -a ECODE
declare -a STATUS

baseOID=".1.3.6.1.4.1.6486.801.1.1.1.1.1.1.1.2."
strgOID=".1.3.6.1.2.1.47.1.1.1.1.7."

HOST=$1
SNMPv=2c
SNMPc=$2
SLOTS=(275 276 277 278)
ESTRG="PSU"

if [ "$3" = true ]
then
  STACK=4
else
  STACK=2
fi

for (( i=0; i<STACK; i++ ))
do
  CHECK[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $baseOID${SLOTS[$i]}`
  STRNG[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $strgOID${SLOTS[$i]}`

  case ${CHECK[$i]} in
	1)
	  STATUS[$i]="Up"
	  ECODE[$i]=0
	  ;;
	2)
	  STATUS[$i]="Down"
	  ECODE[$i]=1
	  ;;
	3)
	  STATUS[$i]="Testing"
	  ECODE[$i]=3
	  ;;
	4)
	  STATUS[$i]="Unknown"
	  ECODE[$i]=3
	  ;;
	5)
	  STATUS[$i]="Secondary"
	  ECODE[$i]=0
	  ;;
	6)
	  STATUS[$i]="Not Present"
	  ECODE[$i]=1
	  ;;
	7)
	  STATUS[$i]="Unpowered"
	  ECODE[$i]=1
	  ;;
	8)
	  STATUS[$i]="Master"
	  ECODE[$i]=0
	  ;;
	9)
	  STATUS[$i]="Idle"
	  ECODE[$i]=0
	  ;;
	10)
	  STATUS[$i]="Powersave"
	  ECODE[$i]=0
	  ;;
	*)
	  STATUS[$i]="Error!"
	  ECODE[$i]=3
	  ;;
  esac
  
  case $EXITCODE in
    0)
	  EXITCODE=$(( EXITCODE + ECODE[$i] ))
	  ;;
	1)
	  case ${ECODE[$i]} in
	    0|1)
		  EXITCODE=$(( EXITCODE+ECODE[$i] ))
		  ;;
		2|3|*)
		  EXITCODE=2
		  ;;
	  esac
	  ;;
	2)
	  ;;
	3)
	  case ${ECODE[$i]} in
		1|2)
		  EXITCODE=${ECODE[$i]}
		  ;;
		0|3)
		  ;;
		esac
	  ;;
  esac
  
  ESTRG="$ESTRG, Slot ${STRNG[$i]} - ${STATUS[$i]}"  
done


echo "$ESTRG"
exit $EXITCODE