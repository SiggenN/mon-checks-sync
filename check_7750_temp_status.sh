#!/bin/bash

#To run the script /check_7750_temp_status.sh "snmpcommunity" "IP" "oid;oid;oid;oid" "50" "60"

PROGNAME="check_7750_temp_status"
print_help() {
echo "$PROGNAME is a OP5 plugin to check temperature status on Nokia/Alcatel 7750"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST] [OIDs string ';' sepparated] [WARNiNG TEMP] [CRITICAL TEMP]"
echo "example ./check_7750_temp_status.sh community IP 'oid;oid;oid;oid;oid' 70 90"
}

declare -a TEMP
declare -a SENSORNAME

BASETEMPOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.18.1."
BASENAMEOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.8.1."
EXITCODE=0

SNMPv="2c"      #SNMP Version
SNMPc=$1        #SNMP Community
HOST=$2         #HOST
OID=$3          #OID Array
WARN=$4         #Warning temp
CRIT=$5         #Critical temp

IFS=';' read -a OID <<< "$OID"

arrL=${#OID[@]} #count array

for (( i=0; i<arrL; i++ )); do

TEMP[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASETEMPOID${OID[$i]}`
SENSORNAME[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASENAMEOID${OID[$i]}`

if [ "${TEMP[$i]}" != "-1" ]
then
        if [ "${TEMP[$i]}" -gt "$CRIT" ]
        then
                TEMPSTATUS="CRITICAL - "
                EXITCODE=2

        elif [ "${TEMP[$i]}" -gt "$WARN" ]  && [ $EXITCODE -ne 2 ]
        then
                TEMPSTATUS="WARNING - "
                EXITCODE=1
        elif [ "${TEMP[$i]}" -le "$WARN" ] && [ $EXITCODE -eq 0 ]
        then
                TEMPSTATUS="OK - "
                EXITCODE=0
        fi
        TEMPACTIVE="$TEMPACTIVE ${SENSORNAME[$i]}: ${TEMP[$i]} C "
        GRAPHACTIVE="$GRAPHACTIVE${SENSORNAME[$i]}(Temp)=${TEMP[$i]};$WARN;$CRIT;; "
fi
done
echo "$TEMPSTATUS$TEMPACTIVE|$GRAPHACTIVE"
exit $EXITCODE
