#!/bin/sh
# Packetfront Solutions 2012
#
# First version 2012-11-15 Abdullah Khalid
# Second version 2013-07-24 Joel Rangsmo : Now using the "-/+ buffers/cache" instead of "Mem: free"
# Third version 2019-09-10 Emma Lindholm : added available memory in cleartext in output

function showmem {
totalmem=$(free -m | awk '/Mem:/ {print  $2}')
freemem=$(free -m | awk '/cache:/ {print  $4}')

warning=$((totalmem/5))
critical=$((totalmem/10))
return_value=0
memstate="Normal"



if [ $freemem -lt $warning ]
then
        return_value=1
        memstate="Warning!"
fi

if [ $freemem -lt $critical ]
then
        return_value=2
        memstate="Critical!"
fi

echo "Memory state is $memstate - $freemem MB available | available=${freemem}MB;;;0"
return $return_value
}

rundir=`dirname $0`
showmem