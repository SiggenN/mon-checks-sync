#!/bin/bash

#Made by Niklas Sigg @ NetNordic Sweden AB
#This is a script to help monitor the status of a UPS. There are a few oids included already and more will be added over time.

print_help() {
    echo ""
    echo "This is a script for Icinga2, used for monitoring status of certain UPS Vendors. Currently supported:"
    echo "Delta UPS - DeltaUPS-MIB.mib (http://www.circitor.fr/Mibs/Html/D/DeltaUPS-MIB.php)"
    echo "Eaton UPS - XUPS MIB (http://powerquality.eaton.fr/support/software-drivers/downloads/connectivity-firmware.asp)"
    echo "Cyberpower UPS - MIB (https://www.cyberpowersystems.com/products/software/mib-files/) OBS Don't forget to ad a Zero after the generic oid"
    echo ""
    echo "Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass]"
    echo "       -x [Privacy mode] -X [Privacy pass] -t [Checktype] (delta_status|eatonxups_status|cyberups_status|trafficutil)"
    echo ""
    echo "The 'delta_status' checktype returns one of these values floating(0), charging(1), resting(2), discharging(3) as well as the estimated minutes remaining on the battery."
    echo "The 'eatonxups_status' checktype returns on of these values batteryCharging(1),batteryDischarging(2),batteryFloating(3),batteryResting(4),unknown(5),batteryDisconnected(6),batteryUnderTest(7),checkBattery(8) as well as estimated minutes remaining and estimated battery capacity in %."
    echo "The 'cyberups_status' Output status (battery power, mains, etc) integer: 2 main power. integer 3 on battery. anything else bad."
    echo ""
}

#Flags
while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -t|--type)
    shift
    type=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

if [ $? -ne 0 ]; then
  echo "Failed. Rechecking your flags might help."
  print_help
  exit 3
fi

#Cyberpower ups oid from check_cps.pl in folder mon-check-sync
cyupsBatteryTimeRemaining=".1.3.6.1.4.1.3808.1.1.1.2.2.4.0"	# Runtime remaining of load if on battery power
cyupsBatteryCapacity=".1.3.6.1.4.1.3808.1.1.1.2.2.1.0" 	#  The battery capacity expressed in percentage.
cyupsBatteryStatus=".1.3.6.1.4.1.3808.1.1.1.4.1.1.0"	# Output status (battery power, mains, etc) integer: 2 main power. integer 3 on battery. anything else bad.

#Delta UPS oids from DeltaUPS-MIB.mib (http://www.circitor.fr/Mibs/Html/D/DeltaUPS-MIB.php)
dupsBatteryStatus="1.3.6.1.4.1.2254.2.4.7.2" #ok(0), low(1), depleted(2)
dupsBatteryCharge="1.3.6.1.4.1.2254.2.4.7.3" #floating(0), charging(1), resting(2), discharging(3)
dupsIdentName="1.3.6.1.4.1.2254.2.4.1.5" #A string identifying the UPS. This object should be set by the administrator and is usually left empty.
dupsBatteryEstimatedTime="1.3.6.1.4.1.2254.2.4.7.5" #Estimated time from backup to low battery shutdown. Guessing this is in minutes.
dupsBatteryCapacity="1.3.6.1.4.1.2254.2.4.7.8" # An estimate of the battery charge remaining expressed as a percent of full charge. INTEGER	0..100

#Eaton UPS - XUPS MIB (http://powerquality.eaton.fr/support/software-drivers/downloads/connectivity-firmware.asp)
xupsBatTimeRemaining=".1.3.6.1.4.1.534.1.2.1.0" # INTEGER32 (0..2147483647)
xupsBatCapacity=".1.3.6.1.4.1.534.1.2.4.0" # INTEGER32 (0..100)
xupsBatteryAbmStatus=".1.3.6.1.4.1.534.1.2.5.0" # INTEGER {batteryCharging(1),batteryDischarging(2),batteryFloating(3),batteryResting(4),unknown(5),batteryDisconnected(6),batteryUnderTest(7),checkBattery(8)}
xupsBatCurrent=".1.3.6.1.4.1.534.1.2.3" # INTEGER32 (-2147483648.2147483647)


cyberups_status_function () {
 #You have to use Snmpget when using timticks
  if [[ "$SNMPl" == "authPriv" ]]; then
  cyupsBatteryStatus=$(snmpget -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $cyupsBatteryStatus 2>&1)
  cyupsBatteryTimeRemaining=$(snmpget -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $cyupsBatteryTimeRemaining 2>&1)
  cyupsBatteryCapacity=$(snmpget -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $cyupsBatteryCapacity 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  cyupsBatteryStatus=$(snmpget -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $cyupsBatteryStatus 2>&1)
  cyupsBatteryTimeRemaining=$(snmpget -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $cyupsBatteryTimeRemaining 2>&1)
  cyupsBatteryCapacitys=$(snmpget -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $cyupsBatteryCapacity 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  cyupsBatteryStatus=$(snmpget -Oqv -v $SNMPv -l $SNMPl -u $SNMPu $HOST $cyupsBatteryStatus 2>&1)
  cyupsBatteryTimeRemaining=$(snmpget -v $SNMPv -l $SNMPl -u $SNMPu $HOST $cyupsBatteryTimeRemaining 2>&1)
  cyupsBatteryCapacity=$(snmpget -Oqv -v $SNMPv -l $SNMPl -u $SNMPu $HOST $cyupsBatteryCapacity 2>&1)
else
  cyupsBatteryStatus=$(snmpget -Oqv -v $SNMPv -c $SNMPc $HOST $cyupsBatteryStatus 2>&1)
  cyupsBatteryTimeRemaining=$(snmpget -v $SNMPv -c $SNMPc $HOST $cyupsBatteryTimeRemaining 2>&1)
  cyupsBatteryCapacity=$(snmpget -Oqv -v $SNMPv -c $SNMPc $HOST $cyupsBatteryCapacity 2>&1)
fi

    tidkvar=$(echo $cyupsBatteryTimeRemaining | cut -d' ' -f4 | sed 's/[()]//g') # ger mig tidticks
    timeremainingminutes=$(($tidkvar/100/60)) # ger oss minuterna
    if [[ "$cyupsBatteryStatus" -eq "3" ]]; then #UPS on battery
      status="Battery is discharging. Estimated battery time remaining $timeremainingminutes minutes."
      exitcode=2
    elif [[ "$cyupsBatteryStatus" -eq "2" ]]; then #UPS on mainpower
      status="Battery is on mainpower. Estimated battery time remaining $timeremainingminutes minutes."
      exitcode=0
    elif [[ "$cyupsBatteryStatus" -eq "4" ]]; then #UPS is boosting utility power
      status="Boosting line voltage Estimated battery time remaining $timeremainingminutes minutes."
      exitcode=1
    elif [[ "$cyupsBatteryStatus" -eq "5" ]]; then #UPS is sleeping
      status="UPS is sleeping."
      exitcode=2
    elif [[ "$cyupsBatteryStatus" -eq "6" ]]; then #The UPS is off
      status="UPS is offline!."
      exitcode=2
    elif [[ "$cyupsBatteryStatus" -eq "7" ]]; then #UPS is rebooting
      status="UPS is rebooting."
      exitcode=2
    else
      status="Battery unknown fault"
      exitcode=3
    fi
     
    echo "$status | 'Minutes Remaining'=$timeremainingminutes;60;45 'Capacity'=$cyupsBatteryCapacity%;75;56"
    exit $exitcode
}

deltaups_status_function () {
  if [[ "$SNMPl" == "authPriv" ]]; then
    dupsBatteryCharge=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $dupsBatteryCharge 2>&1))
    dupsBatteryEstimatedTime=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $dupsBatteryEstimatedTime 2>&1))
    dupsBatteryCapacity=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $dupsBatteryCapacity 2>&1))
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    dupsBatteryCharge=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $dupsBatteryCharge 2>&1))
    dupsBatteryEstimatedTime=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $dupsBatteryEstimatedTime 2>&1))
    dupsBatteryCapacity=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $dupsBatteryCapacity 2>&1))
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    dupsBatteryCharge=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $dupsBatteryCharge 2>&1))
    dupsBatteryEstimatedTime=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $dupsBatteryEstimatedTime 2>&1))
    dupsBatteryCapacity=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $dupsBatteryCapacity 2>&1))
  else
    dupsBatteryCharge=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $dupsBatteryCharge 2>&1))
    dupsBatteryEstimatedTime=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $dupsBatteryEstimatedTime 2>&1))
    dupsBatteryCapacity=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $dupsBatteryCapacity 2>&1))
  fi
    if [[ "$dupsBatteryCharge" -eq "3" ]]; then
      status="Battery is discharging. Estimated battery time remaining $dupsBatteryEstimatedTime minutes."
      exitcode=2
    elif [[ "$dupsBatteryCharge" -eq "2" ]]; then
      status="Battery is resting. Estimated battery time remaining $dupsBatteryEstimatedTime minutes."
      exitcode=0
    elif [[ "$dupsBatteryCharge" -eq "1" ]]; then
      status="Battery is charging. Estimated battery time remaining $dupsBatteryEstimatedTime minutes."
      exitcode=0
    else
      status="Battery is floating. Estimated battery time remaining $dupsBatteryEstimatedTime minutes."
      exitcode=0
    fi
  echo "$status | 'Minutes Remaining'=$dupsBatteryEstimatedTime;60;45 'Capacity'=$dupsBatteryCapacity%;75;56"
  exit $exitcode
}

eatonxups_status_function () {
  if [[ "$SNMPl" == "authPriv" ]]; then
    xupsBatteryAbmStatus=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $xupsBatteryAbmStatus 2>&1))
    xupsBatTimeRemaining=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $xupsBatTimeRemaining 2>&1))
    xupsBatCapacity=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $xupsBatCapacity 2>&1))
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    xupsBatteryAbmStatus=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $xupsBatteryAbmStatus 2>&1))
    xupsBatTimeRemaining=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $xupsBatTimeRemaining 2>&1))
    xupsBatCapacity=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $xupsBatCapacity 2>&1))
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    xupsBatteryAbmStatus=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $xupsBatteryAbmStatus 2>&1))
    xupsBatTimeRemaining=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $xupsBatTimeRemaining 2>&1))
    xupsBatCapacity=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $xupsBatCapacity 2>&1))
  else
    xupsBatteryAbmStatus=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $xupsBatteryAbmStatus 2>&1))
    xupsBatTimeRemaining=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $xupsBatTimeRemaining 2>&1))
    xupsBatCapacity=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $xupsBatCapacity 2>&1))
  fi

    (( timeremainingminutes=$xupsBatTimeRemaining / 60 ))
    timeremainingseconds=$xupsBatTimeRemaining

    #Battery charging function
    battcharge() {
        if [[ "$xupsBatCapacity" -ge "100" ]]; then
            status="Battery is fully charged, it will soon enter floating state. Current capacity is at $xupsBatCapacity% and current battery time is $timeremainingminutes minutes."
            exitcode=0
        elif [[ "$xupsBatCapacity" -lt "100" ]]; then
            status="Battery is charging. Current capacity is at $xupsBatCapacity% and current battery time is $timeremainingminutes minutes."
            exitcode=0
        else
            status="Capacity and time unknown."
            exitcode=3
        fi

        echo "$status | 'Minutes Remaining'=$timeremainingminutes;60;45 'Time Remaining (Seconds)'=$timeremainingseconds;3600;2700 'Capacity'=$xupsBatCapacity%;75;56"
        exit $exitcode
    }
    #Battery discharge function
    battdischarge() {
        if [[ "$xupsBatCapacity" -le "100" ]]; then
            status="Battery is discharging. Current capacity is at $xupsBatCapacity% and current battery time is $timeremainingminutes minutes."
            exitcode=2
        else
            status="Capacity and time unknown."
            exitcode=3
        fi

        echo "$status | 'Minutes Remaining'=$timeremainingminutes;60;45 'Time Remaining (Seconds)'=$timeremainingseconds;3600;2700 'Capacity'=$xupsBatCapacity%;75;56"
        exit $exitcode
    }
    #Battery floating function
    battfloating() {
        if [[ "$xupsBatCapacity" -ge "0" ]]; then
            status="Battery Floating. The charger is temporarily charging the battery to its float voltage. Current capacity is at $xupsBatCapacity% and current battery time is $timeremainingminutes minutes."
            exitcode=0
        else
            status="Capacity and time unknown."
            exitcode=3
        fi

        echo "$status | 'Minutes Remaining'=$timeremainingminutes;60;45 'Time Remaining (Seconds)'=$timeremainingseconds;3600;2700 'Capacity'=$xupsBatCapacity%;75;56"
        exit $exitcode
    }

    case $xupsBatteryAbmStatus in
        8)
            exitcode=1
            status="Check Battery. Battery state is uncertain following a poor battery test result."
            ;;
        7)
            exitcode=0
            status="Battery under test"
            ;;
        6)
            exitcode=2
            status="Battery Disconnected"
            ;;
        5)
            exitcode=3
            status="Battery Status Unknown"
            ;;
        4)
            exitcode=0
            status="Battery Resting. The battery is fully charged and none of the other actions(charging/discharging/floating) is being done"
            ;;
        3) battfloating ;;
        2) battdischarge ;;
        1) battcharge ;;
        *)
            exitcode=3
            status="Error!"
            ;;
    esac

    echo "$status | 'Minutes Remaining'=$timeremainingminutes;60;45 'Time Remaining (Seconds)'=$timeremainingseconds;3600;2700 'Capacity'=$xupsBatCapacity%;75;56"
    exit $exitcode

}

if [[ $type = "delta_status" ]]; then
  deltaups_status_function
elif [[ $type = "eatonxups_status" ]]; then
  eatonxups_status_function
  elif [[ $type = "cyberups_status" ]]; then
  cyberups_status_function
fi
