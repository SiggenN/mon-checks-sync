#!/bin/bash

PROGNAME="check_pw9130_capacity"
print_help() {
echo "$PROGNAME is a Icinga plugin to check ABM status on Eaton Powerware 9130"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST]"
}

declare -i OUTP

OID=".1.3.6.1.4.1.534.1.2.4.0"	#capacity

SNMPv="1"	#SNMP Version
SNMPc=$1	#SNMP Community
HOST=$2		#HOSTIP

OUTP=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $OID`

echo "$OUTP% | Capacity=$OUTP%;90;70"
exit $EXITCODE