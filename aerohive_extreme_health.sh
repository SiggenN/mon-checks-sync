#!/bin/bash

# author    Niklas Sigg | NetNordic Sweden AB
# date      2021-08-25

print_help() {
    echo ""
    echo "Aerohive AP and Extreme healthchecks specifically for Unionen"
    echo "Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -t [Type of healthcheck] (extremetemp|extremecpu|extremepower|extremememory|aerohivecpu|aerohivememory)"
    echo "Example: ./aerohive_extreme_health -H xxx.xx.xxx.xx -v 2c -c public -t extremetemp"
    echo ""
}

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -t|--type)
    shift
    type=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

extremeCurrentTemperature=".1.3.6.1.4.1.1916.1.1.1.8.0" #INTEGER ( 0..100 )
extremeOverTemperatureAlarm=".1.3.6.1.4.1.1916.1.1.1.7.0" #TruthValue  Represents a boolean value.  SYNTAX INTEGER { true(1), false(2) }
extremeCpuMonitorTotalUtilization=".1.3.6.1.4.1.1916.1.32.1.2.0" #INTEGER ( 0..100 )
extremePowerAlarm=".1.3.6.1.4.1.1916.1.1.1.22.0" #TruthValue  Represents a boolean value.  SYNTAX INTEGER { true(1), false(2) }
aerohiveCpuUtilization=".1.3.6.1.4.1.26928.1.2.3.0" #INTEGER ( 0..100 )
aerohiveMemUtilization=".1.3.6.1.4.1.26928.1.2.4.0" #INTEGER ( 0..100 )
extremeMemoryMonitorSystemTotal=1.3.6.1.4.1.1916.1.32.2.2.1.2 #bits
extremeMemoryMonitorSystemUsage="1.3.6.1.4.1.1916.1.32.2.2.1.4" #bits

extreme_mem_function () {

    if [[ "$SNMPc" == "2c" ]]; then
            extremeMemoryMonitorSystemTotal=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeMemoryMonitorSystemTotal | sed 's/["]//g'))
            extremeMemoryMonitorSystemUsage=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeMemoryMonitorSystemUsage | sed 's/["]//g'))
        elif [[ "$SNMPl" == "authPriv" ]]; then
            extremeMemoryMonitorSystemTotal=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $extremeMemoryMonitorSystemTotal 2>&1 | sed 's/["]//g'))
            extremeMemoryMonitorSystemUsage=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $extremeMemoryMonitorSystemUsage 2>&1 | sed 's/["]//g'))
        elif [[ "$SNMPl" == "authNoPriv" ]]; then
            extremeMemoryMonitorSystemTotal=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $extremeMemoryMonitorSystemTotal 2>&1 | sed 's/["]//g'))
            extremeMemoryMonitorSystemUsage=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $extremeMemoryMonitorSystemUsage 2>&1 | sed 's/["]//g'))
        elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
            extremeMemoryMonitorSystemTotal=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $extremeMemoryMonitorSystemTotal 2>&1 | sed 's/["]//g'))
            extremeMemoryMonitorSystemUsage=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $extremeMemoryMonitorSystemUsage 2>&1 | sed 's/["]//g'))
        else
            extremeMemoryMonitorSystemTotal=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeMemoryMonitorSystemTotal | sed 's/["]//g'))
            extremeMemoryMonitorSystemUsage=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeMemoryMonitorSystemUsage | sed 's/["]//g'))
    fi

percentof90=$(($extremeMemoryMonitorSystemTotal*90/100)) #taking 90% of total memory and saving in a variable
percentof80=$(($extremeMemoryMonitorSystemTotal*80/100)) #taking 80% of total memory and saving in a variable

    if [[ "$extremeMemoryMonitorSystemUsage" -ge "$percentof90" ]]; then
            echo "Critical! Memory usage is over 90%. Usage at $extremeMemoryMonitorSystemUsage bits."
            exit 2
        elif [[ "$extremeMemoryMonitorSystemUsage" -ge "$percentof80" && "$extremeMemoryMonitorSystemUsage" -lt "$percentof90" ]]; then
            echo "Warning! Memory usage is over 80% (still under 90%). Usage at $extremeMemoryMonitorSystemUsage bits."
            exit 0
        elif [[ "$extremeMemoryMonitorSystemUsage" -lt "$percentof80" ]]; then
            echo "OK! Memory usage is under 80%. Usage at $extremeMemoryMonitorSystemUsage bits."
            exit 0
    fi
}

extreme_temp_function () {
    if [[ "$SNMPc" == "2c" ]]; then
            extremeCurrentTemperature=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeCurrentTemperature))
            extremeOverTemperatureAlarm=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeOverTemperatureAlarm))
        elif [[ "$SNMPl" == "authPriv" ]]; then
            extremeCurrentTemperature=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $extremeCurrentTemperature 2>&1))
            extremeOverTemperatureAlarm=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $extremeOverTemperatureAlarm 2>&1))
        elif [[ "$SNMPl" == "authNoPriv" ]]; then
            extremeCurrentTemperature=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $extremeCurrentTemperature 2>&1))
            extremeOverTemperatureAlarm=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $extremeOverTemperatureAlarm 2>&1))
        elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
            extremeCurrentTemperature=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $extremeCurrentTemperature 2>&1))
            extremeOverTemperatureAlarm=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $extremeOverTemperatureAlarm 2>&1))
        else
            extremeCurrentTemperature=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeCurrentTemperature))
            extremeOverTemperatureAlarm=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeOverTemperatureAlarm))
    fi

    if [[ "$extremeOverTemperatureAlarm" != "2" ]]; then
            echo "Critical! Temperature is currently at $extremeCurrentTemperature C."
            exit 2
        elif [[ "$extremeOverTemperatureAlarm" == "2" ]]; then
            echo "OK! Temperature is currently at $extremeCurrentTemperature C."
            exit 0
    fi
}

extreme_cpu_function () {
    if [[ "$SNMPc" == "2c" ]]; then
            extremeCpuMonitorTotalUtilization=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeCpuMonitorTotalUtilization))
        elif [[ "$SNMPl" == "authPriv" ]]; then
            extremeCpuMonitorTotalUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $extremeCpuMonitorTotalUtilization 2>&1))
        elif [[ "$SNMPl" == "authNoPriv" ]]; then
            extremeCpuMonitorTotalUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $extremeCpuMonitorTotalUtilization 2>&1))
        elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
            extremeCpuMonitorTotalUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $extremeCpuMonitorTotalUtilization 2>&1))
        else
            extremeCpuMonitorTotalUtilization=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremeCpuMonitorTotalUtilization))
    fi

    if [[ "$extremeCpuMonitorTotalUtilization" -ge "85" ]]; then
            echo "Critical! CPU utilization is currently at $extremeCpuMonitorTotalUtilization%."
            exit 2
        elif [[ "$extremeCpuMonitorTotalUtilization" -ge "60" && "$extremeCpuMonitorTotalUtilization" -lt "85" ]]; then
            echo "Warning! CPU utilization is currently at $extremeCpuMonitorTotalUtilization%."
            exit 1
        elif [[ "$extremeCpuMonitorTotalUtilization" -lt "60" ]]; then
            echo "OK! CPU utilization is currently at $extremeCpuMonitorTotalUtilization%."
            exit 0
        else 
            echo "Snmpwalk response contains unknown data. Troubleshooting with snmpwalk and script needed."
            exit 3
    fi
}

extreme_poweralarm_function () {
    if [[ "$SNMPc" == "2c" ]]; then
            extremePowerAlarm=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremePowerAlarm))
        elif [[ "$SNMPl" == "authPriv" ]]; then
            extremePowerAlarm=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $extremePowerAlarm 2>&1))
        elif [[ "$SNMPl" == "authNoPriv" ]]; then
            extremePowerAlarm=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $extremePowerAlarm 2>&1))
        elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
            extremePowerAlarm=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $extremePowerAlarm 2>&1))
        else
            extremePowerAlarm=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $extremePowerAlarm))
    fi

    if [[ "$extremePowerAlarm" != "2" ]]; then
            echo "Critical! Alarm state indicates either fan failure or overtemperature condition."
            exit 2
        elif [[ "$extremePowerAlarm" == "2" ]]; then
            echo "OK! Alarm state of the power supply is OK."
            exit 0
    fi
}

aerohive_cpu_function () {
    if [[ "$SNMPc" == "2c" ]]; then
            aerohiveCpuUtilization=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $aerohiveCpuUtilization))
        elif [[ "$SNMPl" == "authPriv" ]]; then
            aerohiveCpuUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $aerohiveCpuUtilization 2>&1))
        elif [[ "$SNMPl" == "authNoPriv" ]]; then
            aerohiveCpuUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $aerohiveCpuUtilization 2>&1))
        elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
            aerohiveCpuUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $aerohiveCpuUtilization 2>&1))
        else
            aerohiveCpuUtilization=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $aerohiveCpuUtilization))
    fi

    if [[ "$aerohiveCpuUtilization" -ge "85" ]]; then
            echo "Critical! CPU utilization is currently at $aerohiveCpuUtilization%."
            exit 2
        elif [[ "$aerohiveCpuUtilization" -ge "60" && "$aerohiveCpuUtilization" -lt "85" ]]; then
            echo "Warning! CPU utilization is currently at $aerohiveCpuUtilization%."
            exit 1
        elif [[ "$aerohiveCpuUtilization" -lt "60" ]]; then
            echo "OK! CPU utilization is currently at $aerohiveCpuUtilization%."
            exit 0
        else 
            echo "Snmpwalk response contains unknown data. Troubleshooting with snmpwalk and script needed."
            exit 3
    fi
}

aerohive_mem_function () {
    if [[ "$SNMPc" == "2c" ]]; then
            aerohiveMemUtilization=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $aerohiveMemUtilization))
        elif [[ "$SNMPl" == "authPriv" ]]; then
            aerohiveMemUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $aerohiveMemUtilization 2>&1))
        elif [[ "$SNMPl" == "authNoPriv" ]]; then
            aerohiveMemUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $aerohiveMemUtilization 2>&1))
        elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
            aerohiveMemUtilization=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $aerohiveMemUtilization 2>&1))
        else
            aerohiveMemUtilization=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $aerohiveMemUtilization))
    fi

    if [[ "$aerohiveMemUtilization" -ge "90" ]]; then
            echo "Critical! Memory utilization is currently at $aerohiveMemUtilization%."
            exit 2
        elif [[ "$aerohiveMemUtilization" -ge "80" && "$aerohiveMemUtilization" -lt "90" ]]; then
            echo "Warning! Memory utilization is currently at $aerohiveMemUtilization%."
            exit 1
        elif [[ "$aerohiveMemUtilization" -lt "80" ]]; then
            echo "OK! Memory utilization is currently at $aerohiveMemUtilization%."
            exit 0
        else 
            echo "Snmpwalk response contains unknown data. Troubleshooting with snmpwalk and script needed."
            exit 3
    fi
}

if [[ "$type" == "extremetemp" ]]; then
        extreme_temp_function
    elif [[ "$type" == "extremecpu" ]]; then
        extreme_cpu_function
    elif [[ "$type" == "extremepower" ]]; then
        extreme_poweralarm_function
    elif [[ "$type" == "extremememory" ]]; then
        extreme_mem_function
    elif [[ "$type" == "aerohivecpu" ]]; then
        aerohive_cpu_function
    elif [[ "$type" == "aerohivememory" ]]; then
        aerohive_mem_function
    else
        echo "Something wrong with the passing "-type" flag. Check for typos or open help with -h."
fi

