#!/bin/bash

query=$(/bin/find /opt/packetfront/becs3/roots/cjm/ -type f -name "*.lic" ! -name ".*" -exec ls -1rAht {} + | tail -1)
validto=$(grep "valid_to" $query | cut -d '"' -f '4-' | cut -d '"' -f '1')
today=$(date -d now+30days +%s)                   #Adds 30 days to todays and formats output in seconds
validto1=$(date -d $validto +%s)                  #Formats license expiration date in seconds
validto=$(date -d $validto +"%b %d %Y")           #Formats license expiration date in human readable
#today=$(date -d now +"%b %m %Y")

if [ "$validto1" -le "$today" ]; then
            echo "CRITICAL - License expires in less than 30 days at $validto"
            exit 2
    elif [ "$validto1" -gt "$today" ]; then
            echo "OK - More than 30 days until license expire at $validto"
            exit 0
    else
            echo "Unknown, somethings wrong"
            exit 3
fi
