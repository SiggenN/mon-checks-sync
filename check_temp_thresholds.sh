#!/bin/bash

PROGNAME="$0"
VERSION="Version 1.0"
AUTHOR="Author:Sam Jigarehi och Farhan Islam"

print_version() {
  echo ""
  echo "$PROGNAME - $VERSION"
  echo "$AUTHOR"
}

print_help() {
  echo ""
  echo "$PROGNAME Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -o [Oid] -LW [LowWarning] -LC [LowCritical] -HW [HighWarning] -HC [HighCritical]"
  echo ""
  echo "This script has low and high thresholds for temperature"
}

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privmode)
    shift
    SNMPx=$1;;
    -X|--privpass)
    shift
    SNMPprivpass=$1;;
    -o|--oid)
    shift
    SNMPo=$1;;
    -LW|--lowWarning)
    shift
    LWARN=$1;;
    -LC|--lowCritical)
    shift
    LCRIT=$1;;
    -HW|--highWarning)
    shift
    HWARN=$1;;
    -HC|--highCritical)
    shift
    HCRIT=$1;;
    -help|-h)
    print_help
    exit
  esac
  shift
done

if [[ "$SNMPl" == "authPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SNMPo 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SNMPo 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SNMPo 2>&1)
else
  OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1)
fi

if [[ "$OUTPUT" -lt "$LCRIT" ]]; then
  STATUS="CRITICAL - $OUTPUT C"
  EXITCODE=2
elif [[ "$OUTPUT" -lt "$LWARN" ]]; then
  STATUS="WARNING - $OUTPUT C"
  EXITCODE=1
elif [[ "$OUTPUT" -gt "$HCRIT" ]]; then
  STATUS="CRITICAL - $OUTPUT C"
  EXITCODE=2
elif [[ "$OUTPUT" -gt "$HWARN" && "$OUTPUT" -lt "$HCRIT" ]]; then
  STATUS="WARNING - $OUTPUT C"
  EXITCODE=1
elif [[ "$OUTPUT" -lt "$HWARN" && "$OUTPUT" -gt "$LWARN" ]]; then
  STATUS="OK - $OUTPUT C"
  EXITCODE=0
fi

echo "$STATUS | 'Temperature'=$OUTPUT;$HWARN;$HCRIT;$LWARN;$LCRIT"
exit $EXITCODE
