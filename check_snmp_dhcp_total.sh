#!/bin/sh

PROGNAME=`basename $0`
VERSION="Version 0.3,"
AUTHOR="2014, Magnus Lidén (http://www.netnordic.se)"

##Will be $ARGX$ input from OP5 X=ARG number
SNMPc=$13        #Community for SNMP host
OIDN1=$1         #OID number for right dhcp
OIDN1=$2         #OID number for right dhcp
OIDN1=$3         #OID number for right dhcp
OIDN1=$4         #OID number for right dhcp
OIDN1=$5         #OID number for right dhcp
OIDN1=$6         #OID number for right dhcp
OIDN1=$7         #OID number for right dhcp
OIDN1=$8         #OID number for right dhcp
OIDN1=$9         #OID number for right dhcp
OIDN1=$10        #OID number for right dhcp
OIDN1=$11        #OID number for right dhcp
OIDN1=$12        #OID number for right dhcp
SERVER=$14	#HOSTNAME
##OID that is been used for this check
OID=.1.3.6.1.4.1.6527.3.1.2.4.2.24.1.4.$OIDN.1000
#Statuses for the service
ST_OK=0
ST_UK=3

print_version() {
    echo "$VERSION $AUTHOR"
}

print_help() {
    print_version $PROGNAME $VERSION
    echo ""
    echo "$PROGNAME is a Nagios plugin to check DHCP scopes on an ALU SR7."
    echo ""
    echo "Usage: $PROGNAME [DHCP scope number1-12] [SNMP Community]"
    echo ""
    exit $ST_UK
}

while test -n "$1"; do
case "$1" in
	-help|-h)
	print_help
	exit $ST_UK
	;;
#	*)
#	    echo "Unknown argument: $1"
#           print_help
#           exit $ST_UK
#		;;
esac
shift
done


#snmpwalk -v2c -c seabread07 10.141.1.1 $OID
output=`snmpget -v2c -c $SNMPc $SERVER $OID`
#output="SNMPv2-SMI::enterprises.6527.3.1.2.4.2.24.1.4.100.1000 = Gauge32: 400"
output1=`echo "$output" |  awk -F: '/Gauge32/ { getline; print $4 }'` #Takes the output removed everything except the whitespace and the value at the end after Gauge32:
output1=`echo "$output1" | sed 's/ //g'` #Removed the whitespace before the value
output2=$(echo "$output1/$MAXVAL*100" | bc -l) #Outputs the value in percentage
output3=`printf "%0.f\n" "$output2"` #Removed the decimals and rounds up the percentage value
output4=`echo "$output1 used out of $MAXVAL : $output3%|dhcp=$output3%;$WARN;$CRIT;;"` #Output for the check

if [ -n "$WARN" -a -n "$CRIT" ]
then
    if [ "$output1" -ge "$WARN" -a "$output1" -lt "$CRIT" ]
    then
        echo "WARNING - ${output4}"
	exit $ST_WR
    elif [ "$output1" -ge "$CRIT" ]
    then
        echo "CRITICAL - ${output4}"
	exit $ST_CR
    else
        echo "OK - ${output4}"
	exit $ST_OK
    fi
else
    echo "OK - ${output4}"
    exit $ST_OK
fi 

