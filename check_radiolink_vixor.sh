#!/bin/bash

AUTHOR="Author: Niklas Sigg"
VERSION="1.0"
PROGNAME="$0"

print_version() {
  echo ""
  echo "$PROGNAME - $VERSION"
  echo "$AUTHOR"
}

print_help() {
  echo ""
  "$PROGNAME is a plugin to check connectivity with radiolinks for a customer of MittNat."
  echo ""
  echo "Usage: $PROGNAME -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv or skip the flag if you're using snmpv1/snmpv2c) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -V [VLAN]"
}

#Flags
while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivpass=$1;;
    -V|--vlan)
    shift
    VLAN=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

OID="sysUpTimeInstance"

if [[ "$SNMPl" == "authPriv" ]]; then
  OUTPUT=$(snmpget -Ov -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  OUTPUT=$(snmpget -Ov -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  OUTPUT=$(snmpget -Ov -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID 2>&1)
else
  OUTPUT=$(snmpget -Ov -v $SNMPv -c $SNMPc $HOST:$VLAN $OID 2>&1)
fi

if [[ "$OUTPUT" == "Timeticks"* ]]; then
  STATUS="Link is reachable. Current uptime - $OUTPUT"
  EXITCODE=0
else
  STATUS="Link is unreachable."
  EXITCODE=2
fi

echo "$STATUS"
exit $EXITCODE
