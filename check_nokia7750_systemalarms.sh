#!/bin/bash

print_version() {
  echo ""
  echo "$VERSION"
  echo ""
  echo "$AUTHOR"
}

print_help() {

  print_version $PROGNAME $VERSION
  echo ""
  echo "$PROGNAME is a plugin to check system alarms on a NOKIA 7750"
  echo ""
  echo "The multiple oids are components, different 7750 have different ALARMOUTPUT when you run a snmpwalk with the basealarmoid"
  echo ""
  echo "Run a snmpwalk with basealarmoid and only include oids with INTEGER 1's and 2's "
  echo ""
  echo "Usage: $PROGNAME -H [Host] -v [SNMP version] -C [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv or skip the flag if you're using snmpv1/snmpv2c) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -o [OIDs string ';' sepparated]"
  echo "Mib file: TmnxAlarmState (INTEGER) {unknown (0), alarmActive (1), alarmCleared (2)"

}

BASEALARMOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.24.1"
BASENAMEOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.8.1"

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    snmp_community=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

#change newline delimiter from whitespace to \n
IFS=$'\n'

if [[ "$SNMPl" == "authPriv" ]]; then
    ALARMOUTPUT=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $BASEALARMOID 2>&1))
    SENSORNAME=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $BASENAMEOID 2>&1))
else
    ALARMOUTPUT=($(snmpwalk -v $SNMPv -c $snmp_community -Oqv $HOST $BASEALARMOID 2>&1))
    SENSORNAME=($(snmpwalk -v $SNMPv -c $snmp_community -Oqv $HOST $BASENAMEOID 2>&1))
fi

#($?) is a return value of previous command and 0 is a success or found. This means of previous command was not successful it will make an echo and then exit with nagios unknown code.
if [ $? -ne 0 ]; then
    echo "Execution failed. Rechecking your flags might help."
    exit 3
fi

#iterates through every line in the output from SENSORNAME. If the ALARMOUTPUT value equals 1 it will echo a string and execute another function.
alarm_active_function () {
for (( i = 0; i < ${#SENSORNAME[@]}; i++ )); do
    if [[ "${ALARMOUTPUT[i]}" == "1" ]]; then
        echo "${SENSORNAME[i]} Currently has an active alarm!"
        passive_output_function
    fi
done
}

#if ALARMOUTPUT is not empty it will echo a string depending on which value present in each iteration. 
passive_output_function () {
if [[ ${ALARMOUTPUT[i]} -ne "" ]]; then
    for (( i = 0; i < ${#SENSORNAME[@]}; i++ )); do
        if [[ "${ALARMOUTPUT[i]}" == "1" ]]; then
            echo "Critical - ${SENSORNAME[i]} - Active Alarm."
        elif [[ "${ALARMOUTPUT[i]}" == "2" ]]; then
            echo "OK - ${SENSORNAME[i]} - Alarm Cleared"
        elif [[ "${ALARMOUTPUT[i]}" == "0" ]]; then
            echo "OK - ${SENSORNAME[i]} - No Alarms Active"
        else
            echo "Alarms Unknown"
            exit 3
        fi
    ALARMACTIVE="$ALARMACTIVE ${SENSORNAME[$i]}: ${ALARMOUTPUT[$i]} "
    GRAPHACTIVE="$GRAPHACTIVE${SENSORNAME[$i]}(alarmoutput)=${ALARMOUTPUT[$i]}; "
    done
fi
}


if [[ "${ALARMOUTPUT[i]}" == "2" || "${ALARMOUTPUT[i]}" == "0" ]]; then
    echo "There are no active alarms at this time"
    passive_output_function
    exitcode=0
elif [[ "${ALARMOUTPUT[i]}" == "1" ]]; then
    alarm_active_function
    exitcode=2
else
    echo "Alarms Unknown"
    exitcode=3
fi

unset IFS
echo "|$GRAPHACTIVE"
exit $exitcode