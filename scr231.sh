#!/bin/bash

PROGNAME="check_7750_psu"
print_help()    {
        echo "$PROGNAME is a Icinga/OP5 plugin to check the status of PSUs on Nokia/Alcatel 7750."
        echo ""
        echo "Usage: $PROGNAME [HOST] [SNMP Community] [Second PSU slot#]"
}
declare -a CHECK

baseOID=".1.3.6.1.4.1.6527.3.1.2.2.1.5.1.6.1."
exitCode=0

HOST=$1         #HOST
SNMPv="2c"      #SNMP Version
SNMPc=$2        #SNMP Community
SLOT2=$3        #Second Psu Slot
SLOTS=('1' '$SLOT2')

for i in 0 4; do
        CHECK[$i]= snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $baseOID${SLOT2[$i]}
        if [ "${CHECK[$i]}" != "-1" ]
        then
                if [ "${CHECK[$i]}" == "3" ]
                then
                        CHECK[$i]=true
                else
                        CHECK[$i]=false
                fi
        else
                Status="OID error!"
                exitCode=3
        fi
done

if [ "$CHECK0" != "-1" ] && [ "$CHECK1" != "-1" ]
then
        if              [ $CHECK0 ] && [ $CHECK1 ]
        then
                Status="OK - power supplies working"
                exitCode=0
        elif    [ $CHECK0 ] || [ $CHECK1 ]
        then
                Status="Warning - PSU redundancy broken"
                exitCode=1
        fi
else
        Status="Unknown error!"
        exitCode=3
fi

echo $Status
exit $exitCode
