#!/usr/bin/env bash

SCRIPTDESC="check_nrpe_match.sh - Run a command with NRPE on two hosts and verify that the output is matching"

PLUGINPATH="/usr/lib64/nagios/plugins/check_nrpe"

COMMAND="$1"
HOST1="$2"
HOST2="$3"

if [[ -z "$COMMAND" ]] || [[ -z "$HOST1" ]] || [[ -z "$HOST2" ]]; then
    /bin/echo "UNKNOWN - Required arguments was not provided"
    /bin/echo -e "\n$SCRIPTDESC\nUsage: $0 'name_of_cmd' 'host1' 'host2'"
    exit "3"

fi

function execute_check {
    /bin/echo "$($PLUGINPATH -t "55" -H "$1" -c "$COMMAND" 2>&1)"

}

# Executes the command on both target hosts
OUTPUT1="$(execute_check "$HOST1")"

if [[ "$OUTPUT1" =~ "NRPE: " ]]; then
    /bin/echo "Failed to compare plugin output due to NRPE issues with host \"$HOST1\": $OUTPUT1"
    exit "3"

fi

OUTPUT2="$(execute_check "$HOST2")"

if [[ "$OUTPUT2" =~ "NRPE: " ]]; then
    /bin/echo "Failed to compare plugin output due to NRPE issues with host \"$HOST2\": $OUTPUT2"
    exit "3"

fi

# Tries to match the output
if [[ "$OUTPUT1" == "$OUTPUT2" ]]; then
    /bin/echo "MATCHING OK: $OUTPUT1"
    exit "0"

else
    /bin/echo "MATCHING CRITICAL: Failed to match output of command \"$COMMAND\" on host \"$HOST1\" and \"$HOST2\""
    exit "2"

fi