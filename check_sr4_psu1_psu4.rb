#!/usr/bin/ruby

# Copyright Packetfront Solutions AB - All rights reserved
# The check is still in alpha and needs a lot of cleanup

require 'rubygems'
require 'optparse'
require 'snmp'

include SNMP

options = {}

options[:host] = ""
options[:community] = "public"
options[:version] = "2c"
options[:debug] = false

opt_parser = OptionParser.new do |opt|
    opt.banner = "Usage: check_sr7_psu -H host [-C community] [-v 1,2c]"

    opt.on("-H","--host host","Host name or address") do |host|
        options[:host] = host
    end

    opt.on("-C", "--community [community]", "SNMP community string. Default: public") do |community|
        options[:community] = community || "public"
    end

    opt.on("-v", "--version [1,2c]", "SNMP version. Default: 2c") do |version|
        options[:version] = version || "2c"
    end

    opt.on("-d", "--debug", "Runs the check in debug mode") do
        options[:debug] = true
    end

    opt.on("-h","--help","Show this screen") do
        puts opt_parser
    end

end

opt_parser.parse!

if options[:host] == ""
    puts opt_parser
    exit 3
end

request = Manager.new(:host => options[:host], :community => options[:community])

if options[:debug]
    psu1 = request.get_value("1.3.6.1.4.1.6527.3.1.2.2.1.5.1.6.1.1")
    psu4 = request.get_value("1.3.6.1.4.1.6527.3.1.2.2.1.5.1.6.1.4")
    puts "\nDebug:\nPSU1 = #{psu1}\nPSU2 = #{psu2}"

end

psu1 = request.get_value("1.3.6.1.4.1.6527.3.1.2.2.1.5.1.6.1.1").to_s.include?("3")
psu4 = request.get_value("1.3.6.1.4.1.6527.3.1.2.2.1.5.1.6.1.4").to_s.include?("3")
request.close

# Checks the status of the power suppiles. In the future I want the faulty PSU to be printed.

if psu1 and psu4
    puts "OK - power supplies working"
    exit 0

elsif psu1 or psu4
    puts "Warning - PSU redundancy broken"
    exit 1

else
    puts "Unknown error!"
    exit 3

end

