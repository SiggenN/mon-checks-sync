#!/bin/bash

dbuser=$(grep "vDBUser=" /opt/packetfront/bbe/server/conf/db.conf | cut -d '=' -f '2')          #Get username and cut unnecessary data
dbname=$(grep "vDBName=" /opt/packetfront/bbe/server/conf/db.conf | cut -d '=' -f '2')          #Get database name and cut unnecessary data
query=$(/usr/pgsql-9.1/bin/psql -d bbedb -U app -h 127.0.0.1 -c 'select * from license order by id asc;' | grep "valid_to" | cut -d '"' -f '4-' | cut -d '"' -f '1' | sort -n -r | head -1)          #Get BBE license expiration date from sql database and cut unnecessary data
today1=$(date -d now+60days +%s)                #Adds 60 days to todays and formats output in seconds
validto1=$(date -d $query +%s)                  #Formats license expiration date in seconds
validto=$(date -d $query +"%b %d %Y")           #Formats license expiration date in human readable
#today=$(date -d now +"%b %m %Y")

#echo $query
#echo $today
#echo $today1
#echo $validto
#echo $validto1

if [ "$validto1" -le "$today1" ]; then
            echo "CRITICAL - License expires in less than 60 days at $validto"
            exit 2
    elif [ "$validto1" -gt "$today1" ]; then
            echo "OK - More than 60 days until license expire at $validto"
            exit 0
    else
            echo "Unknown, somethings wrong"
            exit 3
fi