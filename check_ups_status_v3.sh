#!/bin/bash

PROGNAME="UPS Status $0"
VERSION="Version 1.0"
AUTHOR="Author: Niklas Sigg"

print_version() {
  echo ""
  echo "$VERSION"
  echo ""
  echo "$AUTHOR"
}

print_help() {

  print_version $PROGNAME $VERSION
  echo ""
  echo "$PROGNAME is a plugin to check status on supporting OID's"
  echo ""
  echo "Usage: $PROGNAME -H [Host] -v [SNMP version] -C [SNMP community] -l [SEC level] -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -o [SNMP oid]"
}

while [[ $1 != "" ]]; do

  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -C|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -o|--oid)
    shift
    SNMPo=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

resp=" - Unit Status "

if [[ "$SNMPv" == "3" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST ${SNMPo} 2>&1)
elif [[ "$SNMPv" == "3" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST ${SNMPo} 2>&1)
elif [[ "$SNMPv" == "3" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST ${SNMPo} 2>&1)
else
  OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST ${SNMPo} 2>&1)
fi

if [ $? -ne 0 ]
then
  echo "UNKNOWN${resp}"
  exit 3
fi

case $OUTPUT in

  8)
    EXITCODE=1
    STATUS="Check Battery. Battery state is uncertain following a poor battery test result."
    ;;
  7)
    EXITCODE=0
    STATUS="Battery under test"
    ;;
  6)
    EXITCODE=2
    STATUS="Battery Disconnected"
    ;;
  5)
    EXITCODE=3
    STATUS="Battery Status Unknown"
    ;;
  4)
    EXITCODE=0
    STATUS="Battery Resting. The battery is fully charged and none of the other actions(charging/discharging/floating) is being done."
    ;;
  3)
    EXITCODE=0
    STATUS="Battery Floating. The charger is temporarily charging the battery to its float voltage."
    ;;
  2)
    EXITCODE=1
    STATUS="Battery Discharging"
    ;;
  1)
    EXITCODE=0
    STATUS="Battery Charging"
    ;;
  *)
    EXITCODE=3
    STATUS="Error!"
    ;;
esac

echo "$STATUS"
exit $EXITCODE
