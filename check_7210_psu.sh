#!/bin/bash

PROGNAME="check_7210_psu"
print_help()    {
        echo "$PROGNAME is a Icinga/OP5 plugin to check the status of PSUs on Alcatel 7210 SAS."
        echo ""
        echo "Usage: $PROGNAME [HOST] [SNMP Community]"
}

EXITCODE=3

declare -a CHECK
declare -a STATUS
declare -a ECODE

baseOID=".1.3.6.1.4.1.6527.3.1.2.2.1.5.1.2.1."

HOST=$1
SNMPv=2c
SNMPc=$2

for i in 1 2; do
  CHECK[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $baseOID$i`

  case ${CHECK[$i]} in
	1)
	  STATUS[$i]="Unknown"
	  ECODE[$i]=3
	  ;;
	2)
	  STATUS[$i]="NotEquipped"
	  ECODE[$i]=$i
	  ;;
	3)
	  STATUS[$i]="Ok"
	  ECODE[$i]=0
	  ;;
	4)
	  STATUS[$i]="Failed"
	  ECODE[$i]=$i
	  ;;
	5)
	  STATUS[$i]="OutOfService"
	  ECODE[$i]=1
	  ;;
	*)
	  STATUS[$i]="Error!"
	  ECODE[$i]=3
	  ;;
  esac
done

if [ "${ECODE[1]}" -eq 2 ] || [ "${ECODE[2]}" -eq 2 ]
then
  EXITCODE=2
elif [ "${ECODE[1]}" -gt "${ECODE[2]}" ]
then
  EXITCODE=${ECODE[1]}
else
  EXITCODE=${ECODE[2]}
fi

echo "PSU Slot1 State - ${STATUS[1]} , Slot2 State - ${STATUS[2]}"
exit $EXITCODE