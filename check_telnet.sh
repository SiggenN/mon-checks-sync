#!/bin/sh
#
# 20071220 Hakan Jerning, Nagios plugin to check that Telnet is responding
# 20080305 Hakan Jerning, Bugfix: Exit telnet session nicely, no more
#                          "TELNETD-3-READ: socket read(): Input/output error"
# 20080317 Hakan Jerning, Fixed: Exit correctly when not enough memory to login
# 20080903 Hakan Jerning, Increased timeout
# 20081008 Hakan Jerning, Fixed: Send "Start!" if "t" fails 
# 20081103 Hakan Jerning, Fixed again: Exit correctly when not enough memory to login
#

PROGPATH=`dirname $0`
. ${PROGPATH:=.}/utils.sh

usage()
{
	echo "USAGE: $0 <IP>"
	exit $STATE_CRITICAL
}

telnet_connect()
{
	expect -c "
		set timeout 100
		spawn telnet "$1"
		expect {
			?sername:       {send t\r; exp_continue}
			?assword:       {
						send t\r
						expect {
								?assword:	{send Start!\r}
								?hoice:		{puts \"\nThere is not enough memory to login\"; send q\r}
								>		{send \r}
						}
						exp_continue
					}
			>		{puts \"\nUP\"; send exit\r}
			timeout		{puts \"Timeout\"; exit}
			eof		{exit}
		}
		expect {
			timeout
			eof
		}"
}

test $# -eq 0 && usage

RESPONSE=$(telnet_connect $1 | grep -vE "(^q[^A-Za-Z0-9]*$|exit|?onnection closed)" | tail -1)

if [ "$RESPONSE" != "UP" ]
then
	echo "TELNET CRITICAL - $RESPONSE"
	exit $STATE_CRITICAL
else
	echo "TELNET OK"
	exit $STATE_OK
fi

echo "WTF?!"
exit $STATE_UNKNOWN
