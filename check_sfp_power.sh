#!/bin/bash

version="2c"
rx_oid=".1.3.6.1.4.1.9303.4.1.4.1.20" #note integer32 in microWatt
tx_oid=".1.3.6.1.4.1.9303.4.1.4.1.18"
diag_oid=".1.3.6.1.4.1.9303.4.1.4.1.17"
output="dBm"
function inform_user
{
echo "usage: ./check_sfp_power.sh [-c community ] [-H host] [-i interface] [-o mW ][-wr warning] [-cr critical] [-wt warning] [-ct critical] | [-h]"
echo "warning and critical thresholds can be specified as num1:num2 or num1 in microWatt (if -o mW is specified) or dBm (default)."
echo "e.g. ./check_sfp_power.sh -c test -i 25 -H 8.8.8.8 -wr -7:-2 -cr -10:-1 -wt -7:-2 -ct -10:-1"
}

if [[ $1 = "" ]]; then
inform_user
exit
fi
while [[ $1 != "" ]]; do

  case $1 in
    -c | --community )
    shift
    community=$1;;
    -H | --host )
    shift
    host=$1;;
    -i | --interface )
    shift
    interface=$1;;
    -wr | --warning_rx )
    shift
    warn_in=$1;;
    -cr | --critical_rx )
    shift
        crit_in=$1;;
        -wt | --warning_rx )
    shift
    warn_out=$1;;
    -ct | --critical_rx )
    shift
        crit_out=$1;;
    -v | --version )
        shift
    version=$1;;
        -o | --output )
        shift
    output=$1;;
    -h | --help )
    inform_user
    exit;;
  esac
  shift
done

if [[ ! $output == "dBm" ]] && [[ ! $output == "mW" ]]; then
  inform_user
  exit 0
fi
sfp_diag=`snmpget -v $version -c $community -Oqv $host $diag_oid.$interface`
if [[ $sfp_diag == "0" ]]; then
  echo "diagnostic not available"
  exit 1
fi
power_rx=`snmpget -v $version -c $community -Oqv $host $rx_oid.$interface`
power_tx=`snmpget -v $version -c $community -Oqv $host $tx_oid.$interface`
if [[ -z $power_rx ]]  || [[ -z $power_tx ]]; then
  echo "UNKNOWN: No values fetched"
  exit 3
fi

if [[ $output == "dBm" ]]; then
  power_rx=`bc -l <<< "scale=3; 10*l(${power_rx}/1000)/l(10)"`
  power_tx=`bc -l <<< "scale=3; 10*l(${power_tx}/1000)/l(10)"`
fi

#echo $power_rx $power_tx

if [[ $warn_in == *":"* ]]; then
  high_warn_in=`expr match ${warn_in} '.*:\(.*[[:digit:]].*\)'`
  low_warn_in=`expr match ${warn_in} '\(.*[[:digit:]].*\):'`
else
  high_warn_in=$warn_in
  low_warn_in=0
fi
if [[ $crit_in == *":"* ]]; then
  high_crit_in=`expr match ${crit_in} '.*:\(.*[[:digit:]].*\)'`
  low_crit_in=`expr match ${crit_in} '\(.*[[:digit:]].*\):'`
else
  high_crit_in=$crit_in
  low_crit_in=0
fi

if [[ $warn_out == *":"* ]]; then
  high_warn_out=`expr match ${warn_out} '.*:\(.*[[:digit:]].*\)'`
  low_warn_out=`expr match ${warn_out} '\(.*[[:digit:]].*\):'`
else
  high_warn_out=$warn_out
  low_warn_out=0
fi
if [[ $crit_out == *":"* ]]; then
  high_crit_out=`expr match ${crit_out} '.*:\(.*[[:digit:]].*\)'`
  low_crit_out=`expr match ${crit_out} '\(.*[[:digit:]].*\):'`
else
  high_crit_out=$crit_out
  low_crit_out=0
fi
#echo $low_warn_in $high_warn_in $low_crit_in $high_crit_in $low_warn_out $high_warn_out $low_crit_out $high_crit_out

if [[ `bc <<< "(${power_rx} >= ${low_warn_in}) && (${power_rx} <= ${high_warn_in}) && (${power_tx} >= ${low_warn_out}) && (${power_tx} <= ${high_warn_out})"` == 1 ]]; then
echo "OK RX($output)=${power_rx} TX($output)=${power_tx} |RX($output)=${power_rx};${low_warn_in};${low_crit_in}; TX($output)=${power_tx};${low_warn_out};${low_crit_out};" #This last bit is the annoying format Nagios wants for graphs etc.
exit 0
#elif [[ `bc <<< "${power_rx} < ${low_crit}"` ]] || [[ `bc <<< "${power_tx} < ${low_crit}"` ]] || [[ `bc <<< "${power_rx} > ${high_crit}"` ]] || [[ `bc <<< "${power_tx} > ${high_crit}"` ]]; then
elif [[ `bc <<< "(${power_rx} < ${low_crit_in}) || (${power_rx} > ${high_crit_in}) || (${power_tx} < ${low_crit_in}) || (${power_tx} > ${high_crit_out})"` == 1 ]]; then
echo "Critical RX($output)=${power_rx} TX($output)=${power_tx}|RX($output)=${power_rx};${low_warn_in};${low_crit_in}; TX($output)=${power_tx};${low_warn_out};${low_crit_out};"
exit 2
#elif [[ `bc <<< "${power_rx} < ${low_warn}"` ]] || [[ `bc <<< "${power_tx} < ${low_warn}"` ]] || [[ `bc <<< "${power_tx} < ${low_warn}"` ]] || [[ `bc <<< "${power_tx} < ${low_warn}"` ]]; then
elif [[ `bc <<< "(${power_rx} < ${low_warn_in}) || (${power_rx} > ${high_warn_in}) || (${power_tx} < ${low_warn_out}) || (${power_tx} > ${high_warn_out})"` == 1 ]]; then
echo "Warning RX($output)=${power_rx} TX($output)=${power_tx}|RX($output)=${power_rx};${low_warn_in};${low_crit_in}; TX($output)=${power_tx};${low_warn_out};${low_crit_out};"
exit 1
else
echo "Unknown error"
exit 3
fi

