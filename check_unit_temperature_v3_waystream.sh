#!/bin/bash

PROGNAME=`basename $0`
VERSION="Version 2.0"

print_version() {
   echo "$VERSION $AUTHOR"
}

print_help() {

   print_version $PROGNAME $VERSION
   echo ""
   echo "$PROGNAME is a plugin to check temperature on network supporing OID's"
   echo ""
   echo "Usage: $PROGNAME -H [host] -v [SNMP version] -C [SNMP community] -l [SEC level] -u [SNMP user] -A [SNMP pass] -a [Auth mode] -o [SNMP oid] -w [Warning] -c [Critical]"
   echo ""
}

while [[ $1 != "" ]]; do

   case $1 in
      -H | --host )
      shift
      HOST=$1;;
      -v | --version )
      shift
      SNMPv=$1;;
      -C | --community )
      shift
      SNMPc=$1;;
      -l | --seclevel )
      shift
      SNMPa=$1;;
      -u | --user )
      shift
      SNMPu=$1;;
      -A | --pass )
      shift
      SNMPpass=$1;;
      -a | --authmode )
      shift
      SNMPp=$1;;
      -o | --oid )
      shift
      SNMPo=$1;;
      -w  | --warn )
      shift
      WARNTEMP=$1;;
      -c | crit )
      shift
      CRITTEMP=$1;;
      -help | -h )
      print_help
      exit
   esac
   shift
done

ST_OK=0
ST_WR=1
ST_CR=2
ST_UK=3

#Standard response
resp=" - Unit temperature "

#Get current temperature value and strip away uneccesary info.
if [ "$SNMPv" == "3" ]
then
        OUTPUT=$(snmpget -O vq -v $SNMPv -c $SNMPc -l $SNMPa -u $SNMPu -a $SNMPp -A $SNMPpass $HOST ${SNMPo}  2>&1)
else
        OUTPUT=$(snmpget -O vq -v $SNMPv -c $SNMPc $HOST ${SNMPo}  2>&1)
fi
#Exit on SNMP error
EXIT_STATUS=$?
if [[ $EXIT_STATUS -ne 0 ]] || [[  $OUTPUT == 'No Such Object'* ]]
then
   echo "UNKNOWN${resp} "
   exit $ST_UK
fi

CURRTEMP=$(echo $OUTPUT | head -c-3)

CURRTEMP1=$(echo "${CURRTEMP} C | Temperature=$CURRTEMP;$WARNTEMP;$CRITTEMP;;")

if [ "$CURRTEMP" -ge "$WARNTEMP" -a "$CURRTEMP" -lt "$CRITTEMP" ]
   then
      echo "WARNING${resp}${CURRTEMP1}"
      exit $ST_WR
      elif [ "$CURRTEMP" -ge "$CRITTEMP" ]
      then
         echo "CRITICAL${resp}${CURRTEMP1}"
         exit $ST_CR
      elif [ "$CURRTEMP" -lt "$WARNTEMP" -a "$CURRTEMP" -lt "$CRITTEMP" ]
         then
         echo "OK${resp}${CURRTEMP1}"
         exit $ST_OK
      else
         echo "UNKNOWN - Unable to get temperature"
         exit $ST_UK
fi

