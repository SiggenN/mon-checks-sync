PROGNAME=`basename $0`
AUTHOR="Sami Ereq 2015 Netnordic AB"
VERSION="Version 2.0"

print_version() {
        echo "$VERSION $AUTHOR"
}

print_help() {
print_version $PROGNAME $VERSION
echo ""
echo "$PROGNAME is a Nagios plugin to check temp on networkelements supporting OID's"
echo ""
echo "Usage: $PROGNAME [SNMP community] [WARNING temp] [CRITICAL temp] [Host] [OID1] [OID2](Optional)"
echo ""
}

#Will be $ARGX$ input from OP5 X=ARG number
SNMPv="2c"
SNMPc=$1
WARNTEMP=$2
CRITTEMP=$3
HOST=$4
CURRTEMPOID1=$5
CURRTEMPOID2=$6         #Optional OID input, intended to be used for splitting similar OID's to get different sensors.

#Exit status for service
ST_OK=0
ST_WR=1
ST_CR=2
ST_UK=3

#Standard response
resp=" - Unit temperature: "

while test -n "$1"; do
        case "$1" in
                -help|-h)
                print_help
                exit $ST_UK;;
        esac
shift
done

#Get current temperature value and strip away uneccesary info.
OUTPUT="snmpget -O vq -v $SNMPv -c $SNMPc $HOST $CURRTEMPOID1$CURRTEMPOID2"
CURRTEMP=$($OUTPUT | cut -c 1-2)
CURRTEMP1=$(echo "${CURRTEMP} C | Temperature=$CURRTEMP;$WARNTEMP;$CRITTEMP;;")

#echo $CURRTEMP

if [ "$CURRTEMP" -ge "$WARNTEMP" -a "$CURRTEMP" -lt "$CRITTEMP" ]
		then
			echo "WARNING${resp}${CURRTEMP1}"
		exit $ST_WR
	elif [ "$CURRTEMP" -ge "$CRITTEMP" ]
		then
			echo "CRITICAL${resp}${CURRTEMP1}"
		exit $ST_CR
	elif [ "$CURRTEMP" -lt "$WARNTEMP" -a "$CURRTEMP" -lt "$CRITTEMP" ]
		then
			echo "OK${resp}${CURRTEMP1}"
		exit $ST_OK
	else
			echo "UNKNOWN${resp}${CURRTEMP1}"
		exit $ST_UK
fi
