#!/bin/bash

PROGNAME="check_pw9130_time"
print_help() {
echo "$PROGNAME is a Icinga plugin to check ABM status on Eaton Powerware 9130"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST]"
}

declare -i OUTP

OID=".1.3.6.1.4.1.534.1.2.1.0"	#time

SNMPv="1"	#SNMP Version
SNMPc=$1	#SNMP Community
HOST=$2		#HOSTIP

OUTP=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $OID`
(( OUTPm = OUTP / 60 ))

EXITCODE=0

echo "$OUTPm minutes remaining | 'Minutes Remaining'=$OUTPm;60;40 'Time Remaining (Seconds)'=$OUTP;3600;2400"
exit $EXITCODE
