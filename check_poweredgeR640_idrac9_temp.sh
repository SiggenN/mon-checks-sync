#!/bin/bash

# author    Niklas Sigg | NetNordic Sweden AB
# date      2020-08-20

print_help() {
    echo ""
    echo "This is a script for Icinga2, used for monitoring temperature on iDRAC9 interfaces. More specifically Poweredge R640 which utilizes 2 CPU's. For Poweredges with only 1 CPU the OID's are sometimes mixed up and this script will then show wrong temperatures."
    echo "$PROGNAME Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass]"
    echo ""
}

names="1.3.6.1.4.1.674.10892.5.4.700.20.1.8"
CPU1="1.3.6.1.4.1.674.10892.5.4.700.20.1.6.1.1"
CPU2="1.3.6.1.4.1.674.10892.5.4.700.20.1.6.1.2"
SystemBoardInletTemp="1.3.6.1.4.1.674.10892.5.4.700.20.1.6.1.3"
SystemBoardExhaustTemp="1.3.6.1.4.1.674.10892.5.4.700.20.1.6.1.4"

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

IFS=$'\n'
if [[ "$SNMPl" == "authPriv" ]]; then
  namearray=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $names 2>&1))
  cpu1output=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $CPU1 2>&1))
  cpu2output=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $CPU2 2>&1))
  inletoutput=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SystemBoardInletTemp 2>&1))
  exhaustoutput=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SystemBoardExhaustTemp 2>&1))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  namearray=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $names 2>&1))
  cpu1output=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $CPU1 2>&1))
  cpu2output=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $CPU2 2>&1))
  inletoutput=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SystemBoardInletTemp 2>&1))
  exhaustoutput=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SystemBoardExhaustTemp 2>&1))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  namearray=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $names 2>&1))
  cpu1output=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $CPU1 2>&1))
  cpu2output=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $CPU2 2>&1))
  inletoutput=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SystemBoardInletTemp 2>&1))
  exhaustoutput=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SystemBoardExhaustTemp 2>&1))
else
  namearray=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $names))
  cpu1output=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $CPU1))
  cpu2output=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $CPU2))
  inletoutput=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $SystemBoardInletTemp))
  exhaustoutput=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $SystemBoardExhaustTemp))
fi

#Divide the SNMPget result to get the correct display value. 
((cpu1output2=$cpu1output/10))
((cpu2output2=$cpu2output/10))
((inletoutput2=$inletoutput/10))
((exhaustoutput2=$exhaustoutput/10))

#Warning and Critical Thresholds
cpuwarn="50"
cpucrit="60"
inletwarn="32"
inletcrit="40"
exhaustwarn="40"
exhaustcrit="50"

warn_function() {
if [[ "$cpu1output2" > "$cpuwarn" || "$cpu2output2" > "$cpuwarn" || "$inletoutput2" > "$inletwarn" || "$exhaustoutput2" > "$exhaustwarn" ]]; then
    if [[ "$cpu1output2" > "$cpuwarn" ]]; then
        echo "CPU1 Temperature has exceeded the warning threshold at $cpuwarn C"
        exitcode=1
    fi
    if [[ "$cpu2output2" > "$cpuwarn" ]]; then
        echo "CPU2 Temperature has exceeded the warning threshold at $cpuwarn C"
        exitcode=1
    fi
    if [[ "$inletoutput2" > "$inletwarn" ]]; then
        echo "System Board Inlet Temperature has exceeded the warning threshold at $inletwarn C"
        exitcode=1
    fi
    if [[ "$exhaustoutput2" > "$exhaustwarn" ]]; then
        echo "System Board Exhaust Temperature has exceeded the warning threshold at $exhaustwarn C"
        exitcode=1
    fi
fi
}

crit_function () {
if [[ "$cpu1output2" > "$cpucrit" || "$cpu2output2" > "$cpucrit" || "$inletoutput2" > "$inletcrit" || "$exhaustoutput2" > "$exhaustcrit" ]]; then
    if [[ "$cpu1output2" > "$cpucrit" ]]; then
            echo "CPU1 Temperature has exceeded the critical threshold at $cpucrit C"
            exitcode=2
    fi
    if [[ "$cpu2output2" > "$cpucrit" ]]; then
            echo "CPU2 Temperature has exceeded the critical threshold at $cpucrit C"
            exitcode=2
    fi
    if [[ "$inletoutput2" > "$inletcrit" ]]; then
            echo "System Board Inlet Temperature has exceeded the critical threshold at $inletcrit C"
            exitcode=2
    fi
    if [[ "$exhaustoutput2" > "$exhaustcrit" ]]; then
            echo "System Board Exhaust Temperature has exceeded the critical threshold at $exhaustcrit C"
            exitcode=2
    fi
fi
}

if [[ "$cpu1output2" < "$cpuwarn" && "$cpu1output2" < "$cpucrit" && "$cpu2output2" < "$cpuwarn" && "$cpu2output2" < "$cpucrit" && "$inletoutput2" < "$inletwarn" && "$inletoutput2" < "$inletcrit" && "$exhaustoutput2" < "$exhaustwarn" && "$exhaustoutput2" < "$exhaustcrit" ]]; then
    echo "All Temperatures are OK."
    exitcode=0
elif [[ "$cpu1output2" > "$cpucrit" || "$cpu2output2" > "$cpucrit" || "$inletoutput2" > "$inletcrit" || "$exhaustoutput2" > "$exhaustcrit" ]]; then
    crit_function
elif [[ "$cpu1output2" > "$cpuwarn" || "$cpu2output2" > "$cpuwarn" || "$inletoutput2" > "$inletwarn" || "$exhaustoutput2" > "$exhaustwarn" ]]; then
    warn_function
else
    echo "Temperature is Unknown"
    exitcode=3
fi

for (( i = 0; i < ${#namearray[@]}; i++ )); do
    if [[ "${namearray[i]}" == *"CPU1"* ]]; then
        echo "Current CPU1 Temperature - $cpu1output2 C"
    elif [[ "${namearray[i]}" == *"CPU2"* ]]; then
        echo "Current CPU2 Temperature - $cpu2output2 C"
    elif [[ "${namearray[i]}" == *"Inlet"* ]]; then
        echo "Current System Board Inlet Temperature - $inletoutput2 C"
    elif [[ "${namearray[i]}" == *"Exhaust"* ]]; then
        echo "Current System Board Exhaust Temperature - $exhaustoutput2 C"
    else
        echo "Temperature Unknown"
        exit 3
    fi
done

unset IFS

exit $exitcode


