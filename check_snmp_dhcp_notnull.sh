#!/bin/sh

PROGNAME=`basename $0`
VERSION="Version 0.3,"
AUTHOR="2013, Philip Mattsson (http://www.netnordic.se)"

##Will be $ARGX$ input from OP5 X=ARG number
SNMPc=$4        #Community for SNMP host
OIDN=$1         #OID number for right dhcp
#MAXVAL=$2       #Max value
WARN=$2         #Warning value
CRIT=$3         #Critical valuei
SERVER=$5	#HOSTNAME
##OID that is been used for this check
OID=.1.3.6.1.4.1.6527.3.1.2.4.2.24.1.4.$OIDN.1000
#Statuses for the service
ST_OK=0
ST_WR=1
ST_CR=2
ST_UK=3

print_version() {
    echo "$VERSION $AUTHOR"
}

print_help() {
    print_version $PROGNAME $VERSION
    echo ""
    echo "$PROGNAME is a Nagios plugin to check that usage of DHCP scopes on an ALU SR7 is not null."
    echo ""
    echo "Usage: $PROGNAME [DHCP scope number] [Max value] [Warning] [Critical] [SNMP Community]"
    echo ""
    exit $ST_UK
}

while test -n "$1"; do
case "$1" in
	-help|-h)
	print_help
	exit $ST_UK
	;;
#	*)
#	    echo "Unknown argument: $1"
#           print_help
#           exit $ST_UK
#		;;
esac
shift
done


##snmpwalk to retrieve the output
#snmpwalk -v2c -c seabread07 10.141.1.1 $OID
#output=`snmpget -v2c -c $SNMPc $SERVER $OID`
#output="SNMPv2-SMI::enterprises.6527.3.1.2.4.2.24.1.4.100.1000 = Gauge32: 400"
#output1=`echo "$output" |  awk -F: '/Gauge32/ { getline; print $4 }'` #Takes the output removed everything except the whitespace and the value at the end after Gauge32:
#output1=`echo "$output1" | sed 's/ //g'` #Removed the whitespace before the value
#output2=$(echo "$output1/$MAXVAL*100" | bc -l) #Outputs the value in percentage
#output3=`printf "%0.f\n" "$output2"` #Removed the decimals and rounds up the percentage value
#output4=`echo "$output1 used out of $MAXVAL : $output3%|dhcp=$output3%;$WARN;$CRIT;;"` #Output for the check


##snmpwalk to retrieve the output
#snmpwalk -v2c -c seabread07 10.141.1.1 $OID
output=`snmpget -v2c -c $SNMPc $SERVER $OID`
#output="SNMPv2-SMI::enterprises.6527.3.1.2.4.2.24.1.4.100.1000 = Gauge32: 400"
output1=`echo "$output" |  awk -F: '/Gauge32/ { getline; print $4 }'` #Takes the output removed everything except the whitespace and the value at the end after Gauge32:
output1=`echo "$output1" | sed 's/ //g'` #Removed the whitespace before the value
#output2=$(echo "$output1/$MAXVAL*100" | bc -l) #Outputs the value in percentage
#output3=$(printf "%0.f\n" "$output2") #Removed the decimals and rounds up the percentage value
output4=`echo "$output1 used |dhcp=$output1;$WARN;$CRIT;;"` #Output for the check


if [ -n "$WARN" -a -n "$CRIT" ]
then
    if [ "$output1" -le "$WARN" -a "$output1" -gt "$CRIT" ]
    then
        echo "WARNING - ${output4}"
	exit $ST_WR
    elif [ "$output1" -le "$CRIT" ]
    then
        echo "CRITICAL - ${output4}"
	exit $ST_CR
    else
        echo "OK - ${output4}"
	exit $ST_OK
    fi
else
    echo "OK - ${output4}"
    exit $ST_OK
fi 

