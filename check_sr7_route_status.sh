#!/bin/bash

PROGNAME="check_sr7_route_status"
AUTHOR="Dmitrij Siskin 2017/01 NetNordic Solutions AB, Template by Sami Ereq"
VERSION="Version 1.0"

print_version() {
        echo "$VERSION $AUTHOR"
}

print_help() {
print_version $AUTHOR $VERSION
echo ""
echo "$PROGNAME is a OP5 plugin to check current routes on SR7 and SAS-M models"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST] [OID] [WARNiNG ROUTES] [CRITICAL ROUTES]"
}

#Will be $ARGX$ input from OP5 X=ARG number
SNMPv="2c"                                      #SNMP Version
SNMPc=$1                                        #SNMP Community
WARNROUTES=$2                                    #Warning threshold ROUTES
CRITROUTES=$3                                    #Critical threshold ROUTES
HOST=$4                                         #HOST
CURRROUTESOID="1.3.6.1.2.1.4.24.6.0"                         #OID to get number of routes that are currently active on this node

#Exit status for service

ST_OK=0                                         #ROUTES OK
ST_WR=1                                         #ROUTES Warning
ST_CR=2                                         #ROUTES Critical
#ST_UK=3                                        #Unknown stat


declare -i CURRROUTES
CURRROUTES=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $CURRROUTESOID`;

CURRROUTES1=$(echo "${CURRROUTES} | ROUTES=$CURRROUTES;$WARNROUTES;$CRITROUTES;;")

#CMDs after the pipe is for OP5 graphs.

if [ "$CURRROUTES" -gt "$WARNROUTES" -a "$CURRROUTES" -lt "$CRITROUTES" ]
        then
                echo "WARNING - Routes: ${CURRROUTES1}"
                exit $ST_WR
        elif [ "$CURRROUTES" -ge "$CRITROUTES" ]
                then
                echo "CRITICAL - Routes: ${CURRROUTES1}"
                exit $ST_CR
        else
                echo "OK - Routes: ${CURRROUTES1}"
                exit $ST_OK
fi

