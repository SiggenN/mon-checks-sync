#!/bin/bash

WORKER=$1
summary=$(/opt/otrs/bin/otrs.Console.pl Maint::Daemon::Summary)
UNIT=$(echo "$summary" | grep "Handled Worker Tasks:" -A20 | grep -i "$WORKER" | awk '{print $6}' | tr -d "\n\r")
STATUS=$(echo "$summary" | grep "Recurrent cron tasks:" -A20 | grep -i "$WORKER" | awk '{print $4}' | tr -d "\n\r")
TIME=$(echo "$summary" | grep "Handled Worker Tasks:" -A20 | grep -i "$WORKER" | awk '{print $5}' | sed "s/\..*//" | tr -d "\n\r")
RESPONSE="$WORKER has been running for $TIME $UNIT"
RESPONSE2="$WORKER has been running for $TIME $UNIT. READ: https://wiki.netnordic.se/x/QgZM"
#echo "$RESPONSE"
#echo "$summary"
#echo $WORKER
#echo $STATUS
#echo $UNIT
if [ -z $UNIT ]; then
    echo "$WORKER was not running at this check"
    exit 0
fi
if [ $STATUS == 'Fail' ]; then
    echo "$WORKER last run failed"
    exit 2
fi

if [ $UNIT -eq "1" ]; then
    echo "$WORKER has been running for less than a second"
    exit 0
else
    if [ $UNIT == "Hour(s)" ]; then
        echo "$RESPONSE2"
        exit 2
    elif [ $UNIT == "Minute(s)" -a $TIME -ge "5" ]; then
        echo "$RESPONSE2"
        exit 2
    elif [ $UNIT == "Minute(s)" -a $TIME -ge "1" ]; then
        echo "$RESPONSE2"
        exit 1
    elif [ $UNIT == "Second(s)" ]; then
        echo "$RESPONSE"
        exit 0
    else
        echo "Something is wrong, time running $TIME $UNIT"
        exit 3
    fi
fi
        echo "Something is wrong"
        exit 3
    fi
fi
