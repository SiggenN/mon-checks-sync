#!/bin/bash

PROGNAME="check_unit_temperature - Template"
AUTHOR="Sami Ereq 2015/07 Netnordic AB"
VERSION="Version 0.1"

print_version() {
        echo "$VERSION $AUTHOR"
}

print_help() {
print_version $AUTHOR $VERSION
echo ""
echo "$PROGNAME is a OP5 plugin to check temp on HP s5120-24G, may work on other models"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST] [OID] [WARNiNG TEMP] [CRITICAL TEMP]"
}

#Will be $ARGX$ input from OP5 X=ARG number
SNMPv="2c"					#SNMP Version
SNMPc=$1					#SNMP Community
WARNTEMP=$2					#Warning threshold temp
CRITTEMP=$3					#Critical threshold temp
HOST=$4						#HOST
CURRTEMPOID="1.3.6.1.4.1.9303.4.1.2.1.1.2.0"	#OID to get current temp. Script only processes one sensor.

#Exit status for service
ST_OK=0						#Temp OK
ST_WR=1						#Temp Warning
ST_CR=2						#Temp Critical
#ST_UK=3					#Unknown stat

#Get current temperature value and strip away uneccesary info.
OUTPUT="snmpget -v $SNMPv -c $SNMPc $HOST $CURRTEMPOID";
CURRTEMP=$($OUTPUT | awk '{print $NF}' | cut -c1-2);
#CURRTEMP1=$(expr $CURRTEMP / 100)  
CURRTEMP2=$(echo "${CURRTEMP1} C | Temperature=$CURRTEMP;$WARNTEMP;$CRITTEMP;;") #CMDs after the pipe is for OP5 graphs.


if [ "$CURRTEMP1" -gt "$WARNTEMP" -a "$CURRTEMP1" -lt "$CRITTEMP" ]
        then
                echo "WARNING - Unit Temperature: ${CURRTEMP2}"
                exit $ST_WR
        elif [ "$CURRTEMP1" -ge "$CRITTEMP" ]
                then
                echo "CRITICAL - Unit temperature: ${CURRTEMP2}"
                exit $ST_CR
        else
                echo "OK - Unit temperature: ${CURRTEMP2}"
                exit $ST_OK
fi
