#!/usr/bin/perl -w
#basic monitoring with snmp of an TS3100 tape drive from IBM
#v 0.2
#anders.dasilva@softronic.se
#%ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3,'DEPENDENT'=>4)

  use strict;
  use Nagios::Plugin;

  use lib "/usr/lib/nagios/plugins";
  use utils qw(%ERRORS $TIMEOUT);
  
  my $np = Nagios::Plugin->new( 
    usage => "Monitor for IBM TS3100", shortname => "check_ts3100"
  );
  $np->add_arg(
    spec=> 'warning|w=s',
    help=> "-w, --warning=RANGE",
    required=> 0
  );
  $np->add_arg(
    spec=> 'community|c=s',
    help=> "-c, --community=public",
    required=> 0
  );
  $np->add_arg(
    spec=> 'host|H=s',
    help=> "-H, --host=HOSTNAME",
    required=> 1
  );
  $np->add_arg(
    spec=> 'object|o=s',
    help=> "-o, --object=uptime|drive-clean|status|driveLoads",
    required=> 0
  );
  $np->getopts;
  my $host = $np->opts->host;
  my $community;
  if ($np->opts->community eq ''){
    $community = "public";
  }
  else{
    $community=$np->opts->community;
  }
  my $model="";
  my $nodrives=0;

  my $modelwalk=`snmpwalk -v 1 -c $community -On $host .1.3.6.1.2.1.1.1.0`;
  #Basic information
  if ($modelwalk =~ m/.*STRING: (.*)/i){
    $model = $1;
    #One or two drives
    my $drives=`snmpwalk -v 1 -c $community -On $host .1.3.6.1.4.1.2.6.210.3.2`;
    #ger driveEntryId of the last row in result
    if ($drives =~ m/210\.3\.2\.1\.1\.([1-2])/m){
      $nodrives=$1;
    }
  }
  else{
    $np->nagios_exit( CRITICAL, "No returned Model" );
  }

  if ($np->opts->object eq "uptime"){
    my $uptime=`snmpwalk -v 1 -c $community -On $host .1.3.6.1.2.1.1.3.0`;
    if ($uptime =~ m/.*Timeticks: \(\d*\)(.* \d{1,2}:\d{1,2}:\d{1,2})/i){ #place hours in $1
        $np->nagios_exit( OK, "$model Current uptime: $1");

      }
    else{
      $np->nagios_exit( CRITICAL, "No returned uptime" );
    }
  }
  if ($np->opts->object eq "drive-clean"){
  my $driveclean="";
  my $i;
  my $cleanwarning="";
  my @cleanstatus;
    for ($i = 1; $i <= $nodrives; $i++){
      $driveclean=`snmpwalk -v 1 -c $community -On $host .1.3.6.1.4.1.2.6.210.3.2.$i.13`;
      if ($driveclean =~ m/.* = INTEGER: (\d.*)/i){
        $driveclean = $1;
        push(@cleanstatus,$driveclean); 
        if ($driveclean == 5){
          #lägg in status i en array
          $cleanwarning=$cleanwarning."Drive $i requested drive-clean ";
        }
      }
      else{
        $np->nagios_exit( CRITICAL, "Drive $i: No returned drive-clean" );
      }
    }

  if (length($cleanwarning) > 1){
      $np->nagios_exit( WARNING,$cleanwarning);
  }
  else{
      $np->nagios_exit( OK,"$model drive-clean status: @cleanstatus");
  }
  }

  if ($np->opts->object eq "status"){
    my $globalstatus=`snmpwalk -v 1 -c $community -On $host .1.3.6.1.4.1.2.6.210.2.1`;
    if ($globalstatus =~ m/INTEGER: (.*)/i){ #place status integer in $1
      if ($1 == 3){
        $np->nagios_exit( OK,"Current status: ok");
      }
      elsif ($1 == 2){
        $np->nagios_exit( UNKNOWN,"Current status: unknown");
      }
      elsif ($1 == 1){
        $np->nagios_exit( WARNING,"Current status: other");
      }
      elsif ($1 == 4){
        $np->nagios_exit( WARNING,"Current status: non-critical");
      }
      elsif ($1 > 4){
        $np->nagios_exit( CRITICAL,"Current status: critical");
      }
    }
    else{
     $np->nagios_exit( CRITICAL, "No returned statusvalue" );
     }
  }
if ($np->opts->object eq "driveLoads"){
    my $globalstatus=`snmpwalk -v 1 -c $community -On $host .1.3.6.1.4.1.2.6.210.3.2.1.14`;
    if ($globalstatus =~ m/INTEGER: (.*)/i){
        $np->nagios_exit( OK, "Drive1 loads: $1");
    }
    else{
     $np->nagios_exit( CRITICAL, "No returned statusvalue" );
     }
  }
