#!/usr/bin/perl -w

use SOAP::Lite;# +trace;
#use Data::Dumper;
use Getopt::Long;
use Pod::Usage;
use strict;
my $becs_pri = '';
my $becs_port = 4490;
my $becs_user = '';
my $becs_pass = '';
my $ha_modules = '';
my $nonha_modules = '';
my $becs_sec = '';
my $module_uptime = 120;
my $synchtime = 120;
my $becs_cell = '';
my $soap_timeout = 10;
my $retries = 3;
my $help;
my $man;

GetOptions ('primary=s' => \$becs_pri,
            'user=s' => \$becs_user,
            'pass=s' => \$becs_pass,
            'cell=s' => \$becs_cell,
            'ha_modules=s' => \$ha_modules,
            'nonha_modules:s' => \$nonha_modules,
            'port:i' => \$becs_port,
            'uptime:i' => \$module_uptime,
            'synchtime:i' => \$synchtime,
            'secondary=s' => \$becs_sec,
            'help|?' => \$help,
             man => \$man)
|| pod2usage(3);
pod2usage(0) if $help;
pod2usage(-exitval => 0, -verbose => 2) if $man;

if (! ($becs_pri && $becs_sec && $becs_user && $becs_pass && $becs_cell && $ha_modules)) {
    pod2usage(3);
}

my @SOAP_ERRORS;
my $exitval = 0;
my $active='';
my $standby='';
my $output = "";
my $perfdata = "";
my $counter = 0;
my $mod_counter = 0;
my $ha_status;
 
if ($becs_cell =~ /^core$/i) {
    $becs_cell = '';
}
my @ha_module_ary = split /,/, $ha_modules;
my @nonha_module_ary = split /,/, $nonha_modules;

my $EAPI = new SOAP::Lite
  ->ns("urn:becs", "becs")
  ->proxy("http://${becs_pri}:${becs_port}/",timeout => $soap_timeout)
  ->on_fault(\&soapFaults);
my $becs_login;
eval {
  $becs_login = becs_call('sessionLogin',
          SOAP::Data->name("username" => $becs_user),
          SOAP::Data->name("password" => $becs_pass));
};
if (!$becs_login) {
    if ($becs_sec) {
        $EAPI = new SOAP::Lite
          ->ns("urn:becs", "becs")
          ->proxy("http://${becs_sec}:${becs_port}/",timeout => $soap_timeout)
          ->on_fault(\&soapFaults);

        eval { 
            $becs_login = becs_call('sessionLogin',
                SOAP::Data->name("username" => $becs_user),
                SOAP::Data->name("password" => $becs_pass));
        };
        if (!$becs_login) {
            print "Could not connect to BECS";
            exit 2;
        }

    }
    else {
        print "Could not connect to BECS";
        exit 2;
    }
}
my $SID = $becs_login->result->{sessionid};
$ha_status = becs_call('haStatusGet');
if (!($ha_status && $ha_status->can("result"))) {
    becs_call('sessionLogout');
    print "Could not retrieve HA status";
    exit 2;
}
my $timestamp = time();
foreach my $obj (@{$ha_status->result->{state}}) {    
    if ($becs_cell eq $obj->{cell}) {
        $counter += 1;
        #print qq($obj->{nodestate}\n);
        if (!($obj->{flags} =~ /Up/)) {
            if (! $obj->{flags}) {#flags is empty when the peer is down
                $output = $output . qq($obj->{hostname} Peer is Down\n);
            }
            else {
                $output = $output . qq($obj->{hostname} Peer is $obj->{flags}\n);
            }
            if ($obj->{nodestate} eq 'active') { #if the node having the bad flags is the active one it is not critical, since flags seem to come from the peer
                $exitval = 1 unless $exitval > 1;
            }
            else {
                $exitval = 2;
            }
        }
        if ($obj->{nodestate} eq 'standby' || $obj->{nodestate} eq 'synchronising') {
            $standby = $obj->{hostname};
        }
        elsif ($obj->{nodestate} eq 'active') {
            $active = $obj->{hostname};
        }
        my @tmp_mods = @ha_module_ary;
        
        foreach my $module (@{$obj->{modules}}) {
            #print Dumper \$module;
            my $index = 0;
            ++$index until (($index > $#tmp_mods) or (lc($tmp_mods[$index]) eq lc($module->{name})));
            if ($index <= $#tmp_mods) {
                splice(@tmp_mods,$index,1);
                if ($module->{modulestate} eq 'active' && (!($module->{lastmessage} eq 'activate.done' or 'diskspace.sufficient'))) {
                    $output = $output . qq($module->{name} last HA message was $module->{lastmessage} (expected activate.done or diskspace.sufficient) on $obj->{hostname}\n);
                    $exitval = 2;
                }
                elsif ($module->{modulestate} eq 'synchronising') {
                    my $timediff = $timestamp - $module->{time}->{seconds};
                    if ($timediff - $synchtime <= 0) {
                        $output = $output . qq($module->{name} is in state synchronising (expected standby) for $timediff seconds on $obj->{hostname}\n);
                        $exitval = 1 unless $exitval > 1;
                    }
                }
                else {
                    my $app_status = becs_call('applicationStatusGet', SOAP::Data->name("name" => $module->{applicationid}));
                    if (!$app_status) {
                        $output = $output . "Could not get application status for " . $module->{applicationid} . "\n";
                        $exitval = 1 unless $exitval > 1; 
                    }
                    #print Dumper \$app_status->result; 
                    elsif ($app_status->result->{uptime} <= $module_uptime) {
                        $output = $output . $app_status->result->{displayname} . " ($module->{modulestate} state) has recently been restarted, uptime is " . $app_status->result->{uptime} . " seconds\n";
                        if ($module->{modulestate} eq 'active') {
                            $exitval = 2;
                        }
                        else {
                            $exitval = 1 unless $exitval > 1;
                        }    
                    }
                    else {
                        $perfdata = $perfdata . $app_status->result->{displayname} . '_cpu60=' . $app_status->result->{cpuaverage60} . ';;;; ';
                    }
                }
            }
        }
        if (@tmp_mods) {
            $output = $output . qq(Some processes are not running on $obj->{hostname} : );
            foreach my $failmod (@tmp_mods) {
                $output = $output . $failmod . " ";
            }
            if ($obj->{nodestate} eq 'active') {
                $exitval = 2;
            }
            else {
                $exitval = 1 unless $exitval > 1;
            }
            $output = $output . "\n";
        }
    }
}
if (!$counter) {
    $output = qq(Could not retrieve HA status for $becs_cell\n);
    becs_call('sessionLogout');
    $exitval = 2;
    print("$output");
    exit $exitval;
}

my $apps = becs_call('applicationList');

foreach my $module (@nonha_module_ary) {
    my $counter = 0;
    my @applist = grep(/$module/,@{$apps->result->{names}});
    my $pri = 0;
    my $sec = 0;
    #print Dumper \@applist;
    foreach my $app (@applist) {
        my $app_status = becs_call('applicationStatusGet', SOAP::Data->name("name" => $app));
        if (!$app_status) {
            $output = $output . "Could not get application status for " . $app . "\n";
            $exitval = 1 unless $exitval > 1; 
        }
        elsif ($app_status->result->{hostname} eq $active) {
            $pri = 1;
            if ($app_status->result->{uptime} <= $module_uptime) {
                $output = $output . "$module has been restarted too recently on $active\n";
                $exitval = 2;
            }
            else {
                $perfdata = $perfdata . $app_status->result->{displayname} . '_cpu60=' . $app_status->result->{cpuaverage60} . ';;;; ';
            }
        }
        elsif ($app_status->result->{hostname} eq $standby) {
            $sec = 1;
            if ($app_status->result->{uptime} <= $module_uptime) {
                $output = $output . "$module has been restarted too recently on $standby\n";
                $exitval = 1 unless $exitval > 1;
            }
            else {
                $perfdata = $perfdata . $app_status->result->{displayname} . '_cpu60=' . $app_status->result->{cpuaverage60} . ';;;; ';
            }
        }
    }
    if (! $pri) {
        if ($active) {
            $output = $output . "$module is not running on $active\n";
            $exitval = 2;
        }
    }
    if (! $sec) {
        if ($standby) {
            $output = $output . "$module is not running on $standby\n";
            $exitval = 1 unless $exitval > 1;
        }
    }
}
becs_call('sessionLogout');


#print Dumper \$ha_status;
if ($exitval == 2) {
    $output = 'CRITICAL: ' . $output;
}
elsif ($exitval == 3) {
    $output = 'UNKNOWN: ' . $output;
}
elsif ($exitval == 1) {
    $output = 'WARNING: ' . $output;
}
else {
    $output = 'OK: ' . $output . "$ha_modules,$nonha_modules are running on cluster\n";
}
if ($active) {
    $output = $output . "$active is active in HA\n";
}
else {
    $output = $output . "no active node found in HA (possible failover in progress)\n";
}
if (@SOAP_ERRORS) {
    if (stringify_soap_errors()) {
        $output .= "Some errors were encountered during execution:\n";
        $output .= stringify_soap_errors();
    }
}

print("$output" . "|" . "$perfdata");
exit $exitval;


sub build_vars {
    my ($arrs) = @_;

    my @ret = ();
    foreach my $arr (@$arrs) {
    push @ret, SOAP::Data->name($arr->[0], $arr->[2])->type($arr->[1]);
    }

    return @ret;
}


sub build_array {
    my ($name, $type, @vars) = @_;

    my @varr = ();
    push @varr, SOAP::Data->name("item" =>
                 \SOAP::Data->value(@vars)->type($type));
    return SOAP::Data->name(
    $name => \SOAP::Data->value(
        SOAP::Data->name($name . "Item" => @varr)->
        type($type)))->type("ArrayOf_" . $type);
}


sub call_check {
    my ($r) = shift;
    if ($r->can("result") && $r->result->{err} != 0) {
        print "errtxt: \"" . $r->result->{errtxt} . "\"";
        return $r->result->{errtxt};
    }
}

sub stringify_soap_errors {
    my $err = "";
    foreach my $error (@SOAP_ERRORS) {
        if (! ($error->{becs_method} eq "sessionLogin") ) {
            $err .= $error->{error} . " at line " . $error->{line} . " calling method " . $error->{becs_method};
            $err .= "\n";
        }
    }
    return $err;
}

sub soapFaults {
    my $client = shift;
    my $result = shift;
    if ( ref($result) ) {
        chomp( my $err = $result->faultstring );
        push( @SOAP_ERRORS, {"error" => "SOAP FAULT: $err"} );
    }
    else {
        chomp( my $err = $client->transport->status );
        push( @SOAP_ERRORS, {"error" => "TRANSPORT ERROR: $err"} );
    }
    sleep 1;
}

sub becs_call {
    my ($meth) = shift;
    my (@data) = @_;
    my ($r);
    my $i = 1;
    my $hdr;
    if ($SID) {
        $hdr = SOAP::Header->name("request" =>
            \SOAP::Data->value(SOAP::Data->name("sessionid" => $SID)));
    }
    while ($i <= $retries) {
        my $errors = $#SOAP_ERRORS;
        eval {
            if ($hdr) {
                $r = $EAPI->$meth($hdr,
                SOAP::Data->name("in" =>
                       \SOAP::Data->value(@data)));
                }
            else {
                $r = $EAPI->$meth(SOAP::Data->name("in" =>
                       \SOAP::Data->value(@data)));
                }
        };
        if ($#SOAP_ERRORS == $errors) {
            my $becs_error = call_check($r);
            if (! $becs_error) {
                last;
            }
            else {
                push( @SOAP_ERRORS, {"error" => "BECS ERROR: $becs_error"} );
            }
        }
        my ($package, $filename, $line) = caller;
        $SOAP_ERRORS[$#SOAP_ERRORS]{line} = $line;
        $SOAP_ERRORS[$#SOAP_ERRORS]{becs_method} = $meth;
        $i = $i + 1;
    } 
    if ($i > $retries) {
        return;
    }
    return $r;
}
__END__

=head1 NAME

check_becs_ha - Retrieves BECS HA information for a server-pair.

=head1 SYNOPSIS

check_becs_ha [options]
Options:
-help            brief help message
-man             full documentation
-user            BECS user
-pass            BECS user pw
-primary         primary BECS server
-secondary       secondary BECS server, if applicable
-port            port for BECS EAPI
-uptime          limit in seconds since last respawn of a becs module
-cell            the becs cell to check for modules, if empty the core server modules are checked
-ha_modules      comma-separated list of becs HA modules (typically element managers)
-nonha_modules   comma-separated list of becs standalone modules (EM-BECS and EM-IBOS for example)
-synchtime       limit in seconds a cell has been synchronizing

=head1 OPTIONS

=over 4

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=item B<-user>

BECS user

=item B<-pass>

BECS user password

=item B<-primary>

Primary BECS server (in a HA configuration)

=item B<-secondary>

Secondary BECS server (in a HA configuration). Optional

=item B<-port>

BECS eAPI port, defaults to 4490

=item B<-uptime>

limit in seconds since last respawn of a becs module

=item B<-cell>

name of the becs cell to check

=item B<-ha_modules>

comma-separated list of becs HA modules (typically element managers excluding EM-IBOS)

=item B<-nonha_modules>

comma-separated list of becs standalone modules (EM-BECS and EM-IBOS for example)

=item B<-synchtime>

limit in seconds a cell has been synchronizing

=back

=head1 DESCRIPTION

B<check_becs_ha> - Retrieves BECS HA information for a server-pair.

=cut