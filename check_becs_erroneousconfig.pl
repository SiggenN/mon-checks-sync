#!/usr/bin/perl -w
use SOAP::Lite;# +trace;

$VERSION = "1.1";
$AUTHOR = "(c) 2019 Jonas Jacobsson / NetNordic Solutions AB";

##Changelog
#1.1 - added Huawei as platform
#

sub print_version {
    print "check_becs_scopes.pl version $VERSION,\n$AUTHOR\n\n";
}

sub print_help {
    print "check_becs_erroneousconfig.pl is a Nagios plugin to check if any\n",
    "iBOS or Huawei element got stuck in status Lock:ErroneousConfig. This plugin uses SOAP\n",
    "to connect to BECS EAPI.\n\n";
}

sub print_usage {
    print "Usage:\ncheck_becs_erroneousconfig.pl <becs-pri> <becs-sec> <user> <password>\n",
    "       check_becs_erroneousconfig.pl -h|--help\n",
    "       check_becs_erroneousconfig.pl -v|--version\n\n";
}

my $ARGVS = ($#ARGV + 1);

if ($ARGVS == 1 && ($ARGV[0] eq "-h" || $ARGV[0] eq "--help")) {
    print_version();
    print_help();
    print_usage();
    exit 0;
} elsif ($ARGVS == 1 && ($ARGV[0] eq "-v" || $ARGV[0] eq "--version")) {
    print_version();
    exit 0;
} elsif ($ARGVS != 4) {
    print_usage();
    exit 0;
}

my $BECS_SERVER_PRI = $ARGV[0];
my $BECS_SERVER_SEC = $ARGV[1];
my $BECS_USER = $ARGV[2];
my $BECS_PASS = $ARGV[3];
my $BECS_PORT = "4490";

my $EAPI = SOAP::Lite->new(#service=>"http://${BECS_SERVER}:${BECS_PORT}/becs.wsdl",
            proxy=>"http://${BECS_SERVER_PRI}:${BECS_PORT}/");
$EAPI->ns("urn:becs", "becs");

my @error_elements;
my $checked_elements = 0;
# if (!$EAPI) {
#     die("service $!\n");
# }
my $becs_login;
eval {
  $becs_login = becs_call('sessionLogin',
          SOAP::Data->name("username" => $BECS_USER),
          SOAP::Data->name("password" => $BECS_PASS));
};
if (!$becs_login) {
    $EAPI = SOAP::Lite->new(#service=>"http://${BECS_SERVER}:${BECS_PORT}/becs.wsdl",
            proxy=>"http://${BECS_SERVER_SEC}:${BECS_PORT}/");
    $EAPI->ns("urn:becs", "becs");
    $becs_login = becs_call('sessionLogin',
          SOAP::Data->name("username" => $BECS_USER),
          SOAP::Data->name("password" => $BECS_PASS));
}
my $SID = $becs_login->result->{sessionid};

$elementsr = becs_call('objectFind',
                build_array("queries", "object",
                    build_vars([["class", "string", "element-attach"]]),
                    build_array("parameters", "parameterKey",
                        build_vars([["name", "string", "platform"]]),
                        build_array("values", "parameterValue",
                            build_vars([["value", "string", "(asr.*|ms.k|mpc.*|huawei.*)"]])
                        )
                    )
                )
            );

foreach my $element (@{$elementsr->result->{objects}}) {

    $statusr = becs_call('elementStatusGet',
            build_vars([["cell", "string", ""]]),
               SOAP::Data->name("oids" =>
                                \SOAP::Data->value(
                                    SOAP::Data->name("oid" => $element->{oid})->type("unsignedLong")))->type("elementStatusGetInoids"));

    foreach my $status (@{$statusr->result->{status}}) {
#        print "element: " . $element->{oid} . " status: " . $status->{status} . "\n";
        $checked_elements++;
        if ($status->{status} =~ "Lock:ErroneousConfig") {
#            print "adding element: " . $element->{oid} . " status: " . $status->{status} . "\n";
            push @error_elements, $element->{name};
        }
    }

}

becs_call('sessionLogout');
my $errors = (scalar @error_elements);
if ($errors > 0) {
    print "WARNING -  $errors of $checked_elements elements have ErroneousConfig status: @error_elements";
    $exitval = 1;
} else {
    print "OK - None of $checked_elements elements are in ErroneousConfig status";
    $exitval = 0;
}
 
exit $exitval;

# END OF PROGRAM

sub build_vars {
    my ($arrs) = @_;
    my @ret = ();
    foreach my $arr (@$arrs) {
        push @ret, SOAP::Data->name($arr->[0], $arr->[2])->type($arr->[1]);
    }
    return @ret;
}


sub build_array {
    my ($name, $type, @vars) = @_;
    @varr = ();
    push @varr, SOAP::Data->name("item" =>
                 \SOAP::Data->value(@vars)->type($type));
    my @ret = SOAP::Data->name(
    $name => \SOAP::Data->value(
        SOAP::Data->name($name . "Item" => @varr)->
        type($type)))->type("ArrayOf_" . $type);
    return @ret;
}


sub call_check {
    my ($r) = shift;
    if ($r->fault) {
        die "faultstring: \"" . $r->fault->{ faultstring } . "\"";
        return 0;
    }
    if ($r->result->{err} != 0) {
        die "errtxt: \"" . $r->result->{errtxt} . "\"";
        return 0;
    }
    
    return 1;
}


sub becs_call {
    my ($meth) = shift;
    my (@data) = @_;
    my ($r);
    if ($SID) {
    my $hdr = SOAP::Header->name("request" =>
                     \SOAP::Data->value(SOAP::Data->name("sessionid" => $SID)));
    $r = $EAPI->$meth($hdr,
              SOAP::Data->name("in" =>
                       \SOAP::Data->value(@data)));
    } else {
        $r = $EAPI->$meth(SOAP::Data->name("in" =>
                       \SOAP::Data->value(@data)));
    }

    return undef if !call_check($r);

    return $r;
}
