#!/bin/bash

author="Author: Niklas Sigg"

print_help() {
    echo ""
    echo "$author"
    echo "This script is used for monitoring DHCP usage on Huawei ES20E or hardware using same OIDs."
    echo "Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass]"
    echo "       -x [Privacy mode] -X [Privacy pass]"
    echo ""
}

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

namedesc=".1.3.6.1.4.1.2011.6.8.1.1.1.2"
total=".1.3.6.1.4.1.2011.6.8.1.1.1.15"
used=".1.3.6.1.4.1.2011.6.8.1.1.1.16"
usedperc=".1.3.6.1.4.1.2011.6.8.1.1.1.20"
excluded=".1.3.6.1.4.1.2011.6.8.1.1.1.18"
idle=".1.3.6.1.4.1.2011.6.8.1.1.1.19"

if [[ "$SNMPv" == "1" || "$SNMPv" == "2" || "$SNMPv" == "2c" ]]; then
        nameout=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $namedesc 2>&1 | sed 's/["]//g'))
        totalout=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $total 2>&1))
        usedout=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $used 2>&1))
        usedpercout=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $usedperc 2>&1))
        excludedout=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $excluded 2>&1))
        idleout=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $idle 2>&1))
    elif [[ "$SNMPl" == "authPriv" ]]; then
        nameout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $namedesc 2>&1 | sed 's/["]//g'))
        totalout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $total 2>&1))
        usedout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $used 2>&1))
        usedpercout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $usedperc 2>&1))
        excludedout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $excluded 2>&1))
        idleout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $idle 2>&1))
    elif [[ "$SNMPl" == "authNoPriv" ]]; then
        nameout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $namedesc 2>&1 | sed 's/["]//g'))
        totalout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $total 2>&1))
        usedout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $used 2>&1))
        usedpercout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $usedperc 2>&1))
        excludedout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $excluded 2>&1))
        idleout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $idle 2>&1))
    elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
        nameout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu $HOST $namedesc 2>&1 | sed 's/["]//g'))
        totalout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu $HOST $total 2>&1))
        usedout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu $HOST $used 2>&1))
        usedpercout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu $HOST $usedperc 2>&1))
        excludedout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu $HOST $excluded 2>&1))
        idleout=($(snmpwalk -Oqv -v $SNMPv -l $SNMPl -u $SNMPu $HOST $idle 2>&1))
    else
        echo "Something wrong with -v or -l flag"
fi
echo "DHCP Usage:"
exitcode=0
dhcp_function () {

    for ((i=0;i<${#nameout[@]};i++)); do
        if [[ "${usedpercout[i]}" -ge "75" && "${usedpercout[i]}" -lt "95" ]]; then
            echo "Critical! ${nameout[i]} DHCP usage is above 75%!"
            exitcode=1
        fi
    done
}

dhcp_function
echo ""

printf '%s\n' "Name:" > '/tmp/dhcpnameoutput.txt'
printf '%s\n' "Percent:" > '/tmp/dhcpusedpercentoutput.txt'
printf '%s\n' "Total:" > '/tmp/dhcptotaloutput.txt'
printf '%s\n' "Used:" > '/tmp/dhcpusedoutput.txt'
printf '%s\n' "Excluded:" > '/tmp/dhcpexcludedoutput.txt'
printf '%s\n' "Idle:" > '/tmp/dhcpidleoutput.txt'

printf '%s\n' ${nameout[@]} >> '/tmp/dhcpnameoutput.txt'
printf '%s\n' ${usedpercout[@]} >> '/tmp/dhcpusedpercentoutput.txt'
printf '%s\n' ${totalout[@]} >> '/tmp/dhcptotaloutput.txt'
printf '%s\n' ${usedout[@]} >> '/tmp/dhcpusedoutput.txt'
printf '%s\n' ${excludedout[@]} >> '/tmp/dhcpexcludedoutput.txt'
printf '%s\n' ${idleout[@]} >> '/tmp/dhcpidleoutput.txt'

paste /tmp/dhcpnameoutput.txt /tmp/dhcpusedpercentoutput.txt /tmp/dhcpusedoutput.txt /tmp/dhcptotaloutput.txt /tmp/dhcpexcludedoutput.txt /tmp/dhcpidleoutput.txt | column -tc2

exit $exitcode