#!/bin/bash

AUTHOR="Author: Niklas Sigg"
VERSION="VERSION 1.0"
PROGNAME="$0"

print_version() {
  echo ""
  echo "$PROGNAME - $VERSION"
  echo "$AUTHOR"
  echo ""
}

print_help() {
  echo ""
  echo "$PROGNAME"
  echo "Usage: $PROGNAME -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv or skip the flag if you're using snmpv1/snmpv2c) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -ot [oid for time (minutes)] -oc [oid for charge(capacity%)]"
  echo ""
}

#Flags
while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -C|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -ot|--oidtime)
    shift
    OIDTIME=$1;;
    -oc|--oidcharge)
    shift
    OIDCHARGE=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

CHAOID=$OIDCHARGE
MINOID=$OIDTIME

if [[ "$SNMPl" == "authPriv" ]]; then
  TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $MINOID 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $MINOID 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $MINOID 2>&1)
else
  TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $MINOID 2>&1)
fi

if [[ "$SNMPl" == "authPriv" ]]; then
  CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $CHAOID 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $CHAOID 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $CHAOID 2>&1)
else
  CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $CHAOID 2>&1)
fi

if [[ $CHARGEOUTPUT -ge "100" ]]; then
  STATUS="Battery is at full capacity($CHARGEOUTPUT%). Current battery time is $TIMEOUTPUT minutes."
  EXITCODE=0
elif [[ $CHARGEOUTPUT -lt "100" && $CHARGEOUTPUT -ge "90" ]]; then
  STATUS="Battery capacity is at $CHARGEOUTPUT%. Current battery time is $TIMEOUTPUT minutes."
  EXITCODE=0
elif [[ $CHARGEOUTPUT -lt "90" && $CHARGEOUTPUT -ge "70" ]]; then
  STATUS="Battery capacity is at $CHARGEOUTPUT%. Current battery time is $TIMEOUTPUT minutes."
  EXITCODE=1
elif [[ $CHARGEOUTPUT -lt "70" ]]; then
  STATUS="Battery capacity is at $CHARGEOUTPUT%. Current battery time is $TIMEOUTPUT minutes."
  EXITCODE=2
else
  STATUS="Capacity and time unknown."
  EXITCODE=3
fi

echo "$STATUS | 'Minutes Remaining'=$TIMEOUTPUT;90;60 'Capacity'=$CHARGEOUTPUT%;90;70"
exit $EXITCODE
