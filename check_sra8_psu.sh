#!/bin/bash

PROGNAME="check_7750_psu"
print_help()    {
        echo "$PROGNAME is a Icinga/OP5 plugin to check the status of PSUs on Nokia/Alcatel 7750."
        echo ""
        echo "Usage: $PROGNAME [HOST] [SNMP Community] [Second PSU slot#]"
}
declare -a CHECK

baseOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.16.1.8388608"
exitCode=0

HOST=$1         #HOST
SNMPv="2c"      #SNMP Version
SNMPc=$2        #SNMP Community
#SLOT2=$3        #Second Psu Slot
SLOTS=("1" "$3")

for i in 0 1; do
        CHECK[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $baseOID${SLOTS[$i]}`
done

if [ "${CHECK[0]}" != "-1" ] && [ "${CHECK[1]}" != "-1" ]
then
        if              [ "${CHECK[0]}" == 2 ] && [ "${CHECK[1]}" == 2 ]
        then
                Status="OK - power supplies working"
                exitCode=0
        elif    [ "${CHECK[0]}" != 2 ] || [ "${CHECK[1]}" != 2 ]
        then
                Status="Warning - PSU redundancy broken"
                exitCode=1
        fi
else
        Status="Unknown error!"
        exitCode=3
fi

echo "$Status"
exit $exitCode