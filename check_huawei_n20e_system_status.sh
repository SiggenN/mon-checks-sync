#!/usr/bin/env bash
PROGNAME="check_huawei_n20e_system_status"
print_help() {
  echo "$PROGNAME is a Nagios plugin to check system (i.e CPU & Memory) status on Huawei N20E (V800R010C10SPC500),
may work with other models"
  echo ""
  echo "Usage: $PROGNAME -h [host] -c [community] -m ['CPU', 'MEM']"
}

declare -A ENTITIES
declare -A RESULT
declare -A ECODE
declare -A STATUS
declare -A ObjectOID=( ["CPU"]=".5" ["CPUthreshold"]=".6" ["MEM"]=".7" ["MEMthreshold"]=".8" )
declare -A OUTPUT=()

TableOID=".1.3.6.1.4.1.2011.5.25.31.1.1.1.1" # hwEntityStateTable + .1

host=$1
community=$2
mode=$3

ThreshOID="${mode}threshold"

ElementCount=`snmpwalk -c $community -v2c -Onq $host $TableOID${ObjectOID["$ThreshOID"]} | wc -l`
ElementID=""

for (( i=1; i<$ElementCount; i++ )); do
  SNMPGet=`snmpgetnext -c $community -v2c -On $host $TableOID${ObjectOID["$ThreshOID"]}$ElementID`
  if [[ ! $SNMPGet =~ ^$TableOID${ObjectOID["$ThreshOID"]}\..* ]]; then
    break
  fi

  ElementID=`expr match "$SNMPGet" '.*\(\.[0-9]\{8\}\)'`
  if [ `expr match "$SNMPGet" '.*INTEGER: \([0-9]\+\)'` -ne 0 ]; then
  ENTITIES[$i]="$ElementID"
  fi
done

for i in "${!ENTITIES[@]}"; do
  ENTITY=${ENTITIES[$i]}
  RESULT[$i]=`snmpget -c $community -v2c -Onvq $host $TableOID${ObjectOID["$mode"]}$ENTITY`
  Thresh=`snmpget -c $community -v2c -Ovq $host $TableOID${ObjectOID["$ThreshOID"]}$ENTITY`

  if [ "${RESULT[$i]}" -gt 95 ]; then
    ECODE[$i]=2
    STATUS[$i]="CRITICAL $mode - ${RESULT[$i]}%"
  elif [ "${RESULT[$i]}" -ge $Thresh ]; then
    ECODE[$i]=1
    STATUS[$i]="WARNING $mode - ${RESULT[$i]}%"
  elif [ "${RESULT[$i]}" -lt $Thresh ]; then
    ECODE[$i]=0
    STATUS[$i]="OK $mode$i - ${RESULT[$i]}% (Threshold $Thresh%)"
  else
    ECODE[$i]=3
    STATUS[$i]="Unknown $mode$i - ${RESULT[$i]}"
  fi
done

for i in "${!ECODE[@]}"; do
  if [ ${#OUTPUT[@]} -eq 0 ]; then
    OUTPUT["EXIT"]=${ECODE[$i]}
    OUTPUT["STATUS"]=${STATUS[$i]}
    OUTPUT["VALUE"]=${RESULT[$i]}
  fi

  if [ ${OUTPUT["EXIT"]} -ne 3 ]; then
    if [ ${ECODE[$i]} -gt ${OUTPUT["EXIT"]} ]; then
      OUTPUT["EXIT"]=${ECODE[$i]}
      OUTPUT["STATUS"]=${STATUS[$i]}
      OUTPUT["VALUE"]=${RESULT[$i]}
    elif [ ${RESULT[$i]} -gt ${OUTPUT["VALUE"]} ]; then
      OUTPUT["VALUE"]=${RESULT[$i]}
    fi
  fi
done

echo "${OUTPUT["EXIT"]}: ${OUTPUT["STATUS"]}|$mode=${OUTPUT["VALUE"]};90;95;; "
exit "${OUTPUT["EXIT"]}"
