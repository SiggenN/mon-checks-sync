#!/bin/bash

PROGNAME="check_sr7_temp_status"
print_help() {
        echo "$PROGNAME is a OP5 plugin to check temperature status on Alcatel SR7 may work on other models"
        echo ""
        echo "Usage: $PROGNAME [SNMP Community] [HOST] [WARNING TEMP] [CRITICAL TEMP]"
}

declare -a OID=('50331649' '83886081' '83886082' '100663297' '100663298' '134217729' '134217730' '134217731' '134217732' '134217733' '150995041' '150995057' '167772161' '167772162' '167772166' '167772167' '184549633' '184549889' '184549890' '184550145' '184550146' '201328129' '201328130' '201328131')
declare -a TEMP
declare -a SENSORNAME

OIDINDEXMAX=$(expr ${#OID[@]} - 1)

BASETEMPOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.18.1."
BASENAMEOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.8.1."

SNMPv="2c"      #SNMP Version
SNMPc=$1        #SNMP Community
HOST=$2         #HOST
WARN=$3         #Warning temp
CRIT=$4         #Critical temp

for (( i=0; i<$OIDINDEXMAX; i++ )); do
        TEMP[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASETEMPOID${OID[$i]}`
        SENSORNAME[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASENAMEOID${OID[$i]}`

        if [[ "${TEMP[$i]}" =~ ^[0-9]+$ ]] 
        then
        
			if [[ "${TEMP[$i]}" -gt "$CRIT" ]]
			then
				TEMPSTATUS="CRITICAL - "
                EXITCODE=2

			elif [[ "${TEMP[$i]}" -gt "$WARN" ]]  && [[ $EXITCODE -ne 2 ]]
			then
                TEMPSTATUS="WARNING - "
                EXITCODE=1
                
			elif [[ "${TEMP[$i]}" -le "$WARN" ]] && [[ $EXITCODE -eq 0 ]]
			then
                TEMPSTATUS="OK - "
                EXITCODE=0
			fi
        
			TEMPACTIVE="$TEMPACTIVE ${SENSORNAME[$i]}: ${TEMP[$i]} C "
			GRAPHACTIVE="$GRAPHACTIVE ${SENSORNAME[$i]}(Temp)=${TEMP[$i]};$WARN;$CRIT;; "
		fi
done

echo "$TEMPSTATUS$TEMPACTIVE|$GRAPHACTIVE"
exit $EXITCODE
