#!/bin/bash

PROGNAME="UPS Time $0"
VERSION="Version 1.0"
AUTHOR="Author: Niklas Sigg"

print_version() {
  echo ""
  echo "$VERSION"
  echo ""
  echo "$AUTHOR"
}

print_help() {

  print_version $PROGNAME $VERSION
  echo ""
  echo "$PROGNAME is a plugin to check time remaining on supporting OID's"
  echo ""
  echo "Usage: $PROGNAME -H [Host] -v [SNMP version] -C [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv or skip the flag if you're using snmpv1/snmpv2c) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -o [SNMP oid]"
}

while [[ $1 != "" ]]; do

  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -C|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -o|--oid)
    shift
    SNMPo=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

resp=" - Battery time remaining. Rechecking your flags might help. "

if [ "$SNMPl" == "authPriv" ]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST ${SNMPo} 2>&1)
elif [ "$SNMPl" == "authNoPriv" ]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST ${SNMPo} 2>&1)
elif [ "$SNMPl" == "noAuthNoPriv" ]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST ${SNMPo} 2>&1)
else
  OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST ${SNMPo} 2>&1)
fi

if [ $? -ne 0 ]
then
  echo "UNKNOWN${resp}"
  exit 3
fi

(( OUTPUTm=$OUTPUT / 60 )) #get minutes
EXITCODE=0 #OK

echo "$OUTPUTm minutes remaining | 'Minutes Remaining'=$OUTPUTm;60;40 'Time Remaining (Seconds)'=$OUTPUT;3600;2400" #additional output for grafana
exit $EXITCODE
