#!/bin/bash

PROGNAME="$0"
VERSION="Version 1.0"
AUTHOR="Author: Niklas Sigg"

print_version() {
  echo ""
  echo "$PROGNAME - $VERSION"
  echo "$AUTHOR"
}

print_help() {
  echo ""
  echo "$PROGNAME Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -W [Warning] -C [Critical]"
  echo ""
}

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privmode)
    shift
    SNMPx=$1;;
    -X|--privpass)
    shift
    SNMPprivpass=$1;;
    -o|--oid)
    shift
    SNMPo=$1;;
    -W|--warning)
    shift
    WARN=$1;;
    -C|--critical)
    shift
    CRIT=$1;;
    -help|-h)
    print_help
    exit
  esac
  shift
done

if [[ "$SNMPl" == "authPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $SNMPo 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SNMPo 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SNMPo 2>&1)
else
  OUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1)
fi

if [[ "$OUTPUT" -gt "$CRIT" ]]; then
  STATUS="CRITICAL - $OUTPUT C"
  EXITCODE=2
elif [[ "$OUTPUT" -gt "$WARN" && "$OUTPUT" -lt "$CRIT" ]]; then
  STATUS="WARNING - $OUTPUT C"
  EXITCODE=1
elif [[ "$OUTPUT" -lt "$WARN" ]]; then
  STATUS="OK - $OUTPUT C"
  EXITCODE=0
fi

echo "$STATUS | 'Temperature'=$OUTPUT;$WARN;$CRIT"
exit $EXITCODE
