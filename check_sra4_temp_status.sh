#!/bin/bash

PROGNAME="check_sra4_temp_status"
print_help() {
echo "$PROGNAME is a OP5 plugin to check temperature status on Alcatel SRa4 may work on other models"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST] [WARNiNG TEMP] [CRITICAL TEMP]"
}

declare -a OID=('134217729' '150994977' '150994993' '184549633' '184549634')
declare -a TEMP
declare -a SENSORNAME

BASETEMPOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.18.1."
BASENAMEOID=".1.3.6.1.4.1.6527.3.1.2.2.1.8.1.8.1."
EXITCODE=0

SNMPv="2c"      #SNMP Version
SNMPc=$1        #SNMP Community
HOST=$2         #HOST
WARN=$3         #Warning temp
CRIT=$4         #Critical temp

for i in 0 1 2 3 4; do

TEMP[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASETEMPOID${OID[$i]}`
SENSORNAME[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $BASENAMEOID${OID[$i]}`

if [ "${TEMP[$i]}" != "-1" ]
then
        if [ "${TEMP[$i]}" -gt "$CRIT" ]
        then
                TEMPSTATUS="CRITICAL - "
                EXITCODE=2

        elif [ "${TEMP[$i]}" -gt "$WARN" ]  && [ $EXITCODE -ne 2 ]
        then
                TEMPSTATUS="WARNING - "
                EXITCODE=1
        elif [ "${TEMP[$i]}" -le "$WARN" ] && [ $EXITCODE -eq 0 ]
        then
                TEMPSTATUS="OK - "
                EXITCODE=0
        fi
        TEMPACTIVE="$TEMPACTIVE ${SENSORNAME[$i]}: ${TEMP[$i]} C "
        GRAPHACTIVE="$GRAPHACTIVE${SENSORNAME[$i]}(Temp)=${TEMP[$i]};$WARN;$CRIT;; "
fi
done
echo "$TEMPSTATUS$TEMPACTIVE|$GRAPHACTIVE"
exit $EXITCODE
