#!/bin/bash

PROGNAME="check_7750_psu"
print_help()    {
        echo "$PROGNAME is a Icinga/OP5 plugin to check the status of PSUs on Nokia/Alcatel 7750 rewritten by Farhan."
        echo ""
        echo "Usage: $PROGNAME [HOST] [SNMP Community] [Second PSU slot#] [TiMOS version#]"
}

declare -a CHECK

exitCode=0
  
HOST=$1         #HOST
SNMPv="2c"      #SNMP Version
SNMPc=$2        #SNMP Community
SLOTS=("1" "$3") #Second Psu Slot

  baseOID=".1.3.6.1.4.1.6527.3.1.2.2.1.24.2.1.6.3.1."  #integer = 3 means that power is on, integer = 4 means that power is off
  
for i in 0 1; do
  CHECK[$i]=`snmpget -v $SNMPv -c $SNMPc -Oqv $HOST $baseOID${SLOTS[$i]}`
done

if	[ "${CHECK[0]}" == 2 ] || [ "${CHECK[1]}" == 2 ]
  then
    Status="Check if it is the correct SLOT"
    exitcode=1
  
elif	[ "${CHECK[0]}" != "-1" ] && [ "${CHECK[1]}" != "-1" ]
  then
  if	[ "${CHECK[0]}" == 3 ] && [ "${CHECK[1]}" == 3 ]
  then
    Status="OK - power supplies working"
    exitcode=0
    
  elif	[ "${CHECK[0]}" == 4 ] 
  then
    Status="PSU 1 is broken and is running on UPS"
    exitcode=2
  elif [ "${CHECK[1]}" == 4 ] 
  then
    Status="PSU 2 is broken and is running on main power"
    exitcode=2
  fi
else
  Status="Unknown error!"
  exitcode=3
fi 

echo "$Status"
exit $exitcode