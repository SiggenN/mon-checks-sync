#!/usr/bin/env python3
import sys
import os
import requests
import argparse
import re
import urllib3

from requests.auth import HTTPBasicAuth
from datetime import datetime, timedelta

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def getargs():
   parser = argparse.ArgumentParser()
   parser.add_argument('url',
                        help='url of nutanix cluster')
   parser.add_argument('-p', '--password', required=True, action='store',
                        help='password')
   parser.add_argument('-u', '--user', required=True, action='store',
                        help='User name to use when connecting to host')
   args = parser.parse_args()
   return args

def lasto(usecs):
   date = datetime.fromtimestamp(usecs * 0.000001)
   return date.strftime('%Y-%m-%H:%M:%S')

def setExit(severity, status):
    if status == 0:
        return severity
    elif severity == 1 and status != 2:
        return severity
    elif severity == 2 and status != 0:
        return severity
    else:
        return 3

def severity(string):
    global ecode
    s = re.search("^k(\w{1,})", string)
    s = s.group(1)

    if s == "Warning":
        ecode = setExit(1, ecode)
    elif s == "Critical":
        ecode = setExit(2, ecode)
    else:
        ecode = setExit(3, ecode)

    return s

def main():
    global ecode
    ecode = 0
    args = getargs()
    params = {'resolved': False, 'acknowledged': False}

    userauth = HTTPBasicAuth(args.user,args.password)
    headers = {'Accept': 'application/json'}
    req = requests.get(args.url,
                       verify=False,
                       auth=userauth,
                       headers=headers,
                       params=params)
    alerts = req.json()

    #loop through result array
    for alert in alerts['entities']:
        print("{0} - Last Occurrence {1} - Severity: {2}.\n" .format(
            alert['alert_title'],
            lasto(alert['last_occurrence_time_stamp_in_usecs']),
            severity(alert['severity']))
        )

    #exit with exit value
    sys.exit(ecode)

#typical Python construct to execute main function if file executed
if __name__ == '__main__':
    main()