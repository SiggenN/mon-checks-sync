#!/bin/sh
#
# 20071220 Hakan Jerning, Nagios plugin to check that SNMP is resonding
#

PROGPATH=`dirname $0`
. ${PROGPATH:=.}/utils.sh

usage()
{
	echo "USAGE: $0 -A <IP> -c <Community> -v <SNMP version>"
	echo
	echo "EXAMPLE: $0 -A 10.121.100.0 -c reliability -v 2c"
	exit $STATE_CRITICAL
}

test $# -eq 0 && usage

while test -n "$1"; do
	test -n "$2" || usage
	case "$1" in
		-A)
			IP=$2
			shift
			;;
		-c)
			COMMUNITY=$2
			shift
			;;

		-v)
			VERSION=$2
			shift
			;;
		*)
			usage
			;;
	esac
	shift
done


RESPONSE=$(snmpbulkwalk -r 4 -t 30 -c $COMMUNITY -v $VERSION $IP SNMPv2-MIB::sysDescr.0 2>&1; echo $?)
SYSDESCR=$(printf "$RESPONSE" | awk '{ if (NR==1) printf "%s %s %s %s", $4, $5, $6, $7 }')
EXITCODE=$(printf "$RESPONSE" | tail -1)

if [ $EXITCODE -ne 0 ]
then
	echo "SNMP CRITICAL - SNMP is not responding"
	exit $STATE_CRITICAL
else
	echo "SNMP OK - $SYSDESCR"
	exit $STATE_OK
fi

echo "WTF?!"
exit $STATE_UNKNOWN
