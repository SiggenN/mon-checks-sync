#!/bin/bash

# author    Niklas Sigg | NetNordic Sweden AB
# date      2020-08-31

print_help() {
    echo ""
    echo "This is a script for Icinga2, used for monitoring Power Supply status and voltage on iDRAC9. More specifically Poweredge R640 which utilizes 2 CPU's."
    echo "$PROGNAME Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass]"
    echo ""
}

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

namearray="1.3.6.1.4.1.674.10892.5.4.600.12.1.8"
psstatus="1.3.6.1.4.1.674.10892.5.4.600.12.1.5"
psvoltage=".1.3.6.1.4.1.674.10892.5.4.600.20.1.6"

IFS=$'\n'
if [[ "$SNMPl" == "authPriv" ]]; then
  namearray=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $namearray 2>&1))
  psstatus=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $psstatus 2>&1))
  psvoltage=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $psvoltage 2>&1))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  namearray=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $namearray 2>&1))
  psstatus=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $psstatus 2>&1))
  psvoltage=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $psvoltage 2>&1))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  namearray=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $namearray 2>&1))
  psstatus=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $psstatus 2>&1))
  psvoltage=($(snmpwalk -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $psvoltage 2>&1))
else
  namearray=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $namearray))
  psstatus=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $psstatus))
  psvoltage=($(snmpwalk -Oqv -v $SNMPv -c $SNMPc $HOST $psvoltage))
fi

psstatus_function () {
    for (( i = 0; i < ${#namearray[@]}; i++ )); do
        if [[ "${psstatus[i]}" -eq "1" ]]; then
            exitcode=3
        fi
        if [[ "${psstatus[i]}" -eq "2" ]]; then
            exitcode=3
        fi
        if [[ "${psstatus[i]}" -eq "3" ]]; then
            exitcode=0
        fi
        if [[ "${psstatus[i]}" -eq "4" ]]; then
            exitcode=1
        fi
        if [[ "${psstatus[i]}" -eq "5" ]]; then
            exitcode=2
        fi
        if [[ "${psstatus[i]}" -eq "6" ]]; then
            exitcode=2
        fi
        if [[ "$((${psvoltage[i]}/1000))" -lt "210" || "$((${psvoltage[i]}/1000))" -gt "250" ]]; then
            exitcode=2
        fi
    done
}

if [[ "${psstatus[i]}" -eq "3" && "$((${psvoltage[i]}/1000))" -gt "210" && "$((${psvoltage[i]}/1000))" -lt "250" ]]; then
    echo "OK - Power Supply status and voltage is currently within set thresholds."
    exitcode=0
elif [[ "${psstatus[i]}" -ne "3" ]]; then
    echo "Status of one or more PSU's has surpassed set tresholds!"
    psstatus_function
elif [[ "$((${psvoltage[i]}/1000))" -lt "210" && "$((${psvoltage[i]}/1000))" -gt "250" ]]; then
    echo "Voltage of one or more PSU's has surpassed set tresholds! Thresholds are set to: <210 or >250."
    psstatus_function
fi

for (( i = 0; i < ${#namearray[@]}; i++ )); do
    printf "%s: " "${namearray[i]}"
    if [[ "${psstatus[i]}" -eq "1" ]]; then
        echo "Current status of the power supply is mapped as 'Other'. Current Voltage at $((${psvoltage[i]}/1000)) V."
    elif [[ "${psstatus[i]}" -eq "2" ]]; then
        echo "Current status of the power supply is mapped as 'Unknown'. Current Voltage at $((${psvoltage[i]}/1000)) V."
    elif [[ "${psstatus[i]}" -eq "3" ]]; then
        echo "Current status of the power supply is mapped as 'OK'. Current Voltage at $((${psvoltage[i]}/1000)) V."
    elif [[ "${psstatus[i]}" -eq "4" ]]; then
        echo "Current status of the power supply is mapped as 'nonCritical'. Current Voltage at $((${psvoltage[i]}/1000)) V."
    elif [[ "${psstatus[i]}" -eq "5" ]]; then
        echo "Current status of the power supply is mapped as 'Critical'. Current Voltage at $((${psvoltage[i]}/1000)) V."
    elif [[ "${psstatus[i]}" -eq "6" ]]; then
        echo "Current status of the power supply is mapped as 'nonRecoverable'. Current Voltage at $((${psvoltage[i]}/1000)) V."
    fi
done

unset IFS

exit $exitcode