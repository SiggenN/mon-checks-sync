#!/bin/bash

PROGNAME="check_ms4k_uptime"

print_help() {

echo "$PROGNAME is a nagios plugin to check system uptime  on PacketFront MS4k may also work on other models"
echo ""
echo "Usage: $PROGNAME [SNMP Community] [HOST] [WARNING (Min)] [OID] (SNMPif)"
}

SNMPv="2c"                                      #SNMP Version
SNMPc=$1                                        #SNMP Community
HOST=$2                                         #Host
WARN="$3"                                       #Warning Time
OID=$4                                          #OID
HWADDR_OID=".1.3.6.1.2.1.2.2.1.6.1"
ST_OK=0                                         #OK
ST_WR=1                                         #Warning
ST_CR=2                                         #Critical
ST_UK=3                                         #Unknown

get_system_uptime() {

uptime=$(snmpget -v $SNMPv -c $SNMPc $HOST $OID 2>&1)
mac=$(snmpget -v $SNMPv -c $SNMPc $HOST $HWADDR_OID 2>&1 | cut -d":" -f4-10 | sed 's/0:8:ae:/00:08:ae:/g')

if [[ $? -ne 0 ]]  || [[  $uptime == 'No Such Object'* ]] ||  [[  $uptime == 'Timeout'* ]]
then
   echo "UNKNOWN - Failed to get switch status"
   exit $ST_UK
fi
uptime_timetick=$(echo $uptime  | cut -d"(" -f2 | cut -d")" -f1)
uptime_seconds=$(($uptime_timetick/100))
system_uptime=$(printf '%d days, %02d:%02d:%02d\n' $(($uptime_seconds/86400)) $(($uptime_seconds%86400/3600)) $(($uptime_seconds%3600/60)) $(($uptime_seconds%60)) )
system_uptime_text="System uptime: $system_uptime MAC:$mac"


if [ "$uptime_timetick" -lt $(($WARN*6000)) ]
then
        EXIT_CODE=$ST_WR
        EXIT_STATUS="WARNING - "
elif [ "$uptime_timetick" -ge $(($WARN*6000)) ]
then
        EXIT_CODE=$ST_OK
        EXIT_STATUS="OK - "
else
        EXIT_CODE=$ST_UK
        EXIT_STATUS="UNKOWN - "
fi
}


MS4k_uplink_uptime() {

local IF_OID=$1
local IFID=$2

IF_name_OID=".1.3.6.1.2.1.2.2.1.2."
IF_status_OID=".1.3.6.1.2.1.2.2.1.8."

IF_name=$(snmpget -v $SNMPv -c $SNMPc $HOST $IF_name_OID$IFID | cut -d" " -f4| sed 's/gigabitethernet/GigE/g' )
IF_status=$(snmpget -v $SNMPv -c $SNMPc $HOST $IF_status_OID$IFID | cut -d" " -f4 | cut -d"(" -f1)

uplink_timetick=$(snmpget -v $SNMPv -c $SNMPc $HOST $IF_OID | cut -d"(" -f2 | cut -d")" -f1)
uplink_timetick=$(($uptime_timetick-$uplink_timetick))
uplink_seconds=$(($uplink_timetick/100))

system_uptime=$(printf '%d days, %02d:%02d:%02d\n' $(($uplink_seconds/86400)) $(($uplink_seconds%86400/3600)) $(($uplink_seconds%3600/60)) $(($uplink_seconds%60)) )
system_uptime_text="$system_uptime_text - $IF_name($IF_status): $system_uptime"


if [ "$uplink_timetick" -lt $(($WARN*6000)) ] && [ $EXIT_CODE -eq 0 ]
then
        EXIT_CODE=$ST_WR
        EXIT_STATUS="WARNING - "
fi
}

if [ $# -lt 4 ]
then
        print_help
        exit $ST_WR
fi

get_system_uptime
if [[ $# -gt 4  && ( "$OID" == ".1.3.6.1.2.1.25.1.1.0" || "$OID" == ".1.3.6.1.2.1.17.2.3.0" ) ]]
then

  for i in $(echo "$5" | sed "s/,/ /g")
  do
        IF_OID=".1.3.6.1.2.1.2.2.1.9.$i"
        MS4k_uplink_uptime "$IF_OID" $i
  done
fi

echo "$EXIT_STATUS$system_uptime_text"
exit $EXIT_CODE
