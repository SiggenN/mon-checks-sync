#!/bin/bash

#Written by Niklas Sigg

author="Author: Niklas Sigg"
progname="$0"

print_help() {
  echo ""
  echo "$author"
  echo "Usage: -t [Choose checktype- 'cpu' or 'memory'] -H [Host] -v [SNMP version] -C [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -T [Critical Threshold]"
}

#Flags
while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -C|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -t|--type)
    shift
    type=$1;;
    -T|--threshold)
    shift
    THRESH=$1;;
    -o|--oid)
    shift
    SNMPo=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

#Setting checktype flag as case insensitive
typenocase=${type,,}

#Memory usage function
memuse() {
  if [[ "$SNMPl" == "authPriv" ]]; then
    memoutput=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $SNMPo 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    memoutput=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SNMPo 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    memoutput=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SNMPo 2>&1)
  else
    memoutput=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1)
  fi

if [[ "$memoutput" -ge "$THRESH" ]]; then
  i=300
  while [[ $i -gt 0 ]]; do
    ((i--))
    echo -ne "Memory usage passed Critical, allowing leeway for spikes, running again in $i seconds."
    sleep 1
    tput cuu1 && tput el
    if [[ $i -eq 150 ]]; then #hidden check if value is still higher than threshold
        if [[ "$SNMPl" == "authPriv" ]]; then
          memoutput2=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $SNMPo 2>&1)
        elif [[ "$SNMPl" == "authNoPriv" ]]; then
          memoutput2=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SNMPo 2>&1)
        elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
          memoutput2=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SNMPo 2>&1)
        else
          memoutput2=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1)
        fi
        #Quit leeway-loop if value is less than threshold
        if [[ "$memoutput2" -lt "$THRESH" ]]; then
          break
        elif [[ "$memoutput2" -ge "$THRESH" ]]; then
          continue
        fi
    fi
  done
fi

  if [[ "$SNMPl" == "authPriv" ]]; then
    memoutput3=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $SNMPo 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    memoutput3=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SNMPo 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    memoutput3=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SNMPo 2>&1)
  else
    memoutput3=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1)
  fi

  if [[ "$memoutput3" -ge "$THRESH" ]]; then
    status="Critical - Used memory $memoutput3%."
    exitcode=2
  elif [[ "$memoutput3" -lt "$THRESH" ]]; then
    status="OK - Used memory $memoutput3%."
    exitcode=0
  fi

echo "$status"
exit $exitcode
}

#CPU usage function
cpuuse() {
  if [[ "$SNMPl" == "authPriv" ]]; then
    cpuoutput=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $SNMPo 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    cpuoutput=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SNMPo 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    cpuoutput=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SNMPo 2>&1)
  else
    cpuoutput=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1)
  fi

if [[ "$cpuoutput" -ge "$THRESH" ]]; then
  i=300
  while [[ $i -gt 0 ]]; do
    ((i--))
    echo -e "CPU usage passed Critical, allowing leeway for spikes, running again in $i seconds."
    sleep 1
    tput cuu1 && tput el
    if [[ $i -eq 150 ]]; then #hidden check if value is still higher than threshold
        if [[ "$SNMPl" == "authPriv" ]]; then
          cpuoutput2=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $SNMPo 2>&1)
        elif [[ "$SNMPl" == "authNoPriv" ]]; then
          cpuoutput2=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SNMPo 2>&1)
        elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
          cpuoutput2=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SNMPo 2>&1)
        else
          cpuoutput2=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1)
        fi
        #Quit leeway-loop if value is less than threshold
        if [[ "$cpuoutput2" -lt "$THRESH" ]]; then
          break
        elif [[ "$cpuoutput2" -ge "$THRESH" ]]; then
          continue
        fi
    fi
  done
fi

  if [[ "$SNMPl" == "authPriv" ]]; then
    cpuoutput3=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $SNMPo 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    cpuoutput3=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $SNMPo 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    cpuoutput3=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $SNMPo 2>&1)
  else
    cpuoutput3=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1)
  fi

  if [[ "$cpuoutput3" -ge "$THRESH" ]]; then
    status="Critical - CPU at $cpuoutput3%."
    exitcode=2
  elif [[ "$cpuoutput3" -lt "$THRESH" ]]; then
    status="OK - CPU at $cpuoutput3%."
    exitcode=0
  fi

echo "$status"
exit $exitcode
}

if [[ "$typenocase" == "memory" ]]; then
  memuse
elif [[ "$typenocase" == "cpu" ]]; then
  cpuuse
else
  echo "Unkown checktype flag. Functions could not be run. Please recheck the flags."
  exit 3
fi

if [[ $? -ne 0 ]]; then
  echo "Unkown flags. Please recheck flags and try again."
  exit 3
fi
