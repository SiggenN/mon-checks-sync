#! /bin/env python
# Author       : Stijn Gruwier <stijn.gruwier@notforadsgmail.com>
# Description  : Gets FSC ServerView (hardware) alerts through SNMP 
# $Id: check_serverview.py,v 1.8 2008/04/10 08:06:30 sg Exp $
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from schau_utils import NagiosPlugin
from schau_snmp import SnmpClient, SnmpError

###################################################
###   Definitions for building a NagiosPlugin   ###
###################################################


opties = {
    'host' : {'char': 'H', 'type':'string'},
    'protocol' : {'char': 'p', 'type':'int', 'default':1},
    'community' : {'char': 'C', 'type':'string', 'default':'public'},
    'ignore' : {'char': 'i', 'type':'string', 'default':''},
    'type' : {'char': 'T', 'type':'string', 'default':'temp'},
    }

help = {
# filename of the plugin
'filename':
'''check_serverview.py''',

# version
'version':
'''$Revision: 1.8 $''',

# disclaimer, license, legal mumbo jumbo
'disclaimer' : 
'''This nagios plugin comes with ABSOLUTELY NO WARRANTY.  You may redistribute
copies of the plugins under the terms of the GNU General Public License.
Copyright (c) 2007 Gruwier Stijn''',

# Summary
'preamble' :
'''This plugin checks all fsc serverview (hardware) systemHealth, returns global status''',

# Commandline usage
'use' :
'''Usage:	check_serverview.py -H host [-C community] [-p protocol]
		[-i|--ignore=subsystem1[,subsystem2[,...]]] [-T|--Type=[fan,temp,voltages]]
	check_serverview.py (-h|--help)
	check_serverview.py (-V|--version)''', 

# Description of every option
'options' :
'''check_serverview.py
 -H, --host=hostname
    Connect to hostname
 -p, --protocol=[1|2|3]
    Snmp version to use (default 1)
    Affects the community option
 -C, --community=COMMUNITY
    community string with SNMPv1/v2 OR <user>:<password> with SNMPv3
    Default: public
 -i, --ignore=SUBSYSTEM1,SUBSYSTEM2,SUBSYSTEM3
    list of subsystem names to ignore
    comma separated(do not use spaces!)
    Added this feature because I don't care about the deployment subsystem
-T, --Type=[fan,temp,voltages]
    select the type of check to run.
	voltages not implemented yet.
 -w, --warning=WARNINGLEVEL
    Exit with WARNING status if a variable matches WARNINGLEVEL
    not implemented
 -c, --critical=CRITICALLEVEL
    Exit with CRITICAL status if a variable matches CRITICAL
    not implemented
 -t, --timeout=SECONDS
    seconds before plugin times out (default: 25)
    not implemented
 -v, --verbose=[0-]
    Set the amount of output
    not implemented
 -h, --help
    Print help 
 -V, --version
    Print version information''',

# Known deviceInfos
'bugs' :
'''Bugs:	Let me know - stijn.gruwier@gmailnotforads.com
        *remove the 'notforads' substring from the address*''' }

def serverview_function(options):
    # TODO: more option checks
    ignorelist = []
    host = options['host']
    community = options['community']
    protocol = int(options['protocol'])
    timeout = options['timeout']
    ignore = options['ignore']
    checkType = options['type']
    if not host:
        return('UNKNOWN', '-H, --host is a required argument')
    if not community:
        return('UNKNOWN', '-C, --community is a required argument')
    if not protocol in (1,2,3):
        return('UNKNOWN', 'invalid protocol')
    if ignore:
        ignorelist = ignore.lower().split(',')
    if protocol in (1,2):
        snmp = SnmpClient(host, protocol, community)
    else:
        user, key = community.split(':')
        snmp = SnmpClient(host, protocol, community=community)
    
    if checkType == 'fan':
        fanInfoList = []
        deviceInfoString, systemHealth = '', ''
        try:
            fanInfoList = get_fan_info(snmp) 
        except SnmpError:
            return('CRITICAL', 'network or snmp related problem - NOT a hardware problem')
        for deviceInfo in fanInfoList:
            name, status, current, quality = deviceInfo
            deviceInfoString = deviceInfoString + '\nName: %s Status: %s  Current Speed: %s rpm' % (name, status, current)
        return('OK', 'Environment Status: %s' % deviceInfoString)
    elif checkType == 'temp':
        tempInfoList = []
        deviceInfoString, systemHealth = '', ''
        try:
            tempInfoList = get_temp_info(snmp) 
        except SnmpError:
            return('CRITICAL', 'network or snmp related problem - NOT a hardware problem')
        for deviceInfo in tempInfoList:
            name, status, current, warn, crit = deviceInfo
            deviceInfoString = deviceInfoString + '\nName: %s Status: %s  Current Temp: %s Celsius' % (name, status, current)
        return('OK', 'Environment Status: %s' % deviceInfoString)
    # elif checkType == 'voltages':
        # tempInfoList = []
        # deviceInfoString, systemHealth = '', ''
        # try:
            # tempInfoList = get_temp_info(snmp) 
        # except SnmpError:
            # return('CRITICAL', 'network or snmp related problem - NOT a hardware problem')
        # for deviceInfo in tempInfoList:
            # name, status, current, warn, crit = deviceInfo
            # deviceInfoString = deviceInfoString + '\nName: %s Status: %s  Current Voltage: %s V' % (name, status, current)
        # return('OK', 'Environment Status: %s' % deviceInfoString)

        

def get_temp_info(snmp):
    '''Returns a string with subsystem names, and a list of failed ones '''
    sc2tempSensorIdentifier = '.1.3.6.1.4.1.231.2.10.2.2.10.5.1.1.4.1.%s'
    sc2tempSensorStatus = '.1.3.6.1.4.1.231.2.10.2.2.10.5.1.1.5.1.%s'
    sc2tempCurrent = '.1.3.6.1.4.1.231.2.10.2.2.10.5.1.1.6.1.%s'
    sc2tempWarning = '.1.3.6.1.4.1.231.2.10.2.2.10.5.1.1.7.1.%s'
    sc2tempCrit = '.1.3.6.1.4.1.231.2.10.2.2.10.5.1.1.8.1.%s'
    
	
    STATUS_NR2STRING = {1:'Unknown',2:'Not-Available',3:'Ok',4:'Sensor Failure',5:'Failure',6:'Temperature Warning! Too hot!',7:'Temperature Critical! Too hot!',8:'OK, Temperature Normal',9:'Temperature Warning!'}
    tempInfoList = []
    tempSensorStatus, tempSensorIdentifier, tempCurrent, tempWarn, tempCrit = 0,'',0,0,0

    for index in range (1, 12):
		index_str = str(index)
		tempSensorStatus = int(snmp.get(sc2tempSensorStatus % index_str))
		if tempSensorStatus == 2:
			continue
		try:
			tempSensorStatus = STATUS_NR2STRING[tempSensorStatus]
		except:
			tempSensorStatus = 'outofrange'
		tempSensorIdentifier = str(snmp.get(sc2tempSensorIdentifier % index_str))
		tempCurrent = str(snmp.get(sc2tempCurrent % index_str))
		tempWarn = str(snmp.get(sc2tempWarning % index_str))
		tempCrit = str(snmp.get(sc2tempCrit % index_str))
		tempInfoList.append((tempSensorIdentifier,tempSensorStatus,tempCurrent,tempWarn,tempCrit))
    return tempInfoList
	
def get_fan_info(snmp):
    '''Returns a string with subsystem names, and a list of failed ones '''
    sc2fanDesignation = '.1.3.6.1.4.1.231.2.10.2.2.10.5.2.1.3.1.%s'
    sc2fanStatus = '.1.3.6.1.4.1.231.2.10.2.2.10.5.2.1.5.1.%s'
    sc2fanCurrentSpeed = '.1.3.6.1.4.1.231.2.10.2.2.10.5.2.1.6.1.%s'
    sc2fanQuality = '.1.3.6.1.4.1.231.2.10.2.2.10.5.2.1.7.1.%s'
    
	
    STATUS_NR2STRING = {1:'Unknown',2:'Disabled',3:'Ok',4:'Failed',5:'Pre-Failure predicted',6:'Redundant fan failed',7:'Not manageable',8:'Not present'}
    fanInfoList = []
    fanStatus, fanDesignation, fanCurrentSpeed, fanQuality = 0,'',0,0

    for index in range (1, 8):
		index_str = str(index)
		fanStatus = int(snmp.get(sc2fanStatus % index_str))
		# if fanStatus == 2:
			# continue
		try:
			fanStatus = STATUS_NR2STRING[fanStatus]
		except:
			fanStatus = 'outofrange'
		fanSensorIdentifier = str(snmp.get(sc2fanDesignation % index_str))
		fanCurrent = str(snmp.get(sc2fanCurrentSpeed % index_str))
		fanQuality = str(snmp.get(sc2fanQuality % index_str))
		fanInfoList.append((fanSensorIdentifier,fanStatus,fanCurrent,fanQuality))
    return fanInfoList

	

if __name__ == '__main__':
    plug = NagiosPlugin('SERVERVIEW', serverview_function, opties, help)
    plug.run(debug=True)


