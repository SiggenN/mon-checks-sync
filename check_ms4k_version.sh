#!/bin/bash

PROGNAME="check_ms4k_version"

print_help() {
    echo "$PROGNAME is a OP5 plugin to check iBOS version  on PF MS4k may not work on other models"
    echo ""
    echo "Usage: $PROGNAME [SNMP Community] [HOST]"
}

SNMPv="2c"                                      #SNMP Version
SNMPc=$1                                        #SNMP Community
HOST=$2                                         #WARNING Version
OID=".1.3.6.1.2.1.1.1.0"
#Exit status for service
ST_OK=0                                         #Speed OK
ST_WR=1                                         #Speed Warning
ST_UK=3                                         #Speed Unknown

SNMPRES=$(snmpget -v $SNMPv -c $SNMPc $HOST $OID 2>&1)
MODEL=$(echo $SNMPRES | cut -d" " -f4 | cut -d"," -f1)
FULLVERSION=$(echo $SNMPRES | cut -d" " -f7)
SHORTVERSION=$(echo $FULLVERSION | cut -d"-" -f3)


if [[ "$SNMPRES" =~ "PacketFront" ]] || [[ "$SNMPRES" =~ "Waystream" ]]
then
    echo "OK - Current iBOS version is $MODEL $FULLVERSION"
    exit $ST_OK
else
    echo "UNKNOWN - Unable to find switch version"
    exit $ST_UK
fi