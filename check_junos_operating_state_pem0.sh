#!/bin/bash

PROGNAME=`basename $0`
AUTHOR="Sami Ereq 2016 Netnordic AB"
VERSION="Version 1.0"

print_version() {
echo "$VERSION $AUTHOR"
}

print_help() {
print_version $PROGNAME $VERSION
echo ""
echo "$PROGNAME is a OP5 plugin to check PSU och FAN state for Junos elements"
echo ""
echo "Usage: $PROGNAME [SNMP community] [OID] [Host]"
echo ""
}

#Will be $ARGX$ input from OP5 X=ARG number
SNMPv="2c"
SNMPc=$1
OID=$2
HOST=$3

#Exit status for service
OK=0
WR=1
CR=2
UK=3

while test -n "$1"; do
    case "$1" in
        -help|-h)
        print_help
        exit $UK;;
    esac
shift
done

GETPSU=$(snmpget -v $SNMPv -c $SNMPc $HOST $OID -O vq)

if [ "$GETPSU" -eq "2" ]
        then
                echo "2 = Up and running"
                exit $OK
        elif [ "$GETPSU" -eq "1" ]
        then
                echo "1 = Unknown"
                exit $WR
        elif [ "$GETPSU" -eq "3" ]
        then
                echo "3 = Ready to run, not running yet"
                exit $WR
        elif [ "$GETPSU" -eq "4" ]
        then
                echo "4 = Held in reset, not ready yet"
                exit $WR
        elif [ "$GETPSU" -eq "5" ]
        then
                echo "5 = Running at Full Speed (valid for fans only)"
                exit $WR
        elif [ "$GETPSU" -eq "6" ]
        then
                echo "6 = Down or off (for power supply)"
                exit $CR
        elif [ "$GETPSU" -eq "7" ]
        then
                echo "7 = Running as a standby (Backup)"
                exit $OK
        else
                echo "Something is wrong"
                exit $UK
fi

