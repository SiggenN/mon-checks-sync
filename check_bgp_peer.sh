#!/bin/bash

# author    Sebastian Smith Hellstrand
# date      2020-08-19

print_help() {
  echo ""
  echo "$PROGNAME Usage: -H [Host] -v [SNMP version] -c [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -p [Target peer ip]"
  echo ""
}

while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -p|--targetpeerip)
    shift
    targetpeerip=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

if [ $? -ne 0 ]
then
  echo "Failed. Rechecking your flags might help."
  print_help
  exit 3
fi

bgppeerstatusoid="1.3.6.1.2.1.15.3.1.2."


if [[ "$SNMPl" == "authPriv" ]]; then
  output=($(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivpass $HOST $bgppeerstatusoid$targetpeerip 2>&1))
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  output=($(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $bgppeerstatusoid$targetpeerip 2>&1))
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  output=($(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $bgppeerstatusoid$targetpeerip 2>&1))
else
  output=($(snmpget -Oqv -v $SNMPv -c $SNMPc $HOST $bgppeerstatusoid$targetpeerip))
fi

if [[ "$output" -eq "6" ]]; then
      status="Established = connection is established."
      exitcode=0
else
      status="No established connection."
      exitcode=2
fi

echo "$status"
exit $exitcode