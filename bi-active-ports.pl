#!/usr/bin/perl -w
#use Data::Dumper;
my $ARGVS = ($#ARGV + 1);

$VERSION = "1.0";
$AUTHOR = "2016 Per Linusson NetNordic";

sub print_version {
    print "bi_active_ports.pl version $VERSION, $AUTHOR\n\n";
}

sub print_help {
    print "bi_active_ports.pl checks active ports for each ISP in BBE\n"
}

sub print_usage {
    print "Usage: bi_active_ports.pl <server> <postgres port> <user> <database>]\n",
    "       bi_active_ports.pl -h|--help\n",
    "       bi_active_ports.pl -v|--version\n\n";
}

if ($ARGVS == 1 && ($ARGV[0] eq "-h" || $ARGV[0] eq "--help")) {
    print_version();
    print_help();
    print_usage();
    exit 0;
} 
elsif ($ARGVS == 1 && ($ARGV[0] eq "-v" || $ARGV[0] eq "--version")) {
    print_version();
    exit 0;
} 
elsif ($ARGVS != 4) {
    print_usage();
    exit 0;
}

my $server_ip = $ARGV[0];
my $pg_port = $ARGV[1];
my $db_user = $ARGV[2];
my $db = $ARGV[3];
my @isp_ary;
my $out_str = "";
my $out_count_str = "";
my $isp_str = `psql -h $server_ip -p $pg_port -d $db -U $db_user -t -A -F"," -c "SELECT namespacename,primarykey FROM namespace WHERE primarykey != 1000;"`;

if ($isp_str eq "" || $isp_str =~ /FATAL:|ERROR:/) {
  print "Error retrieving information from BBE database\n";
  exit 3;
  }
else {  
  @isp_ary = split /\n/, $isp_str;
  undef $isp_str;
  }


foreach $row (@isp_ary) {
	my @isp = split /,/, $row;
	my $port_count = `psql -h $server_ip -p $pg_port -d $db -U $db_user -t -A -c "SELECT count(DISTINCT deliveryaddress_id) FROM productsubscription WHERE namespace_primarykey = $isp[1] AND subscriptionstate = 'ACTIVE';"`;
	$port_count=~ tr/\"\r\n//d;
	$out_str=$out_str . "$isp[0] ";
	$out_count_str=$out_count_str . "$isp[0] ports=$port_count;;;; ";
}
my $tot_active_ports=`psql -h $server_ip -p $pg_port -d $db -U $db_user -t -A -c "SELECT count(DISTINCT deliveryaddress_id) FROM productsubscription WHERE namespace_primarykey != 1000 AND subscriptionstate = 'ACTIVE';"`;
$tot_active_ports=~ tr/\"\r\n//d;
my $tot_ports=`psql -h $server_ip -p $pg_port -d $db -U $db_user -t -A -c "SELECT count(*) FROM imtdeliveryaddress JOIN imtport ON imtdeliveryaddress.imtport_id = imtport.id WHERE imtdeliveryaddress.status = 'ACTIVE' AND imtport.status = 'ATTACHED';"`;
$tot_ports=~ tr/\"\r\n//d;
my $inactive_ports=$tot_ports-$tot_active_ports;
$out_str=$out_str . "Total active ports ";
$out_count_str=$out_count_str . "Total active ports=$tot_active_ports;;;; ";
$out_str=$out_str . "Inactive ports";
$out_count_str=$out_count_str . "Inactive ports=$inactive_ports;;;; ";
print $out_str,'|',$out_count_str;
exit 0;
