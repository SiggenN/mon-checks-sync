#!/usr/bin/perl

use strict;
use DBI;
use Getopt::Long;
use Pod::Usage;

my $db = 'bbedb';
my $user = 'app';
my $host;
my $pass = '';
my $server = '127.0.0.1';
my $driver = 'Pg';
my $port = 5432;#standard postgres port
my $man = 0;
my $help = 0;

GetOptions ('host=s' => \$host,
                        'user=s' => \$user,
                        'database=s' => \$db,
                        'pass:s' => \$pass,
                        'server:s' => \$server,
                        'port:s' => \$port,
                        'connector:s' => \$driver,
                        'help|?' => \$help,
                         man => \$man)
|| pod2usage(2);

pod2usage(1) if $help;
pod2usage(-exitval => 0, -verbose => 2) if $man;

if (! ($host =~ /kommun|kapacitet/)) {
        print "hostname missing or not recognized";
        exit 1;
}

$host =~ s/SE-BSN_//; #remove prefix to make icinga conf easier.

my $dsn="DBI:$driver:dbname=$db;host=$server;port=$port";
my $db_hand = DBI->connect($dsn, $user, $pass, { RaiseError => 1 }) or die $DBI::errstr;



my $query = qq(SELECT imtport.fqpn,ct_sla."OrgName",ct_sla."SLA",productsubscription.subscriptionid from imtport
JOIN imtdeliveryaddress
ON imtport.id=imtdeliveryaddress.imtport_id AND imtport.status = 'ATTACHED' AND imtport.fqpn LIKE '\%$host\%'
JOIN productsubscription
ON productsubscription.deliveryaddress_id=imtdeliveryaddress.id AND productsubscription.subscriptionstate='ACTIVE'
JOIN servicesubscription
ON productsubscription.id=servicesubscription.productsubscription_id
LEFT JOIN crosstab(\$\$select servicesubscription_id, paramname, paramvalue from servicesubscriptionparam,productsubscription WHERE paramname = 'SLA'
or paramname = 'OrgName' group by servicesubscription_id,productsubscription.subscriptionid, paramname, paramvalue order by 1,2 asc\$\$, \$\$VALUES ('OrgName'),('SLA')\$\$) AS ct_sla(servicesubscription_id bigint, "OrgName" text, "SLA" text)
ON ct_sla.servicesubscription_id=servicesubscription.id order by imtport.fqpn;);

my $query_hand = $db_hand->prepare( $query );
my $res = $query_hand->execute() or die $DBI::errstr;

if($res < 0){
        die $DBI::errstr;
}

if ($res == 0) {
        print "No customers found on switch\n";
        exit;
}
while(my @row = $query_hand->fetchrow_array()) {
        $row[0] =~ s/\.$host//;
        $row[0] =~ s/gabitethernet|gregator//;
#       print "$row[0] Org: $row[1] SLA: $row[2] id: $row[3]  $row[4]\n";
        printf("%-5.5s %-9.9s %-17.17s %-s\n", "$row[0]","SLA: $row[2]","Id: $row[3]","Org: $row[1]");
}

$db_hand->disconnect();

__END__

=head1 NAME

bsn_sla - Retrieves SLA and organization using a specific switch in BSN. Uses the DBI module for database connection.

=head1 SYNOPSIS

bsn_sla.pl host [options]
Options:
-help            brief help message
-man             full documentation
-user            DB user
-pass            password for DB user
-database        database name
-port            port DB server is listening on
-connector       DBI driver

=head1 OPTIONS

=over 4

=item B<-help>

Print a brief help message and exits.

=item B<-man>

Prints the manual page and exits.

=item B<-user>

DB user, defaults to app.

=item B<-pass>

password for DB user.

=item B<-database>

database name, defaults to bbedb.

=item B<-port>

defaults to 5432 (standard postgres port).

=item B<-connector>

driver for database, defaults to Pg.

=back

=head1 DESCRIPTION

B<bsn_sla> Retrieves SLA and organization from BBE database for all ports on a specific switch in BSN.

=cut