#!/bin/bash

AUTHOR="Author: Niklas Sigg"
VERSION="VERSION 1.0"
PROGNAME="$0"

OID_BATT_TIME=".1.3.6.1.4.1.534.1.2.1.0"
OID_BATT_CAPA=".1.3.6.1.4.1.534.1.2.4.0"
OID_BATT_STATUS=".1.3.6.1.4.1.534.1.2.5.0"

print_version() {
  echo ""
  echo "$VERSION"
  echo ""
  echo "$AUTHOR"
}

print_help() {
  echo ""
  echo "$PROGNAME is a plugin to check status on UPS with xups mibs."
  echo ""
  echo "Usage: $PROGNAME -H [Host] -v [SNMP version] -C [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv or skip the flag if you're using snmpv1/snmpv2c) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass]"
}

#Battery charging function
battcharge() {
  if [[ "$SNMPl" == "authPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_TIME 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_TIME 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_TIME 2>&1)
  else
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_TIME 2>&1)
  fi

  if [[ "$SNMPl" == "authPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_CAPA 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_CAPA 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_CAPA 2>&1)
  else
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_CAPA 2>&1)
  fi

  (( TIMEOUTPUTm=$TIMEOUTPUT / 60 ))

  if [[ "$CHARGEOUTPUT" -ge "100" ]]; then
    STATUS="Battery is fully charged, it will soon enter floating state. Current capacity is at $CHARGEOUTPUT% and current battery time is $TIMEOUTPUTm minutes."
    EXITCODE=0
  elif [[ "$CHARGEOUTPUT" -lt "100" ]]; then
    STATUS="Battery is charging. Current capacity is at $CHARGEOUTPUT% and current battery time is $TIMEOUTPUTm minutes."
    EXITCODE=0
  else
    STATUS="Capacity and time unknown."
    EXITCODE=3
  fi

  echo "$STATUS | 'Minutes Remaining'=$TIMEOUTPUTm;60;45 'Time Remaining (Seconds)'=$TIMEOUTPUT;3600;2700 'Capacity'=$CHARGEOUTPUT%;75;56"
  exit $EXITCODE
}
#Battery discharge function
battdischarge() {
  if [[ "$SNMPl" == "authPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_TIME 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_TIME 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_TIME 2>&1)
  else
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_TIME 2>&1)
  fi

  if [[ "$SNMPl" == "authPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_CAPA 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_CAPA 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_CAPA 2>&1)
  else
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_CAPA 2>&1)
  fi

  (( TIMEOUTPUTm=$TIMEOUTPUT / 60 ))

  if [[ "$CHARGEOUTPUT" -le "100" ]]; then
    STATUS="Battery is discharging. Current capacity is at $CHARGEOUTPUT% and current battery time is $TIMEOUTPUTm minutes."
    EXITCODE=2
  else
    STATUS="Capacity and time unknown."
    EXITCODE=3
  fi

  echo "$STATUS | 'Minutes Remaining'=$TIMEOUTPUTm;60;45 'Time Remaining (Seconds)'=$TIMEOUTPUT;3600;2700 'Capacity'=$CHARGEOUTPUT%;75;56"
  exit $EXITCODE
}
#Battery floating function
battfloating() {
  if [[ "$SNMPl" == "authPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_TIME 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_TIME 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_TIME 2>&1)
  else
    TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_TIME 2>&1)
  fi

  if [[ "$SNMPl" == "authPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_CAPA 2>&1)
  elif [[ "$SNMPl" == "authNoPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_CAPA 2>&1)
  elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_CAPA 2>&1)
  else
    CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_CAPA 2>&1)
  fi

  (( TIMEOUTPUTm=$TIMEOUTPUT / 60 ))

  if [[ "$CHARGEOUTPUT" -ge "0" ]]; then
    STATUS="Battery Floating. The charger is temporarily charging the battery to its float voltage. Current capacity is at $CHARGEOUTPUT% and current battery time is $TIMEOUTPUTm minutes."
    EXITCODE=0
  else
    STATUS="Capacity and time unknown."
    EXITCODE=3
  fi

  echo "$STATUS | 'Minutes Remaining'=$TIMEOUTPUTm;60;45 'Time Remaining (Seconds)'=$TIMEOUTPUT;3600;2700 'Capacity'=$CHARGEOUTPUT%;75;56"
  exit $EXITCODE
}

#Flags
while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -C|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

#Redundant snmpget with status oid
if [[ "$SNMPl" == "authPriv" ]]; then
  STDOUTP=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_STATUS 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  STDOUTP=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_STATUS 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  STDOUTP=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_STATUS 2>&1)
else
  STDOUTP=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_STATUS 2>&1)
fi

if [[ "$SNMPl" == "authPriv" ]]; then
  TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_TIME 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_TIME 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_TIME 2>&1)
else
  TIMEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_TIME 2>&1)
fi

if [[ "$SNMPl" == "authPriv" ]]; then
  CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST $OID_BATT_CAPA 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST $OID_BATT_CAPA 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST $OID_BATT_CAPA 2>&1)
else
  CHARGEOUTPUT=$(snmpget -Ovq -v $SNMPv -c $SNMPc $HOST $OID_BATT_CAPA 2>&1)
fi

if [ $? -ne 0 ]
then
  echo "Unknown Unit status. Rechecking your flags might help."
  exit 3
fi

case $STDOUTP in
  8)
    EXITCODE=1
    STATUS="Check Battery. Battery state is uncertain following a poor battery test result."
    ;;
  7)
    EXITCODE=0
    STATUS="Battery under test"
    ;;
  6)
    EXITCODE=2
    STATUS="Battery Disconnected"
    ;;
  5)
    EXITCODE=3
    STATUS="Battery Status Unknown"
    ;;
  4)
    EXITCODE=0
    STATUS="Battery Resting. The battery is fully charged and none of the other actions(charging/discharging/floating) is being done"
    ;;
  3) battfloating ;;
  2) battdischarge ;;
  1) battcharge ;;
  *)
    EXITCODE=3
    STATUS="Error!"
    ;;
esac

(( TIMEOUTPUTm=$TIMEOUTPUT / 60 ))
echo "$STATUS | 'Minutes Remaining'=$TIMEOUTPUTm;60;45 'Time Remaining (Seconds)'=$TIMEOUTPUT;3600;2700 'Capacity'=$CHARGEOUTPUT%;75;56"
exit $EXITCODE
