#!/bin/bash
PROGNAME="$0"
VERSION="Version 3.0"
AUTHOR="Author: Farhan Islam"
print_version() {
  echo ""
  echo "$PROGNAME - $VERSION"
  echo "$AUTHOR"
}
print_help() {
  echo ""
  echo "$PROGNAME Usage: -H [Host] -v [SNMP version] -c [SNMP community] -o [OID]"
  echo ""
}
while [[ $1 != "" ]]; do
  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -c|--community)
    shift
    SNMPc=$1;;
    -o|--oid)
    shift
    SNMPo=$1;;  
    -help|-h)
    print_help
    exit
  esac
  shift
done

uptime=$(snmpget -v $SNMPv -c $SNMPc $HOST $SNMPo 2>&1) #have to run snmpget to make timeticks work, snmpwalk wont work

if [[ $? -ne 0 ]]  || [[  $uptime == 'No Such Object'* ]] ||  [[  $uptime == 'Timeout'* ]]
then
   echo "UNKNOWN - Failed to get switch status"
   exit 3
fi

uptime_timetick=$(echo $uptime | cut -d' ' -f4 | tr -d '()') #på uptime, tar bort allt förutom rad4 OCH tar bort parantes
uptime_seconds=$(($uptime_timetick/100))

system_uptime=$(printf '%d days, %02d h %02d min %02d sec\n' $(($uptime_seconds/86400)) $(($uptime_seconds%86400/3600)) $(($uptime_seconds%3600/60)) $(($uptime_seconds%60)) )
system_uptime_text="$system_uptime"


if [[ "$uptime_timetick" -gt "0" ]]; then
  STATUS="OK - $uptime_timetick"
  EXITCODE=0
else
  STATUS="UNKNOWN"
  EXITCODE=3  
fi

echo "$system_uptime_text | 'Uptime'=$system_uptime"
exit $EXITCODE