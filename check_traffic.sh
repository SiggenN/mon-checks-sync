#!/bin/bash
#host=localhost 
#community=public
version=2c #we're not really changing this anytime soon.
ispeed="" #bandwidth in Mbps, script is supposed to fetch that, but you never know.
warn_in=70
warn_out=70
crit_in=90
crit_out=90
interface=31
time_out=10
snmp_ifin="1.3.6.1.2.1.31.1.1.1.6" #64-bit counters incoming traffic, TODO (maybe) add option for 32-bit counter
snmp_ifout="1.3.6.1.2.1.31.1.1.1.10" #-as above for outgoing
snmp_if_speed="1.3.6.1.2.1.31.1.1.1.15" #supposedly the OID for if bandwidth, in units of Mbit/s
typeset -i traf_in traf_last_in traf_out traf_last_out traf_diff_in traf_diff_out traf_bps_in traf_bps_out
path_tmpfile="/tmp/traffic_data_"
function inform_user
{
echo "usage: check_traffic_custom [-C community ] [-H host] [-i interface] [-w warning] [-c critical] [-u v3 user] [-p v3 pass]| [-h]"
echo "warning and critical thresholds can be specified as num1:num2 or num1 in % of capacity." 
echo "e.g. ./test.sh -C test -i 25 -H 8.8.8.8 -wi 10:60 -ci 60:100 -wo 10:60 -co 60:100"
}

#options parsing
if [[ $1 = "" ]]; then
inform_user
exit
fi
while [[ $1 != "" ]]; do

  case $1 in 
    -C | --community )
    shift
    community=$1;;
    #echo "snmp community = ${community}";;
    -H | --host )
    shift
    host=$1;;
    -i | --interface )
    shift
    interface=$1;;
    -wi | --warning_in )
    shift
    warn_in=$1;;
    -ci | --critical_in )
    shift
	crit_in=$1;;
	-w | --warning )
    shift
	warn_in=$1
	warn_out=$1;;
	-c | --critical )
    shift
	crit_in=$1
	crit_out=$1;;
    -wo | --warning_out )
    shift
    warn_out=$1;;
    -co | --critical_out )
    shift
    crit_out=$1;;
    -t | --timeout )
    shift
    time_out=$1;;
    -v | --version )
	shift
    version=$1;;
    -s | --speed )
    shift
    ispeed=$1;;
    -u | --user )
    shift
    v3user=$1;;
	-p | --user )
    shift
    v3pass=$1;;
    -h | --help )
    inform_user
    exit;;
  esac
  shift
done

if [[ $version == '2c' ]] && [[ -n $community ]]; then
  snmpstr="snmpget -t $time_out -r 2 -v $version -c $community -Oqv"
elif 
  [[ $version == '3' ]] && [[ -n $v3user ]] && [[ -n $v3pass ]]; then
  snmpstr="snmpget -t $time_out -r 2 -v $version -L authNoPriv -U $v3user -a MD5 -A $v3pass -Oqv"
else
  echo "Missing argument"
  inform_user
  exit
fi

if [[ $ispeed == "" ]]; then
ispeed=`$snmpstr $host $snmp_if_speed.$interface` 
  if [[ $ispeed == 0 ]] || [[ -z $ispeed ]]; then
  echo "no bandwidth given or found for interface"
  exit 3
  fi
fi

#some matching for x:y argument syntax, there's probably a (much) nicer way to do this
if [[ $warn_in == *":"* ]]; then
  high_warn_in=`expr match ${warn_in} '.*:\([[:digit:]]\+\)'`
  low_warn_in=`expr match ${warn_in} '\([[:digit:]]\+\):'`
else
  high_warn_in=$warn_in
  low_warn_in=0
fi
if [[ $crit_in == *":"* ]]; then
  high_crit_in=`expr match ${crit_in} '.*:\([[:digit:]]\+\)'`
  low_crit_in=`expr match ${crit_in} '\([[:digit:]]\+\):'`
else
  high_crit_in=$crit_in
  low_crit_in=0
fi

if [[ $warn_out == *":"* ]]; then
  high_warn_out=`expr match ${warn_out} '.*:\([[:digit:]]\+\)'`
  low_warn_out=`expr match ${warn_out} '\([[:digit:]]\+\):'`
else
  high_warn_out=$warn_out
  low_warn_out=0
fi
if [[ $crit_out == *":"* ]]; then
  high_crit_out=`expr match ${crit_out} '.*:\([[:digit:]]\+\)'`
  low_crit_out=`expr match ${crit_out} '\([[:digit:]]\+\):'`
else
  high_crit_out=$crit_out
  low_crit_out=0
fi

####Main####
#gets octet counters for traffic in and out at two times and computes traffic from the difference. 


traf_in=`$snmpstr $host $snmp_ifin.$interface`
time_stamp=`date +%s%3N`
traf_out=`$snmpstr $host $snmp_ifout.$interface`
if [[ -z $traf_in ]]  || [[ -z $traf_out ]]; then
  echo "UNKNOWN: No values fetched"
  exit 3
fi
if [ -f "${path_tmpfile}${host}_${interface}" ]; then
  mapfile -t args <"${path_tmpfile}${host}_${interface}"
  if [ ${#args[@]} -eq "3" ]; then
    traf_last_in=${args[0]}
    traf_last_out=${args[1]}
    time_stamp_last=${args[2]}
  else
    printf "%d\n%d\n%s" $traf_in $traf_out $time_stamp >"${path_tmpfile}${host}_${interface}"
	echo "OK:IF counters written to file"
	exit 0
  fi
else
  touch "${path_tmpfile}${host}"
  printf "%d\n%d\n%s" $traf_in $traf_out $time_stamp >"${path_tmpfile}${host}_${interface}"
  echo "OK:IF counters written to file" 
  exit 0
fi

if [[ $time_stamp == $time_stamp_last ]]; then
  echo "no difference in time stamps (division by zero)"
  exit 3
fi
time_diff=`bc <<< "scale=3; ($time_stamp-$time_stamp_last)/1000"`

if [[ -z $traf_last_in ]] || [[ -z $traf_last_out ]]; then
  echo "UNKNOWN: No values fetched for last check, writing current values"
  printf "%d\n%d\n%s" $traf_in $traf_out $time_stamp >"${path_tmpfile}${host}_${interface}"
  exit 3
fi

if [ $traf_in -ge $traf_last_in ]; then 
   traf_diff_in=$traf_in-$traf_last_in 

elif [ $traf_in -lt $traf_last_in ]; then
  # counters were reset somewhere in between checks
  echo "Counter value reset, writing current values"
  printf "%d\n%d\n%s" $traf_in $traf_out $time_stamp >"${path_tmpfile}${host}_${interface}"
  exit 0
   
fi

if [ $traf_out -ge $traf_last_out ]; then
   traf_diff_out=$traf_out-$traf_last_out #"$(echo " $traf_out - $traf_last_out" | bc)

elif [ $traf_out -lt $traf_last_out ]; then
  echo "Counter value reset, writing current values"
  printf "%d\n%d\n%s" $traf_in $traf_out $time_stamp >"${path_tmpfile}${host}_${interface}"
  exit 0
fi

traf_bps_in=$(( $traf_diff_in * 1000 / ($time_stamp-$time_stamp_last) * 8 ))
traf_bps_out=$(( $traf_diff_out  * 1000 / ($time_stamp-$time_stamp_last) * 8 ))
traf_mb_in=`bc <<< "scale=5; ( $traf_bps_in ) / 1024 / 1024 / $ispeed * 100" | sed 's/^\./0./'` #in % of BW
traf_mb_out=`bc <<< "scale=5; ( $traf_bps_out ) / 1024 / 1024 / $ispeed * 100" | sed 's/^\./0./'`
printf "%d\n%d\n%s" $traf_in $traf_out $time_stamp >"${path_tmpfile}${host}_${interface}"
#echo "traffic in ${traf_mb_in}, traffic out ${traf_mb_out}"

if [[ `bc <<< "(${traf_mb_in} >= ${low_warn_in}) && (${traf_mb_in} <= ${high_warn_in}) && (${traf_mb_out} >= ${low_warn_out}) && (${traf_mb_out} <= ${high_warn_out})"` == 1 ]]; then
echo "OK|In=${traf_mb_in};${high_warn_in};${high_crit_in};;; In_bps=${traf_bps_in};;;;; Out=${traf_mb_out};${high_warn_out};;;; Out_bps=${traf_bps_out};;;;;" #This last bit is the annoying format Nagios wants for graphs etc.
exit 0
#elif [[ `bc <<< "${traf_mb_in} < ${low_crit}"` ]] || [[ `bc <<< "${traf_mb_out} < ${low_crit}"` ]] || [[ `bc <<< "${traf_mb_in} > ${high_crit}"` ]] || [[ `bc <<< "${traf_mb_out} > ${high_crit}"` ]]; then
elif [[ `bc <<< "(${traf_mb_in} < ${low_crit_in}) || (${traf_mb_in} > ${high_crit_in}) || (${traf_mb_out} < ${low_crit_in}) || (${traf_mb_out} > ${high_crit_out})"` == 1 ]]; then
echo "Critical|In=${traf_mb_in};${high_warn_in};${high_crit_in};;; In_bps=${traf_bps_in};;;;; Out=${traf_mb_out};${high_warn_out};;;; Out_bps=${traf_bps_out};;;;;"
exit 2
#elif [[ `bc <<< "${traf_mb_in} < ${low_warn}"` ]] || [[ `bc <<< "${traf_mb_out} < ${low_warn}"` ]] || [[ `bc <<< "${traf_mb_out} < ${low_warn}"` ]] || [[ `bc <<< "${traf_mb_out} < ${low_warn}"` ]]; then
elif [[ `bc <<< "(${traf_mb_in} < ${low_warn_in}) || (${traf_mb_in} > ${high_warn_in}) || (${traf_mb_out} < ${low_warn_out}) || (${traf_mb_out} > ${high_warn_out})"` == 1 ]]; then
echo "Warning|In=${traf_mb_in};${high_warn_in};${high_crit_in};;; In_bps=${traf_bps_in};;;;; Out=${traf_mb_out};${high_warn_out};;;; Out_bps=${traf_bps_out};;;;;"
exit 1
else
echo "Unknown error"
exit 3
fi
 

