#!/usr/bin/perl -w
use SOAP::Lite;
use Data::Dumper;
$VERSION = "1.2";
$AUTHOR = "2015 Jonas Jacobsson / NetNordic AB";
sub print_version {
    print "check_static_scopes.pl version $VERSION, $AUTHOR\n\n";
}
sub print_help {
    print "check_static_scopes.pl is a Nagios plugin to check DHCP scopes in PackeFront BECS.\n",
    "This plugin uses SOAP to connect to BECS EAPI.\n\n";
}
sub print_usage {
    print "Usage: check_static_scopes.pl <server> <port> <user> <password> <scopename> <library-oid> <warning> <critical> [<exact match>] [<displayname>]\n",
    "       check_static_scopes.pl -h|--help\n",
    "       check_static_scopes.pl -v|--version\n\n";
}

my $ARGVS = ($#ARGV + 1);

if ($ARGVS == 1 && ($ARGV[0] eq "-h" || $ARGV[0] eq "--help")) {
    print_version();
    print_help();
    print_usage();
    exit 0;
} elsif ($ARGVS == 1 && ($ARGV[0] eq "-v" || $ARGV[0] eq "--version")) {
    print_version();
    exit 0;
} elsif ($ARGVS != 8 and $ARGVS != 9 and $ARGVS != 10) {
    print_usage();
    exit 0;
}

my $BECS_SERVER = $ARGV[0];
my $BECS_PORT = $ARGV[1];
my $BECS_USER = $ARGV[2];
my $BECS_PASS = $ARGV[3];
my $BECS_SCOPE = $ARGV[4];
my $BECS_OID = $ARGV[5];
my $WARN = $ARGV[6];
my $CRIT = $ARGV[7];
my $EXACT = $ARGV[8];
my $DISPLAY = $ARGV[9];
my $SUBNET = 0;
my $AVAILLEASES = 0;
my $USEDLEASES = 0;
my $counter = 0 ;
my $PERCENT = 0;
my $RESERVED = 0;
if ($ARGVS == 8) {
    $BECS_OID = $ARGV[5];#OID of library where resources are located in tree
    $WARN = $ARGV[6];
    $CRIT = $ARGV[7];
} 

my $EAPI = SOAP::Lite->new(#service=>"http://${BECS_SERVER}:${BECS_PORT}/becs.wsdl",
            proxy=>"http://${BECS_SERVER}:${BECS_PORT}/");
$EAPI->ns("urn:becs", "becs");
if (!$EAPI) {
    die("service $!\n");
}
my $r = becs_call('sessionLogin',
          SOAP::Data->name("username" => $BECS_USER),
          SOAP::Data->name("password" => $BECS_PASS));
my $SID = $r->result->{sessionid};
my $assigned = 0;
$s = becs_call('objectTreeFind',
                           build_vars([["oid", "unsignedLong", $BECS_OID],["classmask","string","resource-inet"],["walkdown","unsignedInt",1]]));

OUTER: foreach my $scope (@{$s->result->{objects}}) {
    if ($EXACT) {
     	if ($DISPLAY) {
            foreach my $scopeout (@{$scope->{opaque}}) {
			
                if (@{$scopeout->{values}}[0]->{value} eq $BECS_SCOPE && $scopeout->{name} eq "becs.displayname") {

				    count_subnet($scope->{resource}->{prefixlen}, $scope->{resource}->{address});
				    if ( defined $scope->{resource}->{reservebegin}) {

					   $RESERVED=$scope->{resource}->{reservebegin};
	                }	
				        
				    $r = becs_call('objectFind',
               			build_array("queries", "object", (build_vars([["class", "string", "resource-inet"]]),
                            SOAP::Data->name("resource" =>
                            \SOAP::Data->value(
                            SOAP::Data->name("rcparentoid" => $scope->{oid})->type("unsignedLong")))->type("resource"))));
                            foreach my $entry (@{$r->result->{objects}}) {
					            count_leases( $entry->{resource}->{prefixlen}, $entry->{resource}->{prefixlen});
						 	}
                }
	
            }

        }
	    else {
			if ($scope->{name} eq $BECS_SCOPE) {
				count_subnet($scope->{resource}->{prefixlen}, $scope->{resource}->{address});
                if ( defined $scope->{resource}->{reservebegin}) {
                    $RESERVED=$scope->{resource}->{reservebegin};
                }   
                $r = becs_call('objectFind',
                    build_array("queries", "object", (build_vars([["class", "string", "resource-inet"]]),
                        SOAP::Data->name("resource" =>
                        \SOAP::Data->value(
                        SOAP::Data->name("rcparentoid" => $scope->{oid})->type("unsignedLong")))->type("resource"))));
                        foreach my $entry (@{$r->result->{objects}}) {
                            count_leases( $entry->{resource}->{prefixlen}, $entry->{resource}->{prefixlen});
                        }
            }
            else {
                next OUTER if !( $scope->{name} eq $BECS_SCOPE)
            }
	   }	
	}
	else {
	    if ($DISPLAY) {
            foreach my $scopeout (@{$scope->{opaque}}) {
  		       if ($scopeout->{name} eq "becs.displayname" && @{$scopeout->{values}}[0]->{value} =~ /$BECS_SCOPE/) {
                    count_subnet($scope->{resource}->{prefixlen}, $scope->{resource}->{address});
			        if ( defined $scope->{resource}->{reservebegin}) {
                        $RESERVED=$scope->{resource}->{reservebegin};
			        }
                    $r = becs_call('objectFind',
                    build_array("queries", "object", (build_vars([["class", "string", "resource-inet"]]),
                        SOAP::Data->name("resource" =>
                        \SOAP::Data->value(
                        SOAP::Data->name("rcparentoid" => $scope->{oid})->type("unsignedLong")))->type("resource"))));
                        foreach my $entry (@{$r->result->{objects}}) {
                            count_leases( $entry->{resource}->{prefixlen}, $entry->{resource}->{prefixlen});
                        }
                }
            }
	    }
        else { 
	        if  ($scope->{name}  =~ /$BECS_SCOPE/) {
				count_subnet($scope->{resource}->{prefixlen}, $scope->{resource}->{address});
                if ( defined $scope->{resource}->{reservebegin}) {
                    $RESERVED=$scope->{resource}->{reservebegin};
                }
                $r = becs_call('objectFind',
                    build_array("queries", "object", (build_vars([["class", "string", "resource-inet"]]),
                    SOAP::Data->name("resource" =>
                    \SOAP::Data->value(
                    SOAP::Data->name("rcparentoid" => $scope->{oid})->type("unsignedLong")))->type("resource"))));
                    foreach my $entry (@{$r->result->{objects}}) {
					    count_leases( $entry->{resource}->{prefixlen}, $entry->{resource}->{prefixlen});
                    }
            }
            else {
                next OUTER if !( $scope->{name} =~ /$BECS_SCOPE/)
			}
	    }	
    }	
}
op5_output();
becs_call('sessionLogout');
$exitval=2;
exit $exitval;
# END OF PROGRAM

sub build_vars {
    my ($arrs) = @_;

    my @ret = ();
    foreach my $arr (@$arrs) {
    push @ret, SOAP::Data->name($arr->[0], $arr->[2])->type($arr->[1]);
    }

    return @ret;
}
sub op5_output {
    if ($AVAILLEASES !=0 ) {
        $PERCENT = int(($USEDLEASES/$AVAILLEASES)*100);
    }
    else {
        $PERCENT = 0; 
    }
    if ($USEDLEASES+$CRIT>$AVAILLEASES) {
        print "CRITICAL - $USEDLEASES used out of $AVAILLEASES : $PERCENT%|dhcp=$USEDLEASES;".int($AVAILLEASES-$WARN).";".int($AVAILLEASES-$CRIT).";;";
        exit 2;					
    }
    elsif ($USEDLEASES+$WARN>$AVAILLEASES) {
        print "WARNING - $USEDLEASES used out of $AVAILLEASES : $PERCENT%|dhcp=$USEDLEASES;".int($AVAILLEASES-$WARN).";".int($AVAILLEASES-$CRIT).";;";
        exit 1;
    }
    else {
        print "OK - $USEDLEASES used out of $AVAILLEASES : $PERCENT%|dhcp=$USEDLEASES;".int($AVAILLEASES-$WARN).";".int($AVAILLEASES-$CRIT).";;";
        exit 0;
    }
}

sub count_subnet {
    $SUBNET = $SUBNET." ".$_[1]."/".$_[0];
    $AVAILLEASES = $AVAILLEASES+(2 ** (32-$_[0]))-(3+$RESERVED);
}

sub count_leases {
    $USEDLEASES = $USEDLEASES+(2 ** (32-$_[0]));
    $counter=$counter."/".$_[1];
}

sub build_array {
    my ($name, $type, @vars) = @_;

    @varr = ();
    push @varr, SOAP::Data->name("item" =>
                 \SOAP::Data->value(@vars)->type($type));
    return SOAP::Data->name(
    $name => \SOAP::Data->value(
        SOAP::Data->name($name . "Item" => @varr)->
        type($type)))->type("ArrayOf_" . $type);
}

sub call_check {
    my ($r) = shift;
    if ($r->fault) {
    die "faultstring: \"" . $r->fault->{ faultstring } . "\"";
    return 0;
    }
    if ($r->result->{err} != 0) {
    die "errtxt: \"" . $r->result->{errtxt} . "\"";
    return 0;
    }

    return 1;
}

sub becs_call {
    my ($meth) = shift;
    my (@data) = @_;
    my ($r);

    if ($SID) {
    my $hdr = SOAP::Header->name("request" =>
                     \SOAP::Data->value(SOAP::Data->name("sessionid" => $SID)));
    $r = $EAPI->$meth($hdr,
              SOAP::Data->name("in" =>
                       \SOAP::Data->value(@data)));
    } else {
    $r = $EAPI->$meth(SOAP::Data->name("in" =>
                       \SOAP::Data->value(@data)));
    }

    return undef if !call_check($r);

    return $r;
}

