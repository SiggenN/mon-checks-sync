#!/bin/bash

PROGNAME="Universal Temperature $0"
VERSION="Version 1.0"
AUTHOR="Author: Niklas Sigg"

print_version() {
  echo ""
  echo "$VERSION"
  echo ""
  echo "$AUTHOR"
}

print_help() {

  print_version $PROGNAME $VERSION
  echo ""
  echo "$PROGNAME is a plugin to check time remaining on supporting OID's"
  echo ""
  echo "Usage: $PROGNAME -H [Host] -v [SNMP version] -C [SNMP community] -l [SEC level] (noAuthNoPriv|authNoPriv|authPriv or skip the flag if you're using snmpv1/snmpv2c) -u [SNMP user] -a [Auth mode] -A [Auth pass] -x [Privacy mode] -X [Privacy pass] -o [SNMP oid]"
  echo "Only works with single temperature sensor"
}

while [[ $1 != "" ]]; do

  case $1 in
    -H|--host)
    shift
    HOST=$1;;
    -v|--version)
    shift
    SNMPv=$1;;
    -C|--community)
    shift
    SNMPc=$1;;
    -l|--seclevel)
    shift
    SNMPl=$1;;
    -u|--user)
    shift
    SNMPu=$1;;
    -a|--authmode)
    shift
    SNMPa=$1;;
    -A|--authpass)
    shift
    SNMPauthpass=$1;;
    -x|--privacymode)
    shift
    SNMPx=$1;;
    -X|--privacypass)
    shift
    SNMPprivacypass=$1;;
    -o|--oid)
    shift
    SNMPo=$1;;
    -w|--warn)
    shift
    WARNTEMP=$1;;
    -c|--crit)
    shift
    CRITTEMP=$1;;
    -help|-h)
    print_help
    exit
 esac
 shift
done

ST_OK=0
ST_WR=1
ST_CR=2
ST_UK=3

#Standard response
resp=" - Battery Temperature "

#Get current temperature value and strip away uneccesary info.
if [[ "$SNMPl" == "authPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass -x $SNMPx -X $SNMPprivacypass $HOST ${SNMPo} 2>&1)
elif [[ "$SNMPl" == "authNoPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu -a $SNMPa -A $SNMPauthpass $HOST ${SNMPo} 2>&1)
elif [[ "$SNMPl" == "noAuthNoPriv" ]]; then
  OUTPUT=$(snmpget -Ovq -v $SNMPv -l $SNMPl -u $SNMPu $HOST ${SNMPo} 2>&1)
else
  OUTPUT=$(snmpget -O vq -v $SNMPv -c $SNMPc $HOST ${SNMPo}  2>&1)
fi
#Exit on SNMP error
if [ $? -ne 0 ]
then
   echo "UNKNOWN${resp}"
   exit $ST_UK
fi

CURRTEMP=$(echo $OUTPUT | cut -c1-2)

CURRTEMP1=$(echo "${CURRTEMP} C | Temperature=$CURRTEMP;$WARNTEMP;$CRITTEMP;;")

if [ "$CURRTEMP" -ge "$WARNTEMP" -a "$CURRTEMP" -lt "$CRITTEMP" ]
   then
      echo "WARNING${resp}${CURRTEMP1}"
      exit $ST_WR
      elif [ "$CURRTEMP" -ge "$CRITTEMP" ]
      then
         echo "CRITICAL${resp}${CURRTEMP1}"
         exit $ST_CR
      elif [ "$CURRTEMP" -lt "$WARNTEMP" -a "$CURRTEMP" -lt "$CRITTEMP" ]
         then
         echo "OK${resp}${CURRTEMP1}"
         exit $ST_OK
      else
         echo "UNKNOWN - Unable to get temperature"
         exit $ST_UK
fi