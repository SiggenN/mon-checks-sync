#! /bin/env python
# Author       : Stijn Gruwier <stijn.gruwier@notforadsgmail.com>
# Description  : Gets FSC ServerView (hardware) alerts through SNMP 
# $Id: check_serverview.py,v 1.8 2008/04/10 08:06:30 sg Exp $
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from schau_utils import NagiosPlugin
from schau_snmp import SnmpClient, SnmpError

###################################################
###   Definitions for building a NagiosPlugin   ###
###################################################


opties = {
    'host' : {'char': 'H', 'type':'string'},
    'protocol' : {'char': 'p', 'type':'int', 'default':1},
    'community' : {'char': 'C', 'type':'string', 'default':'public'},
    'ignore' : {'char': 'i', 'type':'string', 'default':''}
    }

help = {
# filename of the plugin
'filename':
'''check_serverview.py''',

# version
'version':
'''$Revision: 1.8 $''',

# disclaimer, license, legal mumbo jumbo
'disclaimer' : 
'''This nagios plugin comes with ABSOLUTELY NO WARRANTY.  You may redistribute
copies of the plugins under the terms of the GNU General Public License.
Copyright (c) 2007 Gruwier Stijn''',

# Summary
'preamble' :
'''This plugin checks all fsc serverview (hardware) systemHealth, returns global status''',

# Commandline usage
'use' :
'''Usage:	check_serverview.py -H host [-C community] [-p protocol]
		[-i|--ignore=subsystem1[,subsystem2[,...]]]
	check_serverview.py (-h|--help)
	check_serverview.py (-V|--version)''', 

# Description of every option
'options' :
'''check_serverview.py
 -H, --host=hostname
    Connect to hostname
 -p, --protocol=[1|2|3]
    Snmp version to use (default 1)
    Affects the community option
 -C, --community=COMMUNITY
    community string with SNMPv1/v2 OR <user>:<password> with SNMPv3
    Default: public
 -i, --ignore=SUBSYSTEM1,SUBSYSTEM2,SUBSYSTEM3
    list of subsystem names to ignore
    comma separated(do not use spaces!)
    Added this feature because I don't care about the deployment subsystem
 -w, --warning=WARNINGLEVEL
    Exit with WARNING status if a variable matches WARNINGLEVEL
    not implemented
 -c, --critical=CRITICALLEVEL
    Exit with CRITICAL status if a variable matches CRITICAL
    not implemented
 -t, --timeout=SECONDS
    seconds before plugin times out (default: 25)
    not implemented
 -v, --verbose=[0-]
    Set the amount of output
    not implemented
 -h, --help
    Print help 
 -V, --version
    Print version information''',

# Known deviceInfos
'bugs' :
'''Bugs:	Let me know - stijn.gruwier@gmailnotforads.com
        *remove the 'notforads' substring from the address*''' }

def serverview_function(options):
    # TODO: more option checks
    ignorelist = []
    host = options['host']
    community = options['community']
    protocol = int(options['protocol'])
    timeout = options['timeout']
    ignore = options['ignore']
    if not host:
        return('UNKNOWN', '-H, --host is a required argument')
    if not community:
        return('UNKNOWN', '-C, --community is a required argument')
    if not protocol in (1,2,3):
        return('UNKNOWN', 'invalid protocol')
    if ignore:
        ignorelist = ignore.lower().split(',')
    if protocol in (1,2):
        snmp = SnmpClient(host, protocol, community)
    else:
        user, key = community.split(':')
        snmp = SnmpClient(host, protocol, community=community)
    deviceInfoList = []
    deviceInfoString, systemHealth = '', ''
    try:
        deviceInfoList = get_raid_info(snmp) 
    except SnmpError:
        return('CRITICAL', 'network or snmp related problem - NOT a hardware problem')
    for deviceInfo in deviceInfoList:
        name, status, lastStatus = deviceInfo
        deviceInfoString = deviceInfoString + '\nName: %s\nStatus: %s  Number of device errors: %s' % (name,status,lastStatus)
    return('OK', 'Device Status: %s' % deviceInfoString)

def get_raid_info(snmp):
    '''Returns a string with subsystem names, and a list of failed ones '''
    svrAgentInfo = '.1.3.6.1.4.1.231.2.49.1.1'
    svrStatusOverall = '.1.3.6.1.4.1.231.2.49.1.3.4'
    svrNumberPhysicalDevices = '.1.3.6.1.4.1.231.2.49.1.5.1'
    svrPhysicalDeviceStatus = '.1.3.6.1.4.1.231.2.49.1.5.2.1.15.1.%s'
    svrPhysicalDeviceDisplayName = '.1.3.6.1.4.1.231.2.49.1.5.2.1.24.1.%s'
    svrPhysicalDeviceError = '.1.3.6.1.4.1.231.2.49.1.5.2.1.12.1.%s'
    STATUS_NR2STRING = {1:'Unknown',2:'No Disk',3:'Online',4:'Ready',5:'Failed',6:'Rebuilding',7:'Hot Spare Global',8:'Hot Spare Dedicated',9:'Offline',10:'Unconfigured Failed',11:'Formatting',12:'Dead'}
    deviceInfoList = []
    overallStatus, deviceStatus, deviceName, deviceError = 0,0,'',''
    overallStatus = int(snmp.get_table(svrStatusOverall)[4][0])
    counter = int(snmp.get_table(svrNumberPhysicalDevices)[1][0])
    for index in range (0, counter):
	index_str = str(index)
	deviceStatus = int(snmp.get_table(svrPhysicalDeviceStatus % index_str)[index][0])
	try:
            deviceStatus = STATUS_NR2STRING[deviceStatus]
        except:
            deviceStatus = 'outofrange'
	deviceName = str(snmp.get_table(svrPhysicalDeviceDisplayName % index_str)[index][0])
	deviceError = str(snmp.get_table(svrPhysicalDeviceError % index_str)[index][0])
	deviceInfoList.append((deviceName,deviceStatus,deviceError))
    return deviceInfoList
	
		

if __name__ == '__main__':
    plug = NagiosPlugin('SERVERVIEW', serverview_function, opties, help)
    plug.run(debug=True)

